import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { postNewReceipt } from '../../services/ReceiptService';
import { getAllProduct } from '../../services/ProductServices';
import { getAllSupplierToAdd } from '../../services/SupplierServices';
import { NumericFormat } from 'react-number-format';

function AddReceipt({ isShow, handleClose, updateReceipt }) {

    const [productId, setProductId] = useState("");
    const [supplierId, setSupplierId] = useState("");
    const [quantity, setQuantity] = useState("");
    const [totalMoney, setTotalMoney] = useState("");
    const [note, setNote] = useState("");

    const [listProduct, setListProduct] = useState([]);
    const [listSupplier, setListSupplier] = useState([]);

    useEffect(() => {
        getProduct();
        getSupplier();
    }, [])

    const getProduct = async () => {
        let res_list_product = await getAllProduct();
        if (res_list_product) {
            setListProduct(res_list_product);
        }
    }

    const getSupplier = async () => {
        let res_list_supplier = await getAllSupplierToAdd();
        if (res_list_supplier) {
            setListSupplier(res_list_supplier);
        }
    }

    const handleAddReceipt = async () => {
        let res = await postNewReceipt(productId, supplierId, quantity, totalMoney.replace(/,/g, ''), note);
        if (res.statusCode === 200) {
            handleClose();
            setProductId("");
            setSupplierId("");
            setQuantity("");
            setTotalMoney("");
            setNote("");
            toast.success("Thêm hoá đơn mua hàng thành công!");
            updateReceipt();
        }
        else {
            toast.error("Thêm hoá đơn mua hàng thất bại!");
        }
    }


    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Thêm hoá đơn mua hàng</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên sản phẩm</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={productId}
                                onChange={(event) => setProductId(event.target.value)}
                            >
                                <option value="">--Chọn sản phẩm--</option>
                                {!listProduct && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listProduct) && listProduct.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listProduct) && listProduct.length > 0 &&
                                    listProduct.map((item) => {
                                        return (
                                            <option key={item.productId} value={item.productId} >{item.productName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                    </div>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên nhà cung cấp</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={supplierId}
                                onChange={(event) => setSupplierId(event.target.value)}
                            >
                                <option value="">--Chọn nhà cung cấp--</option>
                                {!listSupplier && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listSupplier) && listSupplier.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listSupplier) && listSupplier.length > 0 &&
                                    listSupplier.map((item) => {
                                        return (
                                            <option key={item.supplierId} value={item.supplierId} >{item.supplierName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                    </div>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Số lượng</label>
                            <input
                                type="number"
                                className="form-control"
                                placeholder="Nhập số lượng"
                                value={quantity}
                                onChange={(event) => setQuantity(event.target.value)}
                            />
                        </div>
                    </div>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tổng số tiền</label>
                            <NumericFormat
                                type="text"
                                className="form-control"
                                thousandSeparator=","
                                placeholder="Nhập tổng số tiền"
                                value={totalMoney}
                                onChange={(event) => setTotalMoney(event.target.value)}
                            />
                        </div>
                    </div>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Ghi chú (nếu có)</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập ghi chú(nếu có)"
                                value={note}
                                onChange={(event) => setNote(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleAddReceipt()}>Thêm</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default AddReceipt;