import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { getAllProduct } from '../../services/ProductServices';
import { getAllStaff } from '../../services/StaffServices';
import { getAllSupplierToAdd } from '../../services/SupplierServices';
import { putEditReceipt } from '../../services/ReceiptService';
import { toast } from 'react-toastify';

function EditReceipt({ isShow, handleClose, dataReceiptEdit, updateReceipt }) {

    const [productId, setProductId] = useState("");
    const [productName, setProductName] = useState("");
    const [supplierId, setSupplierId] = useState("");
    const [supplierName, setSupplierName] = useState("");
    const [quantity, setQuantity] = useState("");
    const [totalMoney, setTotalMoney] = useState("");
    const [note, setNote] = useState("");

    const [listProduct, setListProduct] = useState([]);
    const [listSupplier, setListSupplier] = useState([]);

    useEffect(() => {
        getProduct();
        getSupplier();
    }, [])

    const getProduct = async () => {
        let res_list_product = await getAllProduct();
        if (res_list_product) {
            setListProduct(res_list_product);
        }
    }

    const getSupplier = async () => {
        let res_list_supplier = await getAllSupplierToAdd();
        if (res_list_supplier) {
            setListSupplier(res_list_supplier);
        }
    }

    const handleEditReceipt = async () => {
        let res = await putEditReceipt(70, productId, supplierId, quantity, totalMoney, note);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Sửa hoá đơn mua hàng thành công!");
            updateReceipt();
        }
        else {
            toast.error("Sửa hoá đơn mua hàng thất bại!");
        }
    }

    useEffect(() => {
        if (isShow) {
            setProductId(dataReceiptEdit.productId);
            setProductName(dataReceiptEdit.productName);
            setSupplierId(dataReceiptEdit.supplierId);
            setSupplierName(dataReceiptEdit.supplierName);
            setQuantity(dataReceiptEdit.quantity);
            setTotalMoney(dataReceiptEdit.totalMoney);
            setNote(dataReceiptEdit.note);
        }
    }, [dataReceiptEdit, isShow])

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Chỉnh sửa hoá đơn mua hàng</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên sản phẩm</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={productId}
                                onChange={(event) => setProductId(event.target.value)}
                            >
                                {!listProduct && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listProduct) && listProduct.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listProduct) && listProduct.length > 0 &&
                                    listProduct.map((item) => {
                                        <option defaultValue={item.productId} value={item.productId} >{item.productName}</option>
                                        return (
                                            <option key={item.productId} value={item.productId} >{item.productName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Tên nhà cung cấp</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={supplierId}
                                onChange={(event) => setSupplierId(event.target.value)}
                            >
                                {!listSupplier && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listSupplier) && listSupplier.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listSupplier) && listSupplier.length > 0 &&
                                    listSupplier.map((item) => {
                                        <option defaultValue={item.supplierId} value={item.supplierId} >{item.supplierName}</option>
                                        return (
                                            <option key={item.supplierId} value={item.supplierId} >{item.supplierName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số lượng</label>
                            <input
                                type="number"
                                className="form-control"
                                value={quantity}
                                onChange={(event) => setQuantity(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Tổng số tiền</label>
                            <input
                                type="number"
                                className="form-control"
                                value={totalMoney}
                                onChange={(event) => setTotalMoney(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Ghi chú(nếu có)</label>
                            <input
                                type="text"
                                className="form-control"
                                value={note}
                                onChange={(event) => setNote(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleEditReceipt()}>
                        Sửa
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default EditReceipt;