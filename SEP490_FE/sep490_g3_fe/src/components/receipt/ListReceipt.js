import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { getAllReceipt } from '../../services/ReceiptService';
import AddReceipt from './AddReceipt';
import EditReceipt from './EditReceipt';

function ListReceipt() {

    const [listReceipt, setListReceipt] = useState([]);

    const [totalReceiptPages, setTotalReceiptPages] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const [isShowModalAddReceipt, setIsShowModalAddReceipt] = useState(false);

    const [isShowModalEditReceipt, setIsShowModalEditReceipt] = useState(false);
    const [dataReceiptEdit, setDataReceiptEdit] = useState("");

    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };

    const handleClose = () => {
        setIsShowModalAddReceipt(false);
        setIsShowModalEditReceipt(false);
    }

    useEffect(() => {
        document.title = "Mua hàng";
        getReceipt(1);
    }, [])

    const getReceipt = async (page) => {
        let res = await getAllReceipt(page);
        if (res) {
            setTotalReceiptPages(res.totalPages);
            setListReceipt(res.data);
        }
    }

    const handlePageClick = (event) => {
        getReceipt(+event.selected + 1);
        setCurrentPage(+event.selected);
    }

    const updateReceipt = () => {
        getReceipt(currentPage + 1);
    }

    const handleEditReceipt = (receipt) => {
        setDataReceiptEdit(receipt);
        setIsShowModalEditReceipt(true);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Hoá đơn mua hàng</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <Button className='btn btn-success my-2' onClick={() => setIsShowModalAddReceipt(true)}>
                    <i className="fa-solid fa-circle-plus"></i>&nbsp;
                    Thêm hoá đơn mua hàng
                </Button>
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên sản phẩm</span>
                            </th>
                            <th>
                                <span>Tên nhà cung cấp</span>
                            </th>
                            <th>
                                <span>Số lượng</span>
                            </th>
                            <th>
                                <span>Tổng số tiền</span>
                            </th>
                            <th>
                                <span>Thời gian</span>
                            </th>
                            <th>
                                <span>Ghi chú</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listReceipt) && listReceipt.length > 0 &&
                            listReceipt.map((item, index) => {
                                return (
                                    <tr key={`receipt-${index + 1}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.productName}</td>
                                        <td>{item.supplierName}</td>
                                        <td>{item.quantity}</td>
                                        <td>{formatNumber(item.totalMoney)}</td>
                                        <td>{item.date}</td>
                                        <td>{item.note}</td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center  mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalReceiptPages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <AddReceipt
                isShow={isShowModalAddReceipt}
                handleClose={handleClose}
                updateReceipt={updateReceipt}
            />

            <EditReceipt
                isShow={isShowModalEditReceipt}
                handleClose={handleClose}
                dataReceiptEdit={dataReceiptEdit}
                updateReceipt={updateReceipt}
            />
        </>
    );
}

export default ListReceipt;