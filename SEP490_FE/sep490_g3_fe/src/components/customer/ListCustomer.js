import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { getAllCustomer } from '../../services/CustomerServices';
import ReactPaginate from 'react-paginate';
import EditCustomer from './EditCustomer';

function ListCustomer() {

    const [listCustomer, setListCustomer] = useState([]);

    const [totalCustomerPages, setTotalCustomerPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const [keyword, setKeyword] = useState("");

    const [isShowModalEditCustomer, setIsShowModalEditCustomer] = useState(false);
    const [dataCustomerEdit, setDataCustomerEdit] = useState("");

    const handleClose = () => {
        setIsShowModalEditCustomer(false);
    }

    useEffect(() => {
        document.title = "Khách hàng";
        getCustomer(1);
    }, [])

    const handlePageClick = (event) => {
        getCustomer(+event.selected + 1);
        setCurrentPage(+event.selected);
    }

    const getCustomer = async (page, keyword) => {
        let res = await getAllCustomer(page, keyword);
        if (res) {
            setTotalCustomerPages(res.totalPages);
            setListCustomer(res.data);
        }
    }

    const updateCustomer = () => {
        getCustomer(currentPage + 1, keyword);
    }

    const handleEditCustomer = (customer) => {
        setDataCustomerEdit(customer);
        setIsShowModalEditCustomer(true);
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleSearch();
        }
    }

    const handleSearch = () => {
        getCustomer(1, keyword);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Danh sách khách hàng</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <input
                    className='form-control'
                    placeholder='Tìm theo số điện thoại...'
                    onKeyDown={(event) => handlePressEnter(event)}
                    onChange={(event) => setKeyword(event.target.value)}
                />
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên khách hàng</span>
                            </th>
                            <th>
                                <span>Số điện thoại</span>
                            </th>
                            <th>
                                <span>Email</span>
                            </th>
                            <th>
                                <span>Địa chỉ</span>
                            </th>
                            <th>
                                <span>Tuỳ chỉnh</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listCustomer) && listCustomer.length > 0 &&
                            listCustomer.map((item, index) => {
                                return (
                                    <tr key={`customers-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.customerName}</td>
                                        <td>{item.phone}</td>
                                        <td>{item.email}</td>
                                        <td>{item.address}</td>
                                        <td>
                                            <Button
                                                className='btn btn-warning mx-1 my-1'
                                                onClick={() => handleEditCustomer(item)}
                                            >
                                                Sửa
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center  mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    //forcePage={currentPage}
                    pageCount={totalCustomerPages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <EditCustomer
                isShow={isShowModalEditCustomer}
                handleClose={handleClose}
                dataCustomerEdit={dataCustomerEdit}
                updateCustomer={updateCustomer}
            />
        </>
    );
}

export default ListCustomer;