import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putEditCustomer } from '../../services/CustomerServices';
import { toast } from 'react-toastify';

function EditCustomer({ isShow, handleClose, dataCustomerEdit, updateCustomer }) {

    const [customerId, setCustomerId] = useState("");
    const [customerName, setCustomerName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [active, setActive] = useState("");

    const handleEditCustomer = async () => {
        let res = await putEditCustomer(
            customerId,
            customerName,
            phone,
            email,
            address,
            active);
        if (res && res.statusCode == 200) {
            handleClose();
            toast.success("Sửa thông tin khách hàng thành công!");
            updateCustomer();
        }
        else {
            toast.error("Sửa thông tin khách hàng thất bại!");
        }
    }

    useEffect(() => {
        if (isShow) {
            setCustomerId(dataCustomerEdit.customerId);
            setCustomerName(dataCustomerEdit.customerName);
            setPhone(dataCustomerEdit.phone);
            setEmail(dataCustomerEdit.email);
            setAddress(dataCustomerEdit.address);
            setActive(dataCustomerEdit.active);
        }
    }, [dataCustomerEdit])

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Cập nhật thông tin khách hàng</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên khách hàng</label>
                            <input
                                type="text"
                                className="form-control"
                                value={customerName}
                                onChange={(event) => setCustomerName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số điện thoại</label>
                            <input
                                type="text"
                                className="form-control"
                                value={phone}
                                onChange={(event) => setPhone(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Địa chỉ</label>
                            <input
                                type="text"
                                className="form-control"
                                value={address}
                                onChange={(event) => setAddress(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="warning" onClick={() => handleEditCustomer()}>Sửa</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default EditCustomer;