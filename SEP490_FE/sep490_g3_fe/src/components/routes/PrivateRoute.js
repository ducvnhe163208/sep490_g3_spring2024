import React, { useContext } from 'react';
import { Route, Routes } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';
import { Alert } from 'react-bootstrap';

function PrivateRoute(props) {
    const { user } = useContext(UserContext);
    if (user && !user.auth) {
        return <>
            <Alert variant="danger" className='mt-3'>
                <Alert.Heading>Bạn đã gặp lỗi!</Alert.Heading>
                <p>
                    Bạn không có quyền truy cập vào trang này
                </p>
            </Alert>
        </>
    }
    return (
        <>
            {props.children}
        </>
    );
}

export default PrivateRoute;