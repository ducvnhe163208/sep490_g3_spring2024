import React from 'react';
import { Link } from 'react-router-dom';

function NotFound() {
    return (
        <>
            <div class="d-flex align-items-center justify-content-center h-75">
                <div class="text-center">
                    <h1 class="display-1 fw-bold">404</h1>
                    <p class="fs-3"> <span class="text-danger">Xin lỗi!</span> Không thấy trang.</p>
                    <p class="lead">
                        Trang bạn đang tìm kiếm không tồn tại.
                    </p>
                    <Link to="/" class="btn btn-primary">Quay lại trang chủ</Link>
                </div>
            </div>
        </>
    );
}

export default NotFound;