import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Register from '../../components/authentication/Register';
import Login from '../../components/authentication/Login';
import ListSupplier from '../../components/supplier/ListSupplier';
import ListStaff from '../../components/staff/ListStaff';
import SellProduct from '../../components/sale/SellProduct';
import ListCategory from '../../components/category/ListCategory';
import ListCustomer from '../../components/customer/ListCustomer';
import HomePage from '../../components/shared-components/HomePage';
import ListProduct from '../../components/product/ListProduct';
import DashBoard from '../../components/DashBoard';
import ListOrder from '../../components/order/ListOrder'
import Attendance from '../../components/attendance/Attendance';
import StaffLogin from '../../components/authentication/StaffLogin';
import UserProfile from '../../components/authentication/UserProfile';
import PrivateRoute from './PrivateRoute';
import NotFound from './NotFound';
import ForgotPassword from '../authentication/ForgotPassword';
import ListReceipt from '../receipt/ListReceipt';

function AppRoutes() {
    return (
        <>
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='/login' element={<Login />} />
                <Route path='/staff-login' element={<StaffLogin />} />
                <Route path='/register' element={<Register />} />
                <Route path='/forgot-password' element={<ForgotPassword />} />
                <Route
                    path='/dashboard'
                    element={
                        <PrivateRoute path='/dashboard' >
                            <DashBoard />
                        </PrivateRoute>}
                />
                <Route
                    path='/customer'
                    element={
                        <PrivateRoute path='/customer' >
                            <ListCustomer />
                        </PrivateRoute>}
                />
                <Route
                    path='/supplier'
                    element={
                        <PrivateRoute path='/supplier' >
                            <ListSupplier />
                        </PrivateRoute>}
                />
                <Route
                    path='/staff'
                    element={
                        <PrivateRoute path='/staff' >
                            <ListStaff />
                        </PrivateRoute>}
                />
                <Route
                    path='/category'
                    element={
                        <PrivateRoute path='/category' >
                            <ListCategory />
                        </PrivateRoute>}
                />
                <Route
                    path='/sale'
                    element={
                        <PrivateRoute path='/sale' >
                            <SellProduct />
                        </PrivateRoute>}
                />
                <Route
                    path='/attendance'
                    element={
                        <PrivateRoute path='/attendance' >
                            <Attendance />
                        </PrivateRoute>}
                />
                <Route
                    path='/product'
                    element={
                        <PrivateRoute path='/product' >
                            <ListProduct />
                        </PrivateRoute>}
                />
                <Route
                    path='/receipt'
                    element={
                        <PrivateRoute path='/receipt' >
                            <ListReceipt />
                        </PrivateRoute>}
                />
                <Route
                    path='/order'
                    element={
                        <PrivateRoute path='/order' >
                            <ListOrder />
                        </PrivateRoute>}
                />
                <Route
                    path='/profile'
                    element={
                        <PrivateRoute path='/profile' >
                            <UserProfile />
                        </PrivateRoute>}
                />
                <Route path='*' element={<NotFound />} />
            </Routes>
        </>
    );
}

export default AppRoutes;