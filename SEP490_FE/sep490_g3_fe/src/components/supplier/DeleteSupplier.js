import { React } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putDeleteSupplier } from '../../services/SupplierServices';
import { toast } from 'react-toastify';

function DeleteSupplier({ isShow, handleClose, dataSupplierDelete, updateSupplier }) {

    const handleDeleteSupplier = async () => {
        let res = await putDeleteSupplier(dataSupplierDelete.supplierId);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Thay đổi trạng thái thành công");
            updateSupplier();
        }
        else {
            toast.error("Thay đổi trạng thái thất bại");
        }
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div>
                        Bạn có muốn thay đổi trạng thái nhà cung cấp <b>{dataSupplierDelete.supplierName}</b> không?<br />
                        Hành động không này thể hoàn tác!
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Huỷ
                    </Button>
                    <Button variant="danger" onClick={() => handleDeleteSupplier()}>
                        Thay đổi
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default DeleteSupplier;