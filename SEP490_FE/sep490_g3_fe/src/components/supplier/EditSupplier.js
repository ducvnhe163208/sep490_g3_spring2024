import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putEditSupplier } from '../../services/SupplierServices';
import { toast } from 'react-toastify';

function EditSupplier({ isShow, handleClose, dataSupplierEdit, updateSupplier }) {

    const [supplierId, setSupplierId] = useState("");
    const [supplierName, setSupplierName] = useState("");
    const [supplierLogo, setSupplierLogo] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [email, setEmail] = useState("");
    const [taxCode, setTaxCode] = useState("");

    const handleEditSupplier = async () => {
        let res = await putEditSupplier(supplierId, supplierName, phone, address, email, supplierLogo, taxCode);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Sửa nhà cung cấp thành công!");
            updateSupplier();
        }
        else {
            toast.error("Sửa nhà cung cấp thất bại!");
        }
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onloadend = () => {
            setSupplierLogo(reader.result);
        };
        reader.readAsDataURL(file);
    };

    useEffect(() => {
        if (isShow) {
            setSupplierId(dataSupplierEdit.supplierId);
            setSupplierName(dataSupplierEdit.supplierName);
            setPhone(dataSupplierEdit.phone);
            setAddress(dataSupplierEdit.address);
            setEmail(dataSupplierEdit.email);
            setSupplierLogo(dataSupplierEdit.logo);
            setTaxCode(dataSupplierEdit.taxCode);
        }
    }, [isShow, dataSupplierEdit])

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Chỉnh sửa nhà cung cấp</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên nhà cung cấp</label>
                            <input
                                type="text"
                                className="form-control"
                                value={supplierName}
                                onChange={(event) => setSupplierName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Logo nhà cung cấp</label>
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                            />
                            {supplierLogo && (
                                <div className=' d-flex justify-content-center'>
                                    <img src={supplierLogo} className='add-image my-3' alt="Supplier Logo"/>
                                </div>
                            )}
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số điện thoại</label>
                            <input
                                type="text"
                                className="form-control"
                                value={phone}
                                onChange={(event) => setPhone(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Địa chỉ</label>
                            <input
                                type="text"
                                className="form-control"
                                value={address}
                                onChange={(event) => setAddress(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mã số thuế</label>
                            <input
                                type="text"
                                className="form-control"
                                value={taxCode}
                                onChange={(event) => setTaxCode(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleEditSupplier()}>
                        Sửa
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default EditSupplier;