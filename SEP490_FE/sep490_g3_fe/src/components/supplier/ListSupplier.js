import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { getAllSupplier } from '../../services/SupplierServices'
import ReactPaginate from 'react-paginate';
import AddSupplier from './AddSupplier';
import EditSupplier from './EditSupplier';
import DeleteSupplier from './DeleteSupplier';

function ListSupplier() {

    const [listSupplier, setListSupplier] = useState([]);

    const [totalSupplierPages, setTotalSupplierPages] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const [keyword, setKeyword] = useState("");

    const [isShowModalAddSupplier, setIsShowModalAddSupplier] = useState(false);

    const [isShowModalEditSupplier, setIsShowModalEditSupplier] = useState(false);
    const [dataSupplierEdit, setDataSupplierEdit] = useState("");

    const [isShowModalDeleteSupplier, setIsShowModalDeleteSupplier] = useState(false);
    const [dataSupplierDelete, setDataSupplierDelete] = useState("");

    const handleClose = () => {
        setIsShowModalAddSupplier(false);
        setIsShowModalEditSupplier(false);
        setIsShowModalDeleteSupplier(false);
    }

    useEffect(() => {
        document.title = "Nhà cung cấp";
        getSupplier(1);
    }, [])

    const getSupplier = async (page, keyword) => {
        let res = await getAllSupplier(page, keyword);
        if (res) {
            setTotalSupplierPages(res.totalPages);
            setListSupplier(res.data);
        }
    }

    const handlePageClick = (event) => {
        getSupplier(+event.selected + 1, keyword);
        setCurrentPage(+event.selected);
    }

    const updateSupplier = () => {
        getSupplier(currentPage + 1, keyword);
    }

    const handleEditSupplier = (supplier) => {
        setDataSupplierEdit(supplier);
        setIsShowModalEditSupplier(true);
    }

    const handleDeleteSupplier = (supplier) => {
        setDataSupplierDelete(supplier);
        setIsShowModalDeleteSupplier(true);
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleSearch();
        }
    }

    const handleSearch = () => {
        getSupplier(1, keyword);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Danh sách nhà cung cấp</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <input
                    className='form-control'
                    placeholder='Tìm theo tên hoặc số điện thoại...'
                    onKeyDown={(event) => handlePressEnter(event)}
                    onChange={(event) => setKeyword(event.target.value)}
                />
                <Button className='btn btn-success my-2' onClick={() => setIsShowModalAddSupplier(true)}>
                    <i className="fa-solid fa-circle-plus"></i>&nbsp;
                    Thêm nhà cung cấp
                </Button>
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên nhà cung cấp</span>
                            </th>
                            <th>
                                <span>Logo</span>
                            </th>
                            <th>
                                <span>Số điện thoại</span>
                            </th>
                            <th>
                                <span>Địa chỉ</span>
                            </th>
                            <th>
                                <span>Email</span>
                            </th>
                            <th>
                                <span>Mã số thuế</span>
                            </th>
                            <th>
                                <span>Trạng thái</span>
                            </th>
                            <th>Tuỳ chỉnh</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listSupplier) && listSupplier.length > 0 &&
                            listSupplier.map((item, index) => {
                                return (
                                    <tr key={`suppliers-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.supplierName}</td>
                                        <td><img className='supplier-logo' src={item.logo} alt="Supplier Logo" /></td>
                                        <td>{item.phone}</td>
                                        <td>{item.email}</td>
                                        <td>{item.address}</td>
                                        <td>{item.taxCode}</td>
                                        <td>{item.active === true ? <p><i className="fa-solid fa-toggle-on"></i> Đang hợp tác</p> : <p><i className="fa-solid fa-toggle-off"></i> Ngừng hợp tác</p>}</td>
                                        <td>
                                            <Button
                                                className='btn btn-warning mx-1 my-1'
                                                onClick={() => handleEditSupplier(item)}
                                            >
                                                Sửa
                                            </Button>
                                            <Button
                                                className='btn btn-danger mx-1 my-1'
                                                onClick={() => handleDeleteSupplier(item)}
                                            >
                                                {item.active === true ? "Disable" : "Enable"}
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalSupplierPages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <AddSupplier
                isShow={isShowModalAddSupplier}
                handleClose={handleClose}
                updateSupplier={updateSupplier}
            />

            <EditSupplier
                isShow={isShowModalEditSupplier}
                handleClose={handleClose}
                dataSupplierEdit={dataSupplierEdit}
                updateSupplier={updateSupplier}
            />

            <DeleteSupplier
                isShow={isShowModalDeleteSupplier}
                handleClose={handleClose}
                dataSupplierDelete={dataSupplierDelete}
                updateSupplier={updateSupplier}
            />
        </>
    );
}

export default ListSupplier;