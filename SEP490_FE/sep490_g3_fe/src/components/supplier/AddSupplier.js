import { React, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { postAddSupplier } from '../../services/SupplierServices';
import { toast } from 'react-toastify';

function AddSupplier({ isShow, handleClose, updateSupplier }) {

    const [supplierName, setSupplierName] = useState("");
    const [supplierLogo, setSupplierLogo] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [email, setEmail] = useState("");
    const [taxCode, setTaxCode] = useState("");

    const handleAddSupplier = async () => {
        let res = await postAddSupplier(supplierName, phone, address, email, supplierLogo, taxCode);
        if (res) {
            handleClose();
            setSupplierName('');
            setSupplierLogo('');
            setPhone('');
            setAddress('');
            setEmail('');
            setTaxCode('');
            toast.success("Thêm nhà cung cấp thành công!");
            updateSupplier();
        }
        else {
            toast.error("Thêm nhà cung cấp thất bại!");
        }
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onloadend = () => {
                console.log(reader.result);
                setSupplierLogo(reader.result);
            };
            reader.readAsDataURL(file);
        }
        else {
            setSupplierLogo('');
        }
    };

    const handleReset = () => {
        setSupplierName('');
        setSupplierLogo('');
        setPhone('');
        setAddress('');
        setEmail('');
        setTaxCode('');
        setSupplierLogo('');
        handleClose();
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={() => handleReset()}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Thêm nhà cung cấp</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên nhà cung cấp</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên nhà cung cấp"
                                value={supplierName}
                                onChange={(event) => setSupplierName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Logo nhà cung cấp</label>
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                            />
                            {supplierLogo && (
                                <div className=' d-flex justify-content-center'>
                                    <img src={supplierLogo} className='add-image my-3' alt="Supplier Logo"/>
                                </div>
                            )}
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số điện thoại</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập số điện thoại"
                                value={phone}
                                onChange={(event) => setPhone(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Địa chỉ</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập địa chỉ"
                                value={address}
                                onChange={(event) => setAddress(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập email"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mã số thuế</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập mã số thuế"
                                value={taxCode}
                                onChange={(event) => setTaxCode(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleReset()}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleAddSupplier()}>Thêm</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default AddSupplier;