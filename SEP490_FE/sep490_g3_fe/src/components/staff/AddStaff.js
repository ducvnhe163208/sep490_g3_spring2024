import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { getAllRole, postAddStaff } from '../../services/StaffServices';
import { toast } from 'react-toastify';

function AddStaff({ isShow, handleClose, updateStaff }) {

    const [roleId, setRoleId] = useState("");
    const [fullName, setFullName] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [avatar, setAvatar] = useState("");
    const [address, setAddress] = useState("");
    const [taxCode, setTaxCode] = useState("");
    const [citizenIdentificationCard, setCitizenIdentificationCard] = useState("");
    const [bankAccountNumber, setBankAccountNumber] = useState("");

    const [listRole, setListRole] = useState([]);

    useEffect(() => {
        getRole();
    }, [])

    const getRole = async () => {
        let res = await getAllRole();
        if (res) {
            setListRole(res);
        }
    }


    const handleAddStaff = async () => {
        let res = await postAddStaff(
            roleId,
            fullName,
            username,
            password,
            phone,
            email,
            avatar,
            address,
            taxCode,
            citizenIdentificationCard,
            bankAccountNumber);

        if (res.statusCode === 200) {
            handleClose();
            setRoleId('');
            setFullName('');
            setUsername('');
            setPassword('');
            setPhone('');
            setEmail('');
            setAvatar('');
            setAddress('');
            setTaxCode('');
            setCitizenIdentificationCard('');
            setBankAccountNumber('');
            toast.success("Thêm nhân viên thành công!");
            updateStaff();
        }
        else if (res.status === 400) {
            toast.error("Thêm nhân viên thất bại!");
        }
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onloadend = () => {
                setAvatar(reader.result);
            };
            reader.readAsDataURL(file);
        }
        else {
            setAvatar('');
        }
    };

    const handleReset = () => {
        setRoleId('');
        setFullName('');
        setUsername('');
        setPassword('');
        setPhone('');
        setEmail('');
        setAvatar('');
        setAddress('');
        setTaxCode('');
        setCitizenIdentificationCard('');
        setBankAccountNumber('');
        handleClose();
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Thêm nhân viên</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên nhân viên</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên nhân viên"
                                value={fullName}
                                onChange={(event) => setFullName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Chức vụ</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={roleId}
                                onChange={(event) => setRoleId(event.target.value)}
                            >
                                <option value="">--Chọn chức vụ--</option>
                                {!listRole && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listRole) && listRole.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listRole) && listRole.length > 0 &&
                                    listRole.map((item) => {
                                        return (
                                            <option key={item.roleId} value={item.roleId} >{item.roleName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Tên tài khoản</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên tài khoản"
                                value={username}
                                onChange={(event) => setUsername(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mật khẩu</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên mật khẩu"
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số điện thoại</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập số điện thoại"
                                value={phone}
                                onChange={(event) => setPhone(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập email"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Hình ảnh</label>
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                            />
                            {avatar && (
                                <div className=' d-flex justify-content-center'>
                                    <img src={avatar} className='add-image my-3' alt="Avatar Staff" />
                                </div>
                            )}
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Địa chỉ</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập địa chỉ"
                                value={address}
                                onChange={(event) => setAddress(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mã số thuế</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập mã số thuế"
                                value={taxCode}
                                onChange={(event) => setTaxCode(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số CCCD</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập số CCCD"
                                value={citizenIdentificationCard}
                                onChange={(event) => setCitizenIdentificationCard(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số tài khoản ngân hàng</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập số tài khoản ngân hàng"
                                value={bankAccountNumber}
                                onChange={(event) => setBankAccountNumber(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleReset()}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleAddStaff()}>Thêm</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default AddStaff;