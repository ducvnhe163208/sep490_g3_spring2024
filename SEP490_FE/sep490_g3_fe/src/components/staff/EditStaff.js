import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putEditStaff, getAllRole } from '../../services/StaffServices';
import { toast } from 'react-toastify';

function EditStaff({ isShow, handleClose, dataStaffEdit, updateStaff }) {

    const [staffId, setStaffId] = useState("");
    const [adminId, setAdminId] = useState("");
    const [roleId, setRoleId] = useState("");
    const [roleName, setRoleName] = useState("");
    const [fullName, setFullName] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [avatar, setAvatar] = useState("");
    const [address, setAddress] = useState("");
    const [taxCode, setTaxCode] = useState("");
    const [citizenIdentificationCard, setCitizenIdentificationCard] = useState("");
    const [bankAccountNumber, setBankAccountNumber] = useState("");
    const [listRole, setListRole] = useState([]);

    useEffect(() => {
        getRole();
    }, [])

    const getRole = async () => {
        let res = await getAllRole();
        if (res) {
            setListRole(res);
        }
    }


    const handleEditStaff = async () => {
        let res = await putEditStaff(
            staffId,
            roleId,
            fullName,
            username,
            password,
            phone,
            email,
            avatar,
            address,
            taxCode,
            citizenIdentificationCard,
            bankAccountNumber);
        console.log(res);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Sửa nhân viên thành công!");
            updateStaff();
        }
        else {
            toast.error("Sửa nhân viên thất bại!");
        }
    }

    useEffect(() => {
        if (isShow) {
            setStaffId(dataStaffEdit.id);
            setRoleId(dataStaffEdit.roleId);
            setRoleName(dataStaffEdit.roleName);
            setFullName(dataStaffEdit.fullName);
            setUsername(dataStaffEdit.username);
            setPassword(dataStaffEdit.password);
            setPhone(dataStaffEdit.phone);
            setEmail(dataStaffEdit.email);
            setAvatar(dataStaffEdit.avatar);
            setAddress(dataStaffEdit.address);
            setTaxCode(dataStaffEdit.taxCode);
            setCitizenIdentificationCard(dataStaffEdit.citizenIdentificationCard);
            setBankAccountNumber(dataStaffEdit.bankAccountNumber);
        }
    }, [dataStaffEdit, isShow])

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onloadend = () => {
            setAvatar(reader.result);
        };
        reader.readAsDataURL(file);
    };

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Chỉnh sửa nhân viên</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên nhân viên</label>
                            <input
                                type="text"
                                className="form-control"
                                value={fullName}
                                onChange={(event) => setFullName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Chức vụ</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={roleId}
                                onChange={(event) => setRoleId(event.target.value)}
                            >
                                {!listRole && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listRole) && listRole.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listRole) && listRole.length > 0 &&
                                    listRole.map((item) => {
                                        <option defaultValue={item.roleId} value={item.roleId} >{item.roleName}</option>
                                        return (
                                            <option key={item.roleId} value={item.roleId} >{item.roleName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Tên tài khoản</label>
                            <input
                                type="text"
                                className="form-control"
                                value={username}
                                onChange={(event) => setUsername(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mật khẩu</label>
                            <input
                                type="password"
                                className="form-control"
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số điện thoại</label>
                            <input
                                type="text"
                                className="form-control"
                                value={phone}
                                onChange={(event) => setPhone(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                value={email}
                                onChange={(event) => setEmail(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Hình ảnh</label>
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                            />
                            {avatar && (
                                <div className=' d-flex justify-content-center'>
                                    <img src={avatar} className='add-image my-3' alt="Avatar Logo" />
                                </div>
                            )}
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Địa chỉ</label>
                            <input
                                type="text"
                                className="form-control"
                                value={address}
                                onChange={(event) => setAddress(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mã số thuế</label>
                            <input
                                type="text"
                                className="form-control"
                                value={taxCode}
                                onChange={(event) => setTaxCode(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số CCCD</label>
                            <input
                                type="text"
                                className="form-control"
                                value={citizenIdentificationCard}
                                onChange={(event) => setCitizenIdentificationCard(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số tài khoản ngân hàng</label>
                            <input
                                type="text"
                                className="form-control"
                                value={bankAccountNumber}
                                onChange={(event) => setBankAccountNumber(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleEditStaff()}>
                        Sửa
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default EditStaff;