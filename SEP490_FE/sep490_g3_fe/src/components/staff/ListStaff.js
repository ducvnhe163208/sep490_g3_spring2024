import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { getAllStaff } from '../../services/StaffServices';
import ReactPaginate from 'react-paginate';
import AddStaff from './AddStaff';
import EditStaff from './EditStaff';
import DeleteStaff from './DeleteStaff';

function ListStaff() {

    const [listStaff, setListStaff] = useState([]);

    const [totalStaffPages, setTotalStaffPages] = useState(3);
    const [currentPage, setCurrentPage] = useState(0);

    const [keyword, setKeyword] = useState(null);

    const [isShowModalAddStaff, setIsShowModalAddStaff] = useState(false);

    const [isShowModalEditStaff, setIsShowModalEditStaff] = useState(false);
    const [dataStaffEdit, setDataStaffEdit] = useState("");

    const [isShowModalDeleteStaff, setIsShowModalDeleteStaff] = useState(false);
    const [dataStaffDelete, setDataStaffDelete] = useState("");

    const handleClose = () => {
        setIsShowModalAddStaff(false);
        setIsShowModalEditStaff(false);
        setIsShowModalDeleteStaff(false);
    }

    useEffect(() => {
        document.title = "Nhân viên";
        getStaff(1);
    }, []);

    const getStaff = async (page, keyword) => {
        let res = await getAllStaff(page, keyword);
        if (res) {
            setTotalStaffPages(res.totalPages);
            setListStaff(res.data);
        }
    }

    const handlePageClick = (event) => {
        getStaff(+event.selected + 1, keyword);
        setCurrentPage(+event.selected);
    }

    const updateStaff = () => {
        getStaff(currentPage + 1);
    }

    const handleEditStaff = (staff) => {
        setDataStaffEdit(staff);
        setIsShowModalEditStaff(true);
    }

    const handleDeleteStaff = (staff) => {
        setDataStaffDelete(staff);
        setIsShowModalDeleteStaff(true);
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleSearch();
        }
    }

    const handleSearch = () => {
        getStaff(1, keyword);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Danh sách nhân viên</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <input
                    className='form-control mx-2'
                    placeholder='Tìm theo tên hoặc số điện thoại nhân viên...'
                    onKeyDown={(event) => handlePressEnter(event)}
                    onChange={(event) => setKeyword(event.target.value)}
                />
                <Button className='btn btn-success my-2 mx-2' onClick={() => setIsShowModalAddStaff(true)}>
                    <i className="fa-solid fa-circle-plus"></i>&nbsp;
                    Thêm nhân viên
                </Button>
            </div>
            <div className='customize-table mx-2'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên nhân viên</span>
                            </th>
                            <th>
                                <span>Hình ảnh</span>
                            </th>
                            <th>
                                <span>Số điện thoại</span>
                            </th>
                            <th>
                                <span>Email</span>
                            </th>
                            <th>
                                <span>STK ngân hàng</span>
                            </th>
                            <th>
                                <span>Trạng thái</span>
                            </th>
                            <th>Tuỳ chỉnh</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listStaff) && listStaff.length > 0 &&
                            listStaff.map((item, index) => {
                                return (
                                    <tr key={`staffs-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.fullName}</td>
                                        <td><img className='supplier-logo' src={item.avatar} alt="Avatar Staff" /></td>
                                        <td>{item.phone}</td>
                                        <td>{item.email}</td>
                                        <td>{item.bankAccountNumber}</td>
                                        <td>{item.active === true ? <p><i className="fa-solid fa-toggle-on"></i> Đang hợp tác</p> : <p><i className="fa-solid fa-toggle-off"></i> Ngừng hợp tác</p>}</td>
                                        <td>
                                            <Button
                                                className='btn btn-warning mx-1 my-1'
                                                onClick={() => handleEditStaff(item)}
                                            >
                                                Sửa
                                            </Button>
                                            <Button
                                                className='btn btn-danger mx-1 my-1'
                                                onClick={() => handleDeleteStaff(item)}
                                            >
                                                {item.active === true ? "Disable" : "Enable"}
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center  mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalStaffPages ? Math.ceil(totalStaffPages) : 0}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <AddStaff
                isShow={isShowModalAddStaff}
                handleClose={handleClose}
                updateStaff={updateStaff}
            />
            <EditStaff
                isShow={isShowModalEditStaff}
                handleClose={handleClose}
                dataStaffEdit={dataStaffEdit}
                updateStaff={updateStaff}
            />
            <DeleteStaff
                isShow={isShowModalDeleteStaff}
                handleClose={handleClose}
                dataStaffDelete={dataStaffDelete}
                updateStaff={updateStaff}
            />
        </>
    );
}

export default ListStaff;