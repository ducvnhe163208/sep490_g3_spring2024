import { React } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { deleteDeleteStaff } from '../../services/StaffServices';
import { toast } from 'react-toastify';

function DeleteStaff({ isShow, handleClose, dataStaffDelete, updateStaff }) {

    const handleDeleteStaff = async () => {
        let res = await deleteDeleteStaff(dataStaffDelete.id);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Thay đổi trạng thái thành công");
            updateStaff();
        }
        else {
            toast.error("Thay đổi trạng thái thất bại");
        }
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div>
                        Bạn có muốn xoá nhân viên <b>{dataStaffDelete.fullName}</b> không?<br />
                        Hành động không này thể hoàn tác!
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Huỷ
                    </Button>
                    <Button variant="danger" onClick={() => handleDeleteStaff()}>
                        Thay đổi
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default DeleteStaff;