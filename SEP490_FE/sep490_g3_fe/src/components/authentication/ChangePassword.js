import { React, useContext, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { getAdminById, putChangePassword } from '../../services/UserServices';
import { UserContext } from '../../context/UserContext';
import { useNavigate } from 'react-router-dom';


function ChangePassword({ isShow, handleClose }) {

    const { logout, user } = useContext(UserContext);
    const navigate = useNavigate();

    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [reNewPassword, setReNewPassword] = useState("");

    useEffect(() => {
    }, [])

    const handleChangePassword = () => {
        if (!oldPassword || !newPassword || !reNewPassword) {
            toast.error("Vui lòng nhập đầy đủ các trường!");
            return;
        }
        if (newPassword !== reNewPassword) {
            toast.error("Mật khẩu mới và nhập lại mật khẩu mới không khớp!");
            return;
        }
        if (newPassword === reNewPassword) {
            let res_change = putChangePassword(localStorage.getItem("id"), oldPassword, newPassword)
            if (res_change) {
                logout();
                toast.success("Thay đổi mật khẩu thành công, vui lòng đăng nhập lại");
                navigate("/login");
            }
            else {
                toast.error("Thay đổi mật khẩu không thành công, vui lòng thử lại");
                return;
            }
        }
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Đổi mật khẩu</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Mật khẩu cũ</label>
                            <input
                                type="password"
                                className="form-control"
                                onChange={(event) => setOldPassword(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mật khẩu mới</label>
                            <input
                                type="password"
                                className="form-control"
                                onChange={(event) => setNewPassword(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Nhập lại mật khẩu mới</label>
                            <input
                                type="password"
                                className="form-control"
                                onChange={(event) => setReNewPassword(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleChangePassword()}>Đổi mật khẩu</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ChangePassword;