import { React, useState, useContext, useEffect } from 'react';
import { toast } from 'react-toastify';
import { getLogin } from '../../services/HomeServices';
import { UserContext } from '../../context/UserContext';
import { useNavigate } from 'react-router-dom'

function Login() {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isShowPassword, setIsShowPassword] = useState(false);
    const [loadingAPI, setLoadingAPI] = useState(false);

    const { loginContext } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        document.title = "Đăng nhập";
        let token = localStorage.getItem("token");
        if (token) {
            navigate("/");
        }
    }, [])
    /*
                  Created by: DucNHM
                  Created on: 05/03/2024 (dd/mm/yyyy)
                  Description: This function is login
                  File: $/sep490_g3_fe\src\components\Login.js
                  ---
                  Modified by: 
                  ...
            */

    const handleLogin = async () => {
        if (!email || !password) {
            toast.error("Tài khoản hoặc mật khẩu không được để trống");
            setLoadingAPI(false);
            return;
        }
        setLoadingAPI(true);
        let res = await getLogin(email, password);
        
        if (res && res.token) {
            loginContext(1, email, res.token, res.phoneNumber);
            localStorage.setItem("id", res.userid);
            toast.success("Đăng nhập thành công");
            navigate("/dashboard");
        }
        if (res && res.status === 401) {
            toast.error("Sai tài khoản hoặc mật khẩu");
            setLoadingAPI(false);
            return;
        }
        if (res && res.status === 400) {
            toast.error("Đăng nhập thất bại");
            setLoadingAPI(false);
            return;
        }
        setLoadingAPI(false);
    }
    /*
                  Created by: DucNHM
                  Created on: 05/03/2024 (dd/mm/yyyy)
                  Description: This function go back
                  File: $/sep490_g3_fe\src\components\Login.js
                  ---
                  Modified by: 
                  ...
            */
    const handleGoBack = () => {
        navigate("/");
    }

    const handleStaffLogin = () => {
        navigate("/staff-login");
    }

    const handleForgotPassword = () => {
        navigate("/forgot-password");
    }
    /*
                  Created by: DucNHM
                  Created on: 05/03/2024 (dd/mm/yyyy)
                  Description: This method enter to Logim
                  File: $/sep490_g3_fe\src\components\Login.js
                  ---
                  Modified by: 
                  ...
            */
    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleLogin();
        }
    }

    return (
        <>
            <div className='login-container col-12 col-sm-6 col-md-6 col-lg-4 col-xxl-4'>
                <div className="title mb-5">Đăng nhập</div>
                <input
                    type='text'
                    placeholder='Tên tài khoản'
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
                <div className='input-password'>
                    <input
                        type={isShowPassword === true ? "text" : "password"}
                        placeholder='Mật khẩu'
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                        onKeyDown={(event) => handlePressEnter(event)}
                    />
                    <i className={isShowPassword === true ? "fa-solid fa-eye" : "fa-solid fa-eye-slash"}
                        onClick={() => setIsShowPassword(!isShowPassword)}
                    ></i>
                </div>
                <button
                    onClick={() => handleLogin()}
                >
                    {loadingAPI && <i className="fa-solid fa-circle-notch fa-spin"></i>}
                    &nbsp;Đăng nhập
                </button>
                <div className='back'>
                    <span onClick={() => handleGoBack()}>
                        <i className="fa-solid fa-angle-left">
                        </i>&nbsp;Quay lại
                    </span>
                    <span onClick={() => handleStaffLogin()}>
                        &nbsp; | &nbsp;Đăng nhập với nhân viên
                    </span>
                    <span onClick={() => handleForgotPassword()}>
                        &nbsp; | &nbsp;Quên mật khẩu
                    </span>
                </div>
            </div>
        </>
    );
}

export default Login;