import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { postRegister } from '../../services/HomeServices'
import { useNavigate } from 'react-router-dom'
import { VietQR } from 'vietqr'

function Register() {

    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [rePassword, setRePassword] = useState("");
    const [taxCode, setTaxCode] = useState("");
    const [citizenIdentificationCard, setCitizenIdentificationCard] = useState("");
    const [bankAccountNumber, setBankAccountNumber] = useState("");
    const [banks, setBanks] = useState([]);
    const [selectedBank, setSelectedBank] = useState('');

    const validatePassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+}{":;'?/>.<,])(?=.*[^\da-zA-Z]).{8,}$/;
    const validateEmail = /^\S+@\S+\.\S+$/;
    const validatePhone = /(84|0[3|5|7|8|9])+([0-9]{8})/;

    const [isShowPassword, setIsShowPassword] = useState(false);
    const [isShowRePassword, setIsShowRePassword] = useState(false);
    const [loadingAPI, setLoadingAPI] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        document.title = "Đăng ký";
        const vietQR = new VietQR({
            clientID: 'c85abbbb-a5c5-4b39-a06c-7aa8c96619ef',
            apiKey: 'd5f1895a-f19e-4d2b-bc18-9a2e8120a167'
        });

        vietQR.getBanks()
        .then(response => {
            // Kiểm tra xem response có trường data không
            if (response.data) {
                // Gán danh sách ngân hàng từ trường data vào state
                setBanks(response.data);
            } else {
                console.error('No data field found in response:', response);
            }
        })
        .catch(error => {
            console.error('Error fetching banks:', error);
        });
    }, [])
    /*
          Created by: DucNHM
          Created on: 02/03/2024 (dd/mm/yyyy)
          Description: This function a register
          File: $/sep490_g3_fe\src\components\Register.js
          ---
          Modified by: DucNHM
          Date modified: 05/03/2024 (dd/mm/yyyy)
          ...
    */
    const handleRegister = async () => {
        setLoadingAPI(true);
        if (!fullName || !email || !phone || !userName || !password || !rePassword) {
            toast.error("Điền đầy đủ các trường!");
            setLoadingAPI(false);
            return;
        }
        if (!validateEmail.test(email)) {
            toast.error("Định dạng email sai");
            setLoadingAPI(false);
            return;
        }
        if (!validatePhone.test(phone)) {
            toast.error("Định dạng số điện thoại sai");
            setLoadingAPI(false);
            return;
        }
        if (password !== rePassword) {
            toast.error("Mật khẩu nhập lại không giống!");
            setLoadingAPI(false);
            return;
        }
        if (!validatePassword.test(password)) {
            toast.error("Mật khẩu cần ít nhất 1 chữ cái viết thường, 1 chữ cái viết hoa, 1 chữ số và 1 kí tự đặc biệt");
            setLoadingAPI(false);
            return;
        }
        let res = await postRegister(fullName, userName, password, phone, email, taxCode, citizenIdentificationCard, bankAccountNumber, selectedBank);
        if (res.statusCode === 200) {
            toast.success("Đăng ký thành công!");
            navigate("/");
        }
        else if (res.status === 400) {
            toast.error("Tài khoản đã tồn tại!");
        }
        setLoadingAPI(false);
    }
    /*
              Created by: DucNHM
              Created on: 02/03/2024 (dd/mm/yyyy)
              Description: This method enter to register
              File: $/sep490_g3_fe\src\components\Register.js
              ---
              Modified by: 
              ...
        */
    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleRegister();
        }
    }

    const handleGoBack = () => {
        navigate("/");
    }

    const handleSelectChange = (event) => {
        setSelectedBank(event.target.value);
    };

    return (
        <>
            { <div className='register-container col-12 col-sm-6 col-md-6 col-lg-4 col-xxl-4'>
                <div className="title mb-5">Đăng ký</div>
                <input
                    type='text'
                    placeholder='Họ và tên'
                    value={fullName}
                    onChange={(event) => setFullName(event.target.value)}
                />
                <input
                    type='email'
                    placeholder='Email'
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
                <input
                    type='text'
                    placeholder='Số điện thoại'
                    value={phone}
                    onChange={(event) => setPhone(event.target.value)}
                />
                <input
                    type='text'
                    placeholder='Tên tài khoản'
                    value={userName}
                    onChange={(event) => setUserName(event.target.value)}
                />
                <div className='input-password'>
                    <input
                        type={isShowPassword === true ? "text" : "password"}
                        placeholder='Mật Khẩu'
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                    <i className={isShowPassword === true ? "fa-solid fa-eye" : "fa-solid fa-eye-slash"}
                        onClick={() => setIsShowPassword(!isShowPassword)}
                    ></i>
                </div>
                <div className='input-rePassword'>
                    <input
                        type={isShowRePassword === true ? "text" : "password"}
                        placeholder='Nhập lại mật khẩu'
                        value={rePassword}
                        onChange={(event) => setRePassword(event.target.value)}
                        onKeyDown={(event) => handlePressEnter(event)}
                    />
                    <i className={isShowRePassword === true ? "fa-solid fa-eye" : "fa-solid fa-eye-slash"}
                        onClick={() => setIsShowRePassword(!isShowRePassword)}>
                    </i>
                </div>
                <input
                    type='text'
                    placeholder='Mã số thuế'
                    value={taxCode}
                    onChange={(event) => setTaxCode(event.target.value)}
                />
                <input
                    type='text'
                    placeholder='Số CCCD'
                    value={citizenIdentificationCard}
                    onChange={(event) => setCitizenIdentificationCard(event.target.value)}
                />
                <input
                    type='text'
                    placeholder='Số tài khoản ngân hàng'
                    value={bankAccountNumber}
                    onChange={(event) => setBankAccountNumber(event.target.value)}
                />
                <label htmlFor="bankDropdown">Chọn ngân hàng:</label>
                <select className='form-control' id="bankDropdown" value={selectedBank} onChange={handleSelectChange}>
                <option value="">Chọn ngân hàng</option>
                {banks.map(bank => (
                    <option key={bank.bin} value={bank.bin}>{bank.name}</option>
                    ))}
                </select>
                <button
                    onClick={handleRegister}
                >
                    {loadingAPI && <i className="fa-solid fa-circle-notch fa-spin"></i>}
                    &nbsp;Đăng ký
                </button>
                <div className='back'>
                    <span onClick={() => handleGoBack()}>
                        <i className="fa-solid fa-angle-left">
                        </i>&nbsp;Quay lại
                    </span>
                </div>
            </div> }
        </>
    );
}

export default Register;