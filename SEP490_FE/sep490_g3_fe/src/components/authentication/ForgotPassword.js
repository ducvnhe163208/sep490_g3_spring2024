import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { postForgotPassword } from '../../services/UserServices';

function ForgotPassword() {

    const [userName, setUserName] = useState("");
    const navigate = useNavigate();

    const handleForgotPassword = () => {
        if (!userName) {
            toast.error("Tài khoản không được để trống");
            return;
        }
        let res_forgot = postForgotPassword(userName);
        if (res_forgot) {
            toast.success("Thay đổi mật khẩu thành công, vui lòng kiểm tra email của bạn");
            navigate("/login");
        }
        else {
            toast.error("Thay đổi mật khẩu không thành công");
        }
    }

    const handleGoBack = () => {
        navigate("/login");
    }

    return (
        <>
            <div className='login-container col-12 col-sm-6 col-md-6 col-lg-4 col-xxl-4'>
                <div className="title mb-5">Quên mật khẩu</div>
                <input
                    type='text'
                    placeholder='Tên tài khoản'
                    value={userName}
                    onChange={(event) => setUserName(event.target.value)}
                />
                <button
                    onClick={() => handleForgotPassword()}
                >
                    Xác nhận
                </button>
                <div className='back'>
                    <span onClick={() => handleGoBack()}>
                        <i className="fa-solid fa-angle-left">
                        </i>&nbsp;Quay lại
                    </span>
                </div>
            </div>
        </>
    );
}

export default ForgotPassword;