import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../../context/UserContext';
import { getAdminById, putSaveProfile } from '../../services/UserServices';
import { Button } from 'react-bootstrap';
import ChangePassword from './ChangePassword';
import { toast } from 'react-toastify';
import { VietQR } from 'vietqr';

function UserProfile() {

    const { user } = useContext(UserContext);

    const [isShowModalChangePassword, setIsShowModalChangePassword] = useState(false);

    const [fullName, setFullName] = useState("");
    const [avatar, setAvatar] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [taxCode, setTaxCode] = useState("");
    const [citizenIdentificationCard, setCitizenIdentificationCard] = useState("");
    const [bankAccountNumber, setBankAccountNumber] = useState("");
    const [bankBin, setBankBin] = useState("");
    const [banks, setBanks] = useState([]);


    useEffect(() => {
        getUserById();

        const vietQR = new VietQR({
            clientID: 'c85abbbb-a5c5-4b39-a06c-7aa8c96619ef',
            apiKey: 'd5f1895a-f19e-4d2b-bc18-9a2e8120a167'
        });

        vietQR.getBanks()
        .then(response => {
            // Kiểm tra xem response có trường data không
            if (response.data) {
                // Gán danh sách ngân hàng từ trường data vào state
                setBanks(response.data);
            } else {
                console.error('No data field found in response:', response);
            }
        })
        .catch(error => {
            console.error('Error fetching banks:', error);
        });
    }, [])

    const getUserById = async () => {
        let res = await getAdminById(localStorage.getItem("userid"));
        if (res) {
            setFullName(res.fullName);
            setAvatar(res.avatar);
            setPhone(res.phone);
            setEmail(res.email);
            setTaxCode(res.taxCode);
            setCitizenIdentificationCard(res.citizenIdentificationCard);
            setBankAccountNumber(res.bankAccountNumber);
            setBankBin(res.bankBin);
        }
    }

    const handleClose = () => {
        setIsShowModalChangePassword(false);
    }

    const handleSaveProfile = () => {
        let res_save = putSaveProfile(1, fullName, avatar, phone, email, taxCode, citizenIdentificationCard, bankAccountNumber, bankBin);
        if (res_save) {
            toast.success("Lưu hồ sơ thành công!");
        }
        else {
            toast.error("Lưu hồ sơ thất bại!");
        }
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onloadend = () => {
            setAvatar(reader.result);
        };
        reader.readAsDataURL(file);
    };

    const handleSelectChange = (event) => {
        setBankBin(event.target.value);
    };

    return (
        <>
            <span className='d-flex justify-content-center my-3 h1'><b>Hồ sơ người dùng</b></span>
            <div className="container rounded bg-white mt-5 mb-5">
                <div className="row">
                    <div className="col-md-4 border-right">
                        <div className="d-flex flex-column align-items-center text-center p-3 py-5">
                            <img className="rounded-circle mt-5" width="150px" src={avatar} />
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                                style={{ display: 'none' }}
                            />
                            <br />
                            <label
                                style={{ cursor: 'pointer' }}
                                className="btn btn-dark"
                                htmlFor='file-chosen'>
                                Thay avatar
                            </label>
                            <input type='file' id='file-chosen' hidden
                                onChange={handleFileChange}
                            />
                            <br /><br />
                            <div className="text-center"><Button className="btn btn-warning profile-button" type="button" onClick={() => setIsShowModalChangePassword(true)}>Đổi mật khẩu</Button></div>
                            <span> </span>
                        </div>
                    </div>
                    <div className="col-md-6 border-right">
                        <div className="p-3 py-5">
                            <div className="row mt-3">
                                <div className="col-md-12"><label className="labels">Họ và tên</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={fullName}
                                        onChange={(event) => setFullName(event.target.value)}
                                    />
                                </div>
                                <div className="col-md-12 mt-2"><label className="labels">Email</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={email}
                                        onChange={(event) => setEmail(event.target.value)}
                                    />
                                </div>
                                <div className="col-md-12 mt-2"><label className="labels">Số điện thoại</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={phone}
                                        onChange={(event) => setPhone(event.target.value)}
                                    />
                                </div>
                                <div className="col-md-12 mt-2"><label className="labels">Mã số thuế</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={taxCode}
                                        onChange={(event) => setTaxCode(event.target.value)}
                                    />
                                </div>
                                <div className="col-md-12 mt-2"><label className="labels">Số căn cước công dân</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={citizenIdentificationCard}
                                        onChange={(event) => setCitizenIdentificationCard(event.target.value)}
                                    />
                                </div>
                                <div className="col-md-12 mt-2"><label className="labels">Số tài khoản ngân hàng</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={bankAccountNumber}
                                        onChange={(event) => setBankAccountNumber(event.target.value)}
                                    />
                                </div>
                                <div className="col-md-12 mt-2"><label className="labels">Mã ngân hàng</label>
                                    <select className='form-control' id="bankDropdown" value={bankBin} onChange={handleSelectChange}>
                                        <option value="">Chọn ngân hàng</option>
                                        {banks.map(bank => (
                                            <option key={bank.bin} value={bank.bin}>{bank.name}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div className="mt-5 text-center">
                                <Button className="btn btn-primary profile-button" type="button" onClick={() => handleSaveProfile()}>Lưu hồ sơ</Button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <ChangePassword
                isShow={isShowModalChangePassword}
                handleClose={handleClose}
            />
        </>
    );
}

export default UserProfile;