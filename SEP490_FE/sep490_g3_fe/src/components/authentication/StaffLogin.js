import { React, useState, useContext, useEffect } from 'react';
import { toast } from 'react-toastify';
import { getStaffLogin } from '../../services/HomeServices';
import { UserContext } from '../../context/UserContext';
import { useNavigate } from 'react-router-dom'

function StaffLogin() {

    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isShowPassword, setIsShowPassword] = useState(false);
    const [loadingAPI, setLoadingAPI] = useState(false);

    const { loginContext } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        document.title = "Đăng nhập";
        let token = localStorage.getItem("token");
        if (token) {
            window.location.href = "/dashboard";
        }
    }, [])

    const handleLogin = async () => {
        if (!phone) {
            toast.error("Số điện thoại không được để trống");
            setLoadingAPI(false);
            return;
        }
        if (!email) {
            toast.error("Tài khoản không được để trống");
            setLoadingAPI(false);
            return;
        }
        if (!password) {
            toast.error("Mật khẩu không được để trống");
            setLoadingAPI(false);
            return;
        }
        setLoadingAPI(true);
        let res = await getStaffLogin(phone, email, password);
        if (res && res.token) {
            console.log(res);
            loginContext(res.userid, email, res.token, res.phoneNumber);
            localStorage.setItem("role", res.role);
            toast.success("Đăng nhập thành công");
            navigate("/dashboard");
        }
        if (res && res.status === 401) {
            toast.error("Sai tài khoản hoặc mật khẩu");
            setLoadingAPI(false);
            return;
        }
        if (res && res.status === 400) {
            toast.error("Vui lòng kiểm tra số điện thoại");
            setLoadingAPI(false);
            return;
        }
        setLoadingAPI(false);
    }

    const handleGoBack = () => {
        navigate("/");
    }

    const handleAdminLogin = () => {
        navigate("/login");
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleLogin();
        }
    }

    return (
        <>
            <div className='login-container col-12 col-sm-6 col-md-6 col-lg-4 col-xxl-4'>
                <div className="title mb-5">Đăng nhập <br /> cho nhân viên</div>
                <input
                    type='text'
                    placeholder='Số điện thoại'
                    value={phone}
                    onChange={(event) => setPhone(event.target.value)}
                />
                <input
                    type='text'
                    placeholder='Tên tài khoản'
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
                <div className='input-password'>
                    <input
                        type={isShowPassword === true ? "text" : "password"}
                        placeholder='Mật khẩu'
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                        onKeyDown={(event) => handlePressEnter(event)}
                    />
                    <i className={isShowPassword === true ? "fa-solid fa-eye" : "fa-solid fa-eye-slash"}
                        onClick={() => setIsShowPassword(!isShowPassword)}
                    ></i>
                </div>
                <button
                    onClick={() => handleLogin()}
                >
                    {loadingAPI && <i className="fa-solid fa-circle-notch fa-spin"></i>}
                    &nbsp;Đăng nhập
                </button>
                <div className='back'>
                    <span onClick={() => handleGoBack()}>
                        <i className="fa-solid fa-angle-left">
                        </i>&nbsp;Quay lại
                    </span>
                    <span onClick={() => handleAdminLogin()}>
                        &nbsp; | &nbsp;Đăng nhập với chủ shop
                    </span>
                </div>
            </div>
        </>
    );
}

export default StaffLogin;