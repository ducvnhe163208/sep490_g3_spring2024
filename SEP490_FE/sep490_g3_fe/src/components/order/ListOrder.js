import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { getAllOrder } from '../../services/OrderServices';
import ReactPaginate from 'react-paginate';

function ListOrder() {

    const [listOrder, setListOrder] = useState([]);

    const [totalOrderPages, setTotalOrderPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);

    const [keyword, setKeyword] = useState("");

    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };

    const handleClose = () => {
        //setIsShowModalEditOrder(false);
        //setIsShowModalDeleteProduct(false);
    }
    

    useEffect(() => {
        document.title = "Lịch sử giao dịch";
        getOrder(1);
    }, [])

    const handlePageClick = (event) => {
        getOrder(+event.selected + 1);
        setCurrentPage(+event.selected);
    }

    const getOrder = async (page, date) => {
        let res = await getAllOrder(page, date );
        if (res) {
            setTotalOrderPages(res.totalPages);
            setListOrder(res.data);
        }
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleSearch();
        }
    }

    const handleSearch = (event) => {
        getOrder(1, event.target.value);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Danh sách giao dịch</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <input
                    className='form-control'
                    placeholder='Tìm theo tên...'
                    type='date'
                    //onKeyDown={(event) => handlePressEnter(event)}
                    onChange={(event) => handleSearch(event)}
                />
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên khách hàng</span>
                            </th>
                            <th>
                                <span>Nhân viên thu ngân</span>
                            </th>
                            <th>
                                <span>Ngày thực hiện</span>
                            </th>
                            <th>
                                <span>Tổng tiền</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listOrder) && listOrder.length > 0 &&
                            listOrder.map((item, index) => {
                                return (
                                    <tr key={`Orders-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.customerName}</td>
                                        <td>{item.staffName}</td>
                                        <td>{item.oderDate}</td>
                                        <td>{formatNumber(item.totalMoney)}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center  mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    //forcePage={currentPage}
                    pageCount={totalOrderPages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"   
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"  
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
        </>
    );
}

export default ListOrder;