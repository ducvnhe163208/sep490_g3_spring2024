import { React } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putDeleteCategory } from '../../services/CategoryServices';
import { toast } from 'react-toastify';

function DeleteCategory({ isShow, handleClose, dataCategoryDelete, updateCategory }) {

    const handleDeleteSupplier = async () => {
        let res = await putDeleteCategory(dataCategoryDelete.categoryId);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Thay đổi trạng thái thành công");
            updateCategory();
        }
        else {
            toast.error("Thay đổi trạng thái thất bại");
        }
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div>
                        Bạn có muốn thay đổi trạng thái danh mục <b>{dataCategoryDelete.categoryName}</b> không?<br />
                        Hành động không này thể hoàn tác!
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Huỷ
                    </Button>
                    <Button variant="danger" onClick={() => handleDeleteSupplier()}>
                        Thay đổi
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default DeleteCategory;