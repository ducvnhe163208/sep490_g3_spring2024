import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putEditCategory } from '../../services/CategoryServices';
import { toast } from 'react-toastify';

function EditCategory({ isShow, handleClose, dataCategoryEdit, updateCategory }) {

    const [categoryId, setCategoryId] = useState("");
    const [categoryName, setCategoryName] = useState("");

    const handleEditCategory = async () => {
        let res = await putEditCategory(categoryId, categoryName);
        if (res && res.statusCode == 200) {
            handleClose();
            toast.success("Sửa danh mục thành công!");
            updateCategory();
        }
        else {
            toast.error("Sửa danh mục thất bại!");
        }
    }


    useEffect(() => {
        if (isShow) {
            setCategoryId(dataCategoryEdit.categoryId);
            setCategoryName(dataCategoryEdit.categoryName);
        }
    }, [dataCategoryEdit])

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Chỉnh sửa danh mục</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên danh mục</label>
                            <input
                                type="text"
                                className="form-control"
                                value={categoryName}
                                onChange={(event) => setCategoryName(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleEditCategory()}>
                        Sửa
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default EditCategory;