import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { getAllCategory } from '../../services/CategoryServices';
import ReactPaginate from 'react-paginate';
import AddCategory from './AddCategory';
import EditCategory from './EditCategory';
import DeleteCategory from './DeleteCategory';

function ListCategory() {

    const [listCategory, setListCategory] = useState([]);

    const [totalCategoryPages, setTotalCategoryPages] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const [keyword, setKeyword] = useState("");

    const [isShowModalAddCategory, setIsShowModalAddCategory] = useState(false);

    const [isShowModalEditCategory, setIsShowModalEditCategory] = useState(false);
    const [dataCategoryEdit, setDataCategoryEdit] = useState("");

    const [isShowModalDeleteCategory, setIsShowModalDeleteCategory] = useState(false);
    const [dataCategoryDelete, setDataCategoryDelete] = useState("");

    const handleClose = () => {
        setIsShowModalAddCategory(false);
        setIsShowModalEditCategory(false);
        setIsShowModalDeleteCategory(false);
    }

    useEffect(() => {
        document.title = "Danh mục";
        getCategory(1);
    }, [])

    const getCategory = async (page, keyword) => {
        let res = await getAllCategory(page, keyword);
        if (res) {
            setTotalCategoryPages(res.totalPages);
            setListCategory(res.data);
        }
    }

    const handlePageClick = (event) => {
        getCategory(+event.selected + 1);
        setCurrentPage(+event.selected);
    }

    const updateCategory = () => {
        getCategory(currentPage + 1, keyword);
    }

    const handleEditCategory = (category) => {
        setDataCategoryEdit(category);
        setIsShowModalEditCategory(true);
    }

    const handleDeleteCategory = (category) => {
        setDataCategoryDelete(category);
        setIsShowModalDeleteCategory(true);
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleSearch();
        }
    }

    const handleSearch = () => {
        getCategory(1, keyword);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Danh sách danh mục</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <input
                    className='form-control'
                    placeholder='Tìm theo tên danh mục...'
                    onKeyDown={(event) => handlePressEnter(event)}
                    onChange={(event) => setKeyword(event.target.value)}
                />
                <Button className='btn btn-success my-2' onClick={() => setIsShowModalAddCategory(true)}>
                    <i className="fa-solid fa-circle-plus"></i>&nbsp;
                    Thêm danh mục
                </Button>
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên danh mục</span>
                            </th>
                            <th>
                                <span>Tuỳ chỉnh</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listCategory) && listCategory.length > 0 &&
                            listCategory.map((item, index) => {
                                return (
                                    <tr key={`categories-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.categoryName}</td>
                                        <td>
                                            <Button
                                                className='btn btn-warning mx-1 my-1'
                                                onClick={() => handleEditCategory(item)}
                                            >
                                                Sửa
                                            </Button>
                                            <Button
                                                className='btn btn-danger mx-1 my-1'
                                                onClick={() => handleDeleteCategory(item)}
                                            >
                                                {item.active === true ? "Disable" : "Enable"}
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center  mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalCategoryPages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <AddCategory
                isShow={isShowModalAddCategory}
                handleClose={handleClose}
                updateCategory={updateCategory}
            />

            <EditCategory
                isShow={isShowModalEditCategory}
                handleClose={handleClose}
                dataCategoryEdit={dataCategoryEdit}
                updateCategory={updateCategory}
            />
            <DeleteCategory
                isShow={isShowModalDeleteCategory}
                handleClose={handleClose}
                dataCategoryDelete={dataCategoryDelete}
                updateCategory={updateCategory}
            />
        </>
    );
}

export default ListCategory;