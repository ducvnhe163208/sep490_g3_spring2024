import { React, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { postAddCategory } from '../../services/CategoryServices';
import { toast } from 'react-toastify';

function AddCategory({ isShow, handleClose, updateCategory }) {

    const [categoryName, setCategoryName] = useState("");

    const handleAddCategory = async () => {
        let res = await postAddCategory(categoryName);
        if (res) {
            handleClose();
            setCategoryName('');
            toast.success("Thêm danh mục thành công!");
            updateCategory();
        }
        else {
            toast.error("Thêm danh mục thất bại!");
        }
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Thêm danh mục</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên danh mục</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên danh mục"
                                value={categoryName}
                                onChange={(event) => setCategoryName(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleAddCategory()}>Thêm</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default AddCategory;