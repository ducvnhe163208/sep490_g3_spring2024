import { React, useEffect, useRef, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { getAllProduct, getAllProductWithPaging, postAddProduct } from '../../services/ProductServices'
import { getCateIdByname } from '../../services/CategoryServices';
import { getSupIdByName } from '../../services/SupplierServices';
import { CSVLink } from "react-csv";
import { toast } from 'react-toastify';
import ReactPaginate from 'react-paginate';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import DeleteProduct from './DeleteProduct';
import Papa from "papaparse"
import ReactToPrint from 'react-to-print'
import Barcode from 'react-jsbarcode'


function ListProduct() {

    const [listProduct, setListProduct] = useState([]);
    const [listProductExport, setListProductExport] = useState([]);

    const [dataExport, setDataExport] = useState([]);

    const [totalProductPages, setTotalProductPages] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const [keyword, setKeyword] = useState("");
    const [categoryId, setCategoryId] = useState("");

    const [isShowModalAddProduct, setIsShowModalAddProduct] = useState(false);

    const [isShowModalEditProduct, setIsShowModalEditProduct] = useState(false);
    const [dataProductEdit, setDataProductEdit] = useState("");

    const [isShowModalDeleteProduct, setIsShowModalDeleteProduct] = useState(false);
    const [dataProductDelete, setDataProductDelete] = useState("");

    const [supplierId, setSupplierId] = useState("");
    const barcodeRefs = useRef(Array(listProduct.length).fill(null));

    const barcodeOptions = {
        format: "CODE128", // Loại mã vạch
        width: 2, // Độ rộng của các dòng trong mã vạch
        height: 50, // Chiều cao của mã vạch
        displayValue: true // Hiển thị giá trị trên mã vạch
    };

    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };

    const handleClose = () => {
        setIsShowModalAddProduct(false);
        setIsShowModalEditProduct(false);
        setIsShowModalDeleteProduct(false);
    }

    useEffect(() => {
        document.title = "Sản phẩm";
        getProduct(1);
    }, [])

    useEffect(() => {
        getProductToExport();
    }, [])

    const getProduct = async (page, keyword, categoryId) => {
        let res = await getAllProductWithPaging(page, keyword, categoryId);
        if (res) {
            setTotalProductPages(res.totalPages);
            setListProduct(res.data);
        }
    }

    const getProductToExport = async () => {
        let res = await getAllProduct();
        if (res) {
            setListProductExport(res);
        }
    }

    const getSupId = async (search) => {
        let res = await getSupIdByName(search);
        if (res) {
            return res;
        }
    }

    const getCateId = async (search) => {
        let res = await getCateIdByname(search);
        if (res) {
            return res;
        }
    }

    const handlePageClick = (event) => {
        getProduct(+event.selected + 1, keyword, categoryId);
        setCurrentPage(+event.selected);
    }

    const updateProduct = () => {
        getProduct(currentPage + 1, keyword, categoryId);
    }

    const handleEditProduct = (product) => {
        setDataProductEdit(product);
        setIsShowModalEditProduct(true);
    }

    const handleDeleteProduct = (product) => {
        setDataProductDelete(product);
        setIsShowModalDeleteProduct(true);
    }

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            handleSearch();
        }
    }

    const handleSearch = () => {
        getProduct(1, keyword, categoryId);
    }

    const getProductExport = (event, done) => {
        let result = [];
        //if (listProductExport && listProductExport.length > 0) {
        result.push(["Tên sản phẩm", "Danh mục", "Nhà cung cấp", "Giá", "Số lượng", "Mô tả", "Đơn vị"])
        listProductExport.map((item, index) => {
            let arr = [];
            arr[0] = item.productName;
            arr[1] = item.cateName;
            arr[2] = item.supName;
            arr[3] = item.unitPrice;
            arr[4] = item.unitInStock;
            arr[5] = item.description;
            arr[6] = item.unit;
            result.push(arr);
        })
        setDataExport(result);
        done();
        //}
    }

    const handleImportCSV = async (event) => {
        if (event.target && event.target.files && event.target.files[0]) {
            let file = event.target.files[0];
            if (file.type != "text/csv") {
                toast.error("Chỉ nhận file định dạng csv")
                return;
            }
            Papa.parse(file, {
                complete: async function (results) {
                    let rawCSV = results.data;
                    if (rawCSV.length > 0) {
                        if (rawCSV[0] && rawCSV[0].length === 7) {
                            if (rawCSV[0][0] !== "Tên sản phẩm"
                                || rawCSV[0][1] !== "Danh mục"
                                || rawCSV[0][2] !== "Nhà cung cấp"
                                || rawCSV[0][3] !== "Giá"
                                || rawCSV[0][4] !== "Số lượng"
                                || rawCSV[0][5] !== "Mô tả"
                                || rawCSV[0][6] !== "Đơn vị"
                            ) {
                                toast.error("Sai tiêu đề trong file CSV");
                            }
                            else {
                                let result = [];
                                for (let index = 1; index < rawCSV.length; index++) {
                                    const item = rawCSV[index];
                                    if (item.length === 7) {
                                        let obj = {};
                                        obj.productName = item[0];
                                        obj.cateId = item[1];
                                        obj.supId = item[2];
                                        obj.unitPrice = item[3];
                                        obj.unitInStock = item[4];
                                        obj.description = item[5];
                                        obj.unit = item[6];
                                        result.push(obj);
                                    }
                                }
                                const IdPromises = result.map(async (item) => {
                                    const cateId = await getCateId(item.cateId);
                                    const supId = await getSupId(item.supId);
                                    item.cateId = cateId;
                                    item.supId = supId;
                                });

                                await Promise.all(IdPromises);
                                result.forEach(product => {
                                    postAddProduct(
                                        localStorage.getItem("phone"),
                                        product.cateId,
                                        product.supId,
                                        product.productName,
                                        product.unitPrice,
                                        product.unitInStock,
                                        product.description,
                                        null,
                                        product.unit
                                    ).then(response => {
                                        if (response.status === 400) {
                                            toast.error(`${response.data === "Product is exist!" ? "Sản phẩm đã tồn tại" : 'Vui lòng kiểm tra lại dữ liệu'}`);
                                        }
                                        else {
                                            toast.success("Nhập sản phẩm thành công");
                                            updateProduct();
                                        }
                                    }).catch(error => {
                                        toast.error("Lỗi", error)
                                    });
                                });
                            }
                        }
                        else {
                            toast.error("Sai file CSV")
                        }
                    }
                    else {
                        toast.error("Không có dữ liệu trong file")
                    }
                }
            });
        }
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Danh sách sản phẩm</b></span>
            <div className='col-12 col-sm-4 my-3'>

                <input
                    className='form-control mx-2'
                    placeholder='Tìm theo tên...'
                    onKeyDown={(event) => handlePressEnter(event)}
                    onChange={(event) => setKeyword(event.target.value)}
                />
                <Button className='btn btn-success my-2 mx-2' onClick={() => setIsShowModalAddProduct(true)}>
                    <i className="fa-solid fa-circle-plus"></i>&nbsp;
                    Thêm sản phẩm
                </Button>
                <label htmlFor='file-chosen' className='btn btn-warning mx-2'>
                    <i className="fa-solid fa-file-arrow-up"></i>&nbsp;
                    Nhập
                </label>
                <input type='file' id='file-chosen' hidden
                    onChange={(event) => handleImportCSV(event)}
                />
                <CSVLink
                    data={dataExport}
                    filename={"Danh sách sản phẩm.csv"}
                    className="btn btn-primary mx-2"
                    asyncOnClick={true}
                    onClick={getProductExport}
                >
                    <i className="fa-solid fa-file-arrow-down"></i>&nbsp;
                    Xuất
                </CSVLink>
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên sản phẩm</span>
                            </th>
                            <th>
                                <span>Hình ảnh</span>
                            </th>
                            <th>
                                <span>Danh Mục</span>
                            </th>
                            <th>
                                <span>Nhà cung cấp</span>
                            </th>
                            <th>
                                <span>Giá</span>
                            </th>
                            <th>
                                <span>Đơn vị</span>
                            </th>
                            <th>
                                <span>SL kho</span>
                            </th>
                            <th>
                                <span>Trạng thái</span>
                            </th>
                            <th>
                                <span>Tuỳ chỉnh</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listProduct) && listProduct.length > 0 &&
                            listProduct.map((item, index) => {
                                return (
                                    <tr key={`products-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.productName}</td>
                                        <td><img className='supplier-logo' src={item.image} /></td>
                                        <td>{item.cateName}</td>
                                        <td>{item.supName}</td>
                                        <td>{formatNumber(item.unitPrice)}</td>
                                        <td>{item.unit}</td>
                                        <td>{item.unitInStock}</td>
                                        <td>{item.active === true ? <p><i className="fa-solid fa-toggle-on"></i> Đang hợp tác</p> : <p><i className="fa-solid fa-toggle-off"></i> Ngừng hợp tác</p>}</td>
                                        <td>
                                            <div hidden>
                                                <div ref={(ref) => barcodeRefs.current[index] = ref}>
                                                    <Barcode value={item.barcode} options={barcodeOptions} />
                                                </div>
                                            </div>
                                            <ReactToPrint
                                                trigger={() => <button variant="primary" className='btn btn-primary mx-1 my-1'>In mã vạch</button>}
                                                content={() => barcodeRefs.current[index]}
                                            />
                                            <Button
                                                className='btn btn-warning mx-1 my-1'
                                                onClick={() => handleEditProduct(item)}
                                            >
                                                Sửa
                                            </Button>
                                            <Button
                                                className='btn btn-danger mx-1 my-1'
                                                onClick={() => handleDeleteProduct(item)}
                                            >
                                                Xoá
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalProductPages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <AddProduct
                isShow={isShowModalAddProduct}
                handleClose={handleClose}
                updateProduct={updateProduct}
            />
            <EditProduct
                isShow={isShowModalEditProduct}
                handleClose={handleClose}
                dataProductEdit={dataProductEdit}
                updateProduct={updateProduct}
            />
            <DeleteProduct
                isShow={isShowModalDeleteProduct}
                handleClose={handleClose}
                dataProductDelete={dataProductDelete}
                updateProduct={updateProduct}
            />
        </>
    );
}

export default ListProduct;