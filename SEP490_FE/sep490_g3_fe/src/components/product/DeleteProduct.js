import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { deleteDeleteProduct } from '../../services/ProductServices';
import { toast } from 'react-toastify';

function DeleteProduct({ isShow, handleClose, dataProductDelete, updateProduct }) {

    const handleDeleteProduct = async () => {
        let res = await deleteDeleteProduct(dataProductDelete.productId);
        if (res && res.statusCode === 200) {
            handleClose();
            toast.success("Xoá sản phẩm thành công");
            updateProduct();
        }
        else {
            toast.error("Xoá sản phẩm thất bại");
        }
    }

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div>
                        Bạn có muốn xoá sản phẩm <b>{dataProductDelete.productName}</b> không?<br />
                        Hành động không này thể hoàn tác!
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Huỷ
                    </Button>
                    <Button variant="danger" onClick={() => handleDeleteProduct()}>
                        Xoá
                    </Button>
                </Modal.Footer>
            </Modal >
        </>
    );
}

export default DeleteProduct;