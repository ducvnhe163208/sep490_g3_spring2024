import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { postAddProduct } from '../../services/ProductServices';
import { getAllCategoryToAdd } from '../../services/CategoryServices';
import { getAllSupplierToAdd } from '../../services/SupplierServices';
import { NumericFormat } from 'react-number-format';

function AddProduct({ isShow, handleClose, updateProduct }) {

    const [productName, setProductName] = useState("");
    const [cateId, setCateId] = useState("");
    const [supId, setSupId] = useState("");
    const [unitPrice, setUnitPrice] = useState("");
    const [unitInStock, setUnitInStock] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
    const [unit, setUnit] = useState("");

    const [listCategory, setListCategory] = useState("");
    const [listSupplier, setListSupplier] = useState("");

    useEffect(() => {
        getCategory();
    }, [])

    useEffect(() => {
        getSupplier();
    }, [])

    const getCategory = async () => {
        let res_cat = await getAllCategoryToAdd();
        if (res_cat) {
            setListCategory(res_cat);
        }
    }

    const getSupplier = async () => {
        let res_sup = await getAllSupplierToAdd();
        if (res_sup) {
            setListSupplier(res_sup);
        }
    }

    const handleAddProduct = async () => {
        let res = await postAddProduct(
            localStorage.getItem("phone"),
            cateId,
            supId,
            productName,
            unitPrice.replace(/,/g, ''),
            unitInStock,
            description,
            image,
            unit);
        if (res) {
            handleClose();
            setProductName('');
            setCateId('');
            setSupId('');
            setUnitPrice('');
            setUnitInStock('');
            setDescription('');
            setImage('');
            setUnit('');
            toast.success("Thêm sản phẩm thành công!");
            updateProduct();
        }
        else {
            toast.error("Thêm sản phẩm thất bại!");
        }
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onloadend = () => {
                setImage(reader.result);
            };
            reader.readAsDataURL(file);
        }
        else {
            setImage('');
        }
    };

    const handleReset = () => {
        setProductName('');
        setCateId('');
        setSupId('');
        setUnitPrice('');
        setUnitInStock('');
        setDescription('');
        setImage('');
        setUnit('');
        handleClose();
    }
    return (
        <>
            <Modal
                show={isShow}
                onHide={() => handleReset()}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Thêm sản phẩm</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên sản phẩm</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên sản phẩm"
                                value={productName}
                                onChange={(event) => setProductName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Danh mục</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={cateId}
                                onChange={(event) => setCateId(event.target.value)}
                            >
                                <option value="">--Chọn danh mục--</option>
                                {!listCategory && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listCategory) && listCategory.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listCategory) && listCategory.length > 0 &&
                                    listCategory.map((item) => {
                                        return (
                                            <option key={item.categoryId} value={item.categoryId} >{item.categoryName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Nhà cung cấp</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={supId}
                                onChange={(event) => setSupId(event.target.value)}
                            >
                                <option value="">--Chọn nhà cung cấp--</option>
                                {!listSupplier && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listSupplier) && listSupplier.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listSupplier) && listSupplier.length > 0 &&
                                    listSupplier.map((item) => {
                                        return (
                                            <option key={item.supplierId} value={item.supplierId} >{item.supplierName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Giá</label>
                            <NumericFormat
                                type="text"
                                className="form-control"
                                placeholder="Nhập giá"
                                thousandSeparator=","
                                value={unitPrice}
                                onChange={(event) => setUnitPrice(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số lượng tồn kho</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập số lượng tồn kho"
                                value={unitInStock}
                                onChange={(event) => setUnitInStock(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mô tả</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập mô tả"
                                value={description}
                                onChange={(event) => setDescription(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Hình ảnh</label>
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                            />
                            {image && (
                                <div className=' d-flex justify-content-center'>
                                    <img src={image} className='add-image my-3' />
                                </div>
                            )}
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Đơn vị</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập đơn vị"
                                value={unit}
                                onChange={(event) => setUnit(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleReset()}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleAddProduct()}>Thêm</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default AddProduct;