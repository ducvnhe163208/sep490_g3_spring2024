import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putEditProduct } from '../../services/ProductServices';
import { toast } from 'react-toastify';
import { getAllCategoryToAdd } from '../../services/CategoryServices';
import { getAllSupplierToAdd } from '../../services/SupplierServices';
import { NumericFormat } from 'react-number-format';
import removeDiacritics from 'remove-diacritics';
import Barcode from 'react-jsbarcode'

function EditProduct({ isShow, handleClose, dataProductEdit, updateProduct }) {

    const [productId, setProductId] = useState("");
    const [productName, setProductName] = useState("");
    const [cateId, setCateId] = useState("");
    const [supId, setSupId] = useState("");
    const [unitPrice, setUnitPrice] = useState("");
    const [unitInStock, setUnitInStock] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
    const [unit, setUnit] = useState("");

    const [listCategory, setListCategory] = useState("");
    const [listSupplier, setListSupplier] = useState("");

    const barcodeOptions = {
        format: "CODE128", // Loại mã vạch
        width: 2, // Độ rộng của các dòng trong mã vạch
        height: 50, // Chiều cao của mã vạch
        displayValue: true // Hiển thị giá trị trên mã vạch
    };

    useEffect(() => {
        getCategory();
        getSupplier();
    }, [])

    const getCategory = async () => {
        let res_cat = await getAllCategoryToAdd();
        if (res_cat) {
            setListCategory(res_cat);
        }
    }

    const getSupplier = async () => {
        let res_sup = await getAllSupplierToAdd();
        if (res_sup) {
            setListSupplier(res_sup);
        }
    }


    const handleEditProduct = async () => {
        const barcode = `${localStorage.getItem("phone")}-${removeDiacritics(productName)}`;
        let res = await putEditProduct(
            productId,
            cateId,
            supId,
            barcode,
            productName,
            unitPrice,
            unitInStock,
            description,
            image,
            unit);
        if (res && res.statusCode == 200) {
            handleClose();
            toast.success("Sửa sản phẩm thành công!");
            updateProduct();
        }
        else {
            toast.error("Sửa sản phẩm thất bại!");
        }
    }

    const handleFileChange = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onloadend = () => {
            setImage(reader.result);
        };
        reader.readAsDataURL(file);
    };

    useEffect(() => {
        if (isShow) {
            setProductId(dataProductEdit.productId);
            setProductName(dataProductEdit.productName);
            setCateId(dataProductEdit.cateId);
            setSupId(dataProductEdit.supId);
            setUnitPrice(dataProductEdit.unitPrice);
            setUnitInStock(dataProductEdit.unitInStock);
            setDescription(dataProductEdit.description);
            setImage(dataProductEdit.image);
            setUnit(dataProductEdit.unit);
        }
    }, [dataProductEdit])

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Chỉnh sửa sản phẩm</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Tên sản phẩm</label>
                            <input
                                type="text"
                                className="form-control"
                                value={productName}
                                onChange={(event) => setProductName(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Danh mục</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={cateId}
                                onChange={(event) => setCateId(event.target.value)}
                            >
                                {!listCategory && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listCategory) && listCategory.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listCategory) && listCategory.length > 0 &&
                                    listCategory.map((item) => {
                                        <option defaultValue={item.categoryId} value={item.categoryId} >{item.categoryName}</option>
                                        return (
                                            <option key={item.categoryId} value={item.categoryId} >{item.categoryName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Nhà cung cấp</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={supId}
                                onChange={(event) => setSupId(event.target.value)}
                            >
                                {!listSupplier && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listSupplier) && listSupplier.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listSupplier) && listSupplier.length > 0 &&
                                    listSupplier.map((item) => {
                                        <option defaultValue={item.supplierId} value={item.supplierId} >{item.supplierName}</option>
                                        return (
                                            <option key={item.supplierId} value={item.supplierId} >{item.supplierName}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mã vạch</label><br />
                            <div style={{ textAlign: "center" }}>
                                <Barcode value={dataProductEdit.barcode} options={barcodeOptions} width={58} />
                            </div>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Giá</label>
                            <NumericFormat
                                type="text"
                                className="form-control"
                                thousandSeparator=","
                                value={unitPrice}
                                onChange={(event) => setUnitPrice((event.target.value.replace(/,/g, '')))}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Số lượng tồn kho</label>
                            <input
                                type="text"
                                className="form-control"
                                value={unitInStock}
                                onChange={(event) => setUnitInStock(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Mô tả</label>
                            <input
                                type="text"
                                className="form-control"
                                value={description}
                                onChange={(event) => setDescription(event.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Hình ảnh</label>
                            <input
                                type="file"
                                className="form-control"
                                onChange={handleFileChange}
                            />
                            {image && (
                                <div className=' d-flex justify-content-center'>
                                    <img src={image} className='add-image my-3' />
                                </div>
                            )}
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Đơn vị</label>
                            <input
                                type="text"
                                className="form-control"
                                value={unit}
                                onChange={(event) => setUnit(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="warning" onClick={() => handleEditProduct()}>Sửa</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default EditProduct;