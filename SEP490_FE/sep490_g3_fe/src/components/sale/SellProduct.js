import { React, useContext, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { postAddCustomer, getAllCustomerToOrder } from '../../services/CustomerServices';
import { toast } from 'react-toastify';
import { getAllProduct } from '../../services/ProductServices';
import { postAddOrder } from '../../services/OrderServices';
import { UserContext } from '../../context/UserContext';
import { NumericFormat } from 'react-number-format';
import { getAdmin } from '../../services/UserServices';
import axios from 'axios';
import PrintBill from './PrintBill';


function SellProduct() {

    const [listProduct, setListProduct] = useState([]);
    const [keyword, setKeyword] = useState('');
    const { user } = useContext(UserContext);

    const [productOrder, setProductOrder] = useState([]);
    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };

    const [totalPayment, setTotalPayment] = useState(0);
    const [customerPayment, setCustomerPayment] = useState(0);
    const [customerPaymentInput, setCustomerPaymentInput] = useState('');
    const [change, setChange] = useState('');

    const [listCustomer, setListCustomer] = useState([]);
    const [customerId, setCustomerId] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [phone, setPhone] = useState('');
    const [isShowModalPrintBill, setIsShowModalPrintBill] = useState(false);

    const [admin, setAdmin] = useState("");
    const [urlQr, setUrlQr] = useState("");

    useEffect(() => {
        document.title = "Bán hàng";
        getProduct();
        getCustomer();
        getAd();
    }, [])

    useEffect(() => {
        calculateTotalPayment();
    }, [productOrder]);

    const handleClose = () => {
        setIsShowModalPrintBill(false);
    }

    const getAd = async () => {
        let res = await getAdmin();
        if (res) {
            setAdmin(res);
        }
    }

    const generateVietQR = async () => {
        try {
            //var adminInfo = admins.find(admin => admin.adminId === 1);
            // Thông tin cần truyền vào API
            const data = {
                accountNo: admin.bankAccountNumber,
                accountName: admin.fullName,
                acqId: admin.bankBin,
                amount: totalPayment,
                template: 'X3vGfYn'
            };
            // Gửi yêu cầu POST đến API của VietQR
            const response = await axios.post('https://api.vietqr.io/v2/generate', data);
            setUrlQr(response.data.data.qrDataURL);
        } catch (error) {
            console.error('Error generating VietQR:', error);
        }
    };


    const addProductToPayment = (product) => {
        if (product.quantity < 1) {
            toast.error("Số lượng phải phải lớn hơn 0");
            return;
        }
        product.quantity = 1
        const totalPrice = product.quantity === 1 ? product.unitPrice : product.unitPrice * product.quantity;
        setProductOrder((products) => {
            return [...products, { ...product, quantity: 1, totalPrice: totalPrice }];
        })
    }

    const getProduct = async () => {
        let res_pro = await getAllProduct();
        if (res_pro) {
            setListProduct(res_pro);
        }
    }

    const getCustomer = async () => {
        let res_cus = await getAllCustomerToOrder();
        if (res_cus) {
            setListCustomer(res_cus);
        }
    }

    const SearchProduct = (event) => {
        setKeyword(event.target.value);
    }

    const SearchCustomer = (event) => {
        setPhone(event.target.value);
    }

    const onSearchProduct = (searchTerm) => {
        const selectedProduct = listProduct.find(item => item.productName === searchTerm);
        if (selectedProduct) {
            addProductToPayment(selectedProduct);
            setKeyword('');
        }
    }

    const onSearchCustomer = (searchTerm) => {
        const selectedCustomer = listCustomer.find(item => item.phone === searchTerm);
        if (selectedCustomer) {
            setCustomerId(selectedCustomer.customerId);
            setPhone(selectedCustomer.phone);
            setCustomerName(selectedCustomer.customerName);
        }
    }

    const filteredProducts = Array.isArray(listProduct) ? listProduct.filter(item => {
        const searchTerm = keyword.toLowerCase();
        const productName = item.productName.toLowerCase();
        const barCode = item.barcode.toLowerCase();
        return searchTerm && (productName.startsWith(searchTerm) || barCode.startsWith(searchTerm)) && (!productOrder.find(product => product.productName === item.productName) && !productOrder.find(product => product.barcode === item.barCode));
    }) : [];

    const handlePressEnter = (event) => {
        if (event && event.key === 'Enter') {
            const selectedProduct = listProduct.find(item => item.barcode === event.target.value);
            if (selectedProduct && !productOrder.find(product => product.barcode === event.target.value)) {
                addProductToPayment(selectedProduct);
                setKeyword('');
            }
        }
    }

    const filteredCustomers = Array.isArray(listCustomer) ? listCustomer.filter(item => {
        const searchTerm = phone.toLowerCase();
        const phoneSearch = item.phone.toLowerCase();
        return searchTerm && phoneSearch.startsWith(searchTerm);
    }) : [];

    const handleDeleteProduct = (index) => {
        setProductOrder((prevProducts) => {
            const updatedProducts = [...prevProducts];
            updatedProducts.splice(index, 1);
            return updatedProducts;
        });
    }

    const handleChangeInputNumber = (event, index) => {
        if (event.target.value < 1) {
            event.target.value = 1;
        } else if (event.target.value > productOrder[index].unitInStock) {
            event.target.value = productOrder[index].unitInStock;
        }
        const newValue = event.target.value * productOrder[index].unitPrice;
        const updatedProductOrder = [...productOrder];
        updatedProductOrder[index] = {
            ...productOrder[index],
            quantity: event.target.value,
            totalPrice: newValue
        };
        setProductOrder(updatedProductOrder);
    };

    const calculateTotalPayment = () => {
        let total = productOrder.reduce((acc, product) => acc + (product.totalPrice ? product.totalPrice : product.unitPrice), 0);
        setTotalPayment(total);
    };

    const handleCustomerPayment = (event) => {
        const paymentValue = parseInt(event.target.value.replace(/,/g, ''));
        const changeAmount = paymentValue - totalPayment;
        setCustomerPayment(paymentValue);
        setChange(changeAmount >= 0 ? changeAmount : '');
        setCustomerPaymentInput(event.target.value);
    };

    const handleOrderProduct = async () => {
        const phoneRegex = /^[0-9]{10}$/;
        if (productOrder.length === 0) {
            toast.error("Danh sách đơn hàng không được để trống");
            return;
        }
        if (!customerName) {
            toast.error("Khách hàng không được để trống");
            return;
        }
        if (!phoneRegex.test(phone)) {
            toast.error("Sai định dạng số điện thoại");
            return;
        }
        if (!phone) {
            toast.error("Số điện thoại khách hàng không được để trống");
            return;
        }
        if (!customerPaymentInput) {
            toast.error("Số tiền trả lại không được để trống");
            return;
        }
        const orderedProducts = productOrder.map(product => ({
            productId: product.productId,
            quantity: parseFloat(product.quantity ? product.quantity : 1)
        }));
        generateVietQR();
        setIsShowModalPrintBill(true);
        setTimeout(async () => {
            if (customerId === '') {
                let res_add_customer = await postAddCustomer(customerName, phone);
                if (res_add_customer) {
                    let res_order = await postAddOrder(res_add_customer, user.userid, orderedProducts);
                    if (res_order) {
                        setProductOrder([]);
                        setPhone('');
                        setCustomerName('');
                        setCustomerPayment('');
                        setCustomerPaymentInput('');
                        setChange(0);
                        toast.success("Thanh toán thành công!");
                    } else {
                        toast.error("Thanh toán thất bại!");
                    }
                } else {
                    toast.error("Thanh toán thất bại!");
                }
            } else {
                let res_order = await postAddOrder(customerId, user.userid, orderedProducts);
                if (res_order) {
                    setProductOrder([]);
                    setPhone('');
                    setCustomerName('');
                    setCustomerPayment('');
                    setCustomerPaymentInput('');
                    setChange(0);
                    toast.success("Thanh toán thành công!");
                } else {
                    toast.error("Thanh toán thất bại!");
                }
            }
        }, 10000);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1 my-4'><b>Bán hàng</b></span>
            <div className='row my-4'>
                <div className="col-md-4 my-2 search-product-sale">
                    <input
                        className='form-control'
                        placeholder='Tìm theo tên hoặc mã vạch...'
                        type='text'
                        value={keyword}
                        onKeyDown={(event) => handlePressEnter(event)}
                        onChange={(event) => SearchProduct(event)}
                    />
                    <div className='dropdown-sale'>
                        {listProduct && listProduct.length > 0 && keyword !== '' ? (
                            <table className="table">
                                <thead hidden={filteredProducts.length === 0}>
                                    <tr>
                                        <th>Tên sản phẩm</th>
                                        <th>SL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filteredProducts.length === 0 ? (
                                        <tr>
                                            <td colSpan="2">Không có sản phẩm</td>
                                        </tr>
                                    ) : (
                                        filteredProducts.slice(0, 5).map((item, index) => (
                                            <tr key={index} onClick={() => onSearchProduct(item.productName)} className='dropdown-row-sale'>
                                                <td>{item.productName}</td>
                                                <td>{item.unitInStock}</td>
                                            </tr>
                                        ))
                                    )}
                                </tbody>
                            </table>
                        ) : null}
                    </div>
                </div>
                <div className="col-md-4" />
                <div className="col-md-4 my-2 pt-1">
                    {user && user.email && <span><h5><b>Người bán: {user.email}</b></h5></span>}
                </div>
            </div>
            <div className='row my-4'>
                <div className="col-md-7">
                    <div className='customize-table'>
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                    <th>
                                        <span>Tên sản phẩm</span>
                                    </th>
                                    <th>
                                        <span>Số lượng</span>
                                    </th>
                                    <th>
                                        <span>Đơn giá</span>
                                    </th>
                                    <th>
                                        <span>Thành tiền</span>
                                    </th>
                                    <th>
                                        <span>Tuỳ chỉnh</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {productOrder.map((product, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{product.productName}</td>
                                            <td>
                                                <input
                                                    className='form-control'
                                                    type='number'
                                                    min={1}
                                                    max={product.unitInStock}
                                                    defaultValue={1}
                                                    onInput={(event) => { handleChangeInputNumber(event, index) }}
                                                />
                                            </td>
                                            <td>{formatNumber(product.unitPrice)} VNĐ</td>
                                            <td>{product.totalPrice ? `${formatNumber(product.totalPrice)} VNĐ` : `${formatNumber(product.unitPrice)} VNĐ`}</td>
                                            <td><Button className='btn btn-danger' onClick={() => handleDeleteProduct(index)}>Xoá</Button></td>
                                        </tr>
                                    )
                                })
                                }
                            </tbody>
                        </Table >
                    </div >
                    <span className="my-3 text-center"><h3>Tổng tiền hàng: {formatNumber(totalPayment)} VND</h3></span>
                </div >
                <div className="col-md-5 card bill-customer">
                    <span className="my-3 text-center my-4"><h5><b>Thông tin khách hàng</b></h5></span>
                    <div className="mb-3 row">
                        <label className="form-label pt-1 col-4 fw-bold">Số điện thoại</label>
                        <div className='search-product-sale col me-5'>
                            <input
                                type="text"
                                className="form-control"
                                value={phone}
                                onChange={(event) => SearchCustomer(event)}
                            />
                            <div className='dropdown-sale'>
                                {listCustomer && listCustomer.length > 0 && phone !== '' ? (
                                    filteredCustomers.length === 0 ? null : (
                                        filteredCustomers.slice(0, 5).map((item, index) => (
                                            phone !== item.phone && (
                                                <div key={index} onClick={() => onSearchCustomer(item.phone)} className='row dropdown-customer-sale'>
                                                    <span className='col'>{item.customerName}</span>
                                                    <span className='col'   >{item.phone}</span>
                                                </div>
                                            )
                                        ))
                                    )
                                ) : null}
                            </div>
                        </div>
                    </div>
                    <div className="mb-3 row my-4">
                        <label className="form-label pt-1 col-4 fw-bold">Tên khách hàng</label>
                        <div className=' col me-5'>
                            <input
                                type="text"
                                className="form-control"
                                value={customerName}
                                onChange={(event) => setCustomerName(event.target.value)}
                            />
                        </div>
                    </div>
                    <div className="mb-3 row my-4">
                        <label className="form-label pt-1 col-4 fw-bold">Tổng tiền t.toán</label>
                        <div className=' col me-5'>
                            <NumericFormat
                                type="text"
                                thousandSeparator=","
                                className="form-control col me-5"
                                value={totalPayment}
                                readOnly
                            />
                        </div>
                        <span className='vnd-input'>VNĐ</span>
                    </div>
                    <div className="mb-3 row my-4">
                        <label className="form-label pt-1 col-4 fw-bold">Tiền khách trả</label>
                        <div className=' col me-5'>
                            <NumericFormat
                                type="text"
                                thousandSeparator=","
                                className="form-control col me-5"
                                value={customerPaymentInput}
                                onChange={(event) => handleCustomerPayment(event)}
                            />
                        </div>
                        <span className='vnd-input'>VNĐ</span>
                    </div>
                    <div className="mb-3 row my-4">
                        <label className="form-label pt-1 col-4 fw-bold">Tiền trả lại</label>
                        <div className=' col me-5'>
                            <input
                                type="text"
                                className="form-control col me-5"
                                readOnly
                                value={formatNumber(change)}
                            />
                        </div>
                        <span className='vnd-input'>VNĐ</span>
                    </div>
                    <span className="text-center my-4">(giá đã bao gồm phí VAT)</span>
                    <Button type="button" className="btn btn-success btn-lg my-3 my-4" onClick={() => handleOrderProduct()}>In hoá đơn</Button>
                </div>
            </div>
            <PrintBill
                isShow={isShowModalPrintBill}
                handleClose={handleClose}
                products={productOrder}
                customer={{
                    name: customerName,
                    phone: phone,
                    totalPayment: totalPayment,
                    customerPayment: customerPayment,
                    change: change
                }}
                urlQr={urlQr}
            />
        </>
    );
}

export default SellProduct;