import { React, useContext, useRef, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { UserContext } from '../../context/UserContext';
import { ReactToPrint } from 'react-to-print'
import titleLogo from '../../assets/title-logo.png'

function PrintBill({ isShow, handleClose, products, customer, urlQr }) {
    const { user } = useContext(UserContext);

    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };
    const componentRef = useRef();

    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Thanh toán</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="invoice-header">
                        <img width="300px" src={urlQr} />
                    </div>
                    <div className="container" style={{ display: 'none' }}>
                        <div className="invoice" style={{ width: '58mm', fontFamily: 'Arial, sans-serif' }} ref={componentRef}>
                            <div className="header">
                                <img width='40px' src={titleLogo} alt="Logo" />
                                <br />
                                <br />
                                <h5>Hoá đơn bán hàng</h5>
                                <hr />
                                <p>In lúc: {`${new Date().getHours()}:${new Date().getMinutes()}`}</p>
                                <p>Tên shop: {user.email}</p>
                                <p>Ngày bán: {`${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`}</p>
                                <hr />
                            </div>
                            <div className="products">
                                <table className="products-table">
                                    <thead>
                                        <tr>
                                            <th>Tên</th>
                                            <th>SL</th>
                                            <th>T.Tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {products.map((product, index) => (
                                            <tr key={index}>
                                                <td>{product.productName}</td>
                                                <td>{product.quantity}</td>
                                                <td>{formatNumber(product.totalPrice)}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <hr />
                            <div className="summary">
                                <table>
                                    <tr>
                                        <td className='header-summary'>T.Tiền</td>
                                        <td>{formatNumber(customer.totalPayment)}</td>
                                    </tr>
                                    <tr>
                                        <td className='header-summary'>Khách trả</td>
                                        <td>{formatNumber(customer.customerPayment)}</td>
                                    </tr>
                                    <tr>
                                        <td className='header-summary'>Trả lại</td>
                                        <td>{formatNumber(customer.change)}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr />
                            <div className="summary">
                                <table>
                                    <tr>
                                        <td className='header-summary'>Tên KH:</td>
                                        <td>{customer.name}</td>
                                    </tr>
                                    <tr>
                                        <td className='header-summary'>SĐT:</td>
                                        <td>{customer.phone}</td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <div style={{ textAlign: 'center' }}    >
                                <img width="100px" src={urlQr} />
                            </div>
                            <br /><br />
                            <div style={{ textAlign: 'center' }}>
                                <i><b>Xin cảm ơn quý khách</b></i>
                            </div >
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <ReactToPrint
                        trigger={() => <Button variant="primary">In</Button>}
                        content={() => componentRef.current}
                    />
                </Modal.Footer>

            </Modal>
        </>
    );
}

export default PrintBill;