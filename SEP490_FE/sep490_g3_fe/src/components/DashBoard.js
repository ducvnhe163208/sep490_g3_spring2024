import { React, useEffect, useState } from 'react';
import { getAllCustomerReport, getReportPurchaseProduct, getReportSaleProduct, getReportStaffDate, getTopRevenueProducts, getTopSaleQuantity } from '../services/ReportServices';
import { Button } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { getAllStaffNoPaging } from '../services/StaffServices';
import { toast } from 'react-toastify';


function DashBoard() {

    const [benefit, setBenefit] = useState("");
    const [totalMoney, setTotalMoney] = useState("");
    const [totalMoneyReportProduct, setTotalMoneyReportProduct] = useState("");
    const [staffReportProductId, setStaffReportProductId] = useState("");
    const [staffMonthlyRevenueId, setStaffMonthlyRevenueId] = useState("");
    const [staffId, setStaffId] = useState("");

    const [startDate, setStartDate] = useState(new Date().toISOString().split('T')[0]);
    const [endDate, setEndDate] = useState(new Date().toISOString().split('T')[0]);

    const [staffReportProductStartDate, setStaffReportProductStartDate] = useState(new Date().toISOString().split('T')[0]);
    const [staffReportProductEndDate, setStaffReportProductEndDate] = useState(new Date().toISOString().split('T')[0]);

    const [staffMonthlyRevenueStartDate, setStaffMonthlyRevenueStartDate] = useState(new Date().toISOString().split('T')[0]);
    const [staffMonthlyRevenueEndDate, setStaffMonthlyRevenueEndDate] = useState(new Date().toISOString().split('T')[0]);

    const [listStaffReport, setListStaffReport] = useState([]);
    const [listCustomerReport, setListCustomerReport] = useState([]);
    const [listSaleQuantity, setListSaleQuantity] = useState([]);
    const [listRevenueProducts, setListRevenueProducts] = useState([]);
    const [listStaff, setListStaff] = useState([]);
    const [listStaffReportPurchaseProduct, setListStaffReportPurchaseProduct] = useState([]);
    const [listReportSaleProduct, setListReportSaleProduct] = useState([]);
    const [listReportPurchaseProduct, setListReportPurchaseProduct] = useState([]);
    const [listStafffMonthlyRevenue, setListStafffMonthlyRevenue] = useState([]);

    const [totalCustomerReportPages, setTotalCustomerReportPages] = useState(1);
    const [currentCustomerReportPage, setCurrentCustomerReportPage] = useState(0);

    const [totalStaffReportProductPages, setTotalStaffReportProductPages] = useState(1);
    const [currentReportPurchaseProductPage, setCurrentReportPurchaseProductPage] = useState(0);

    const [totalReportSaleProductPages, setTotalReportSaleProductPages] = useState(1);
    const [currentReportSaleProductPage, setCurrentReportSaleProductPage] = useState(0);

    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };

    useEffect(() => {
        document.title = "Tổng quan";
        getCustomerReport(1);
        getSaleQuantity();
        getRevenueProducts();
        getStaffNoPaging();
        getAllReportPurchaseProduct(1);
        getAllReportSaleProduct(1);
    }, [])

    const getStaffNoPaging = async () => {
        let res_staff = await getAllStaffNoPaging();
        if (res_staff) {
            setListStaff(res_staff);
            setListStafffMonthlyRevenue(res_staff);
        }
    }

    const getCustomerReport = async (page) => {
        let res_customer_report = await getAllCustomerReport(page);
        if (res_customer_report) {
            setTotalCustomerReportPages(res_customer_report.totalPages);
            setListCustomerReport(res_customer_report.data);
        }
    }

    const getSaleQuantity = async () => {
        let res_sale_quantity = await getTopSaleQuantity();
        if (res_sale_quantity) {
            setListSaleQuantity(res_sale_quantity);
        }
    }

    const getRevenueProducts = async () => {
        let res_revenue_product = await getTopRevenueProducts();
        if (res_revenue_product) {
            setListRevenueProducts(res_revenue_product);
        }
    }

    const getAllReportPurchaseProduct = async (page) => {
        let res_report_purchase_product = await getReportPurchaseProduct(staffReportProductStartDate, staffReportProductEndDate, page);
        if (res_report_purchase_product) {
            setTotalStaffReportProductPages(res_report_purchase_product.totalPages);
            setListReportPurchaseProduct(res_report_purchase_product.data);
        }
    }

    const handlePageClick = (event) => {
        getCustomerReport(+event.selected + 1);
        setCurrentCustomerReportPage(+event.selected);
    }

    const handleStaffReportProductPageClick = (event) => {
        getAllReportPurchaseProduct(+event.selected + 1);
        setCurrentReportPurchaseProductPage(+event.selected);
    }

    const handleViewRevenueMonthByStaff = async () => {
        if (staffMonthlyRevenueStartDate > staffMonthlyRevenueEndDate) {
            toast.error("Từ ngày lớn hơn đến ngày!");
            return;
        }
        let res_staff_report;
        if(localStorage.getItem("role") === null){
            res_staff_report = await getReportStaffDate(staffMonthlyRevenueStartDate, staffMonthlyRevenueEndDate, staffMonthlyRevenueId);
        }else{
            res_staff_report = await getReportStaffDate(staffMonthlyRevenueStartDate, staffMonthlyRevenueEndDate, localStorage.getItem("role"));
        }
        if (res_staff_report) {
            setListStaffReport(res_staff_report.list);
            setBenefit(res_staff_report.benefit);
        }
    }

    const handleViewReportPurchaseProduct = async () => {
        if (staffReportProductEndDate < staffReportProductStartDate) {
            toast.error("Từ ngày lớn hơn đến ngày!");
            return;
        }
        let res_report_purchase_product = await getReportPurchaseProduct(staffReportProductStartDate, staffReportProductEndDate, 1);
        if (res_report_purchase_product) {
            setListReportPurchaseProduct(res_report_purchase_product.data);
            setTotalMoneyReportProduct(res_report_purchase_product.totalMoney);
        }
    }

    const getAllReportSaleProduct = async (page) => {
        let res_report_sale_product = await getReportSaleProduct(startDate, endDate, page);
        if (res_report_sale_product) {
            setTotalReportSaleProductPages(res_report_sale_product.totalPages);
            setListReportSaleProduct(res_report_sale_product.data);
        }
    }

    const handleViewReportSaleProduct = async () => {
        if (startDate > endDate) {
            toast.error("Từ ngày lớn hơn đến ngày!");
            return;
        }
        let res_report_sale_product;
        if(localStorage.getItem("role") === null){
            res_report_sale_product = await getReportSaleProduct(startDate, endDate, 1, staffId);
        }else{
            res_report_sale_product = await getReportSaleProduct(startDate, endDate, 1, localStorage.getItem("role"));
        }
        if (res_report_sale_product) {
            setTotalReportSaleProductPages(res_report_sale_product.totalPages);
            setListReportSaleProduct(res_report_sale_product.data);
            console.log(totalReportSaleProductPages);
        }
    }

    const handleReportSaleProductPageClick = (event) => {
        getAllReportSaleProduct(+event.selected + 1);
        setCurrentReportSaleProductPage(+event.selected);
    }

    return (
        <>
            <main>
                <div className="container pt-4">
                {localStorage.getItem("role") === null &&
                    <div className='row'>
                        <div className='col'>
                            <section className="mb-4">
                                <div className="card">
                                    <div className="card-header py-3">
                                        <h5 className="mb-0 text-center"><strong>Top 10 sản phẩm bán chạy</strong></h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table table-hover text-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">STT</th>
                                                        <th scope="col">Tên sản phẩm</th>
                                                        <th scope="col">Số lượng</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {Array.isArray(listSaleQuantity) && listSaleQuantity.length > 0 &&
                                                        listSaleQuantity.map((item, index) => {
                                                            return (
                                                                <tr key={`sale-quantity-${index}`}>
                                                                    <td style={{ textAlign: 'center' }}>{index + 1}</td>
                                                                    <td>{item.productName}</td>
                                                                    <td style={{ textAlign: 'center' }}>{item.totalQuantity}</td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div className='col'>
                            <section className="mb-4">
                                <div className="card">
                                    <div className="card-header py-3">
                                        <h5 className="mb-0 text-center"><strong>Top 10 sản phẩm có doanh thu cao nhất</strong></h5>
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table table-hover text-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">STT</th>
                                                        <th scope="col">Tên sản phẩm</th>
                                                        <th scope="col">Tổng doanh thu</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {Array.isArray(listRevenueProducts) && listRevenueProducts.length > 0 &&
                                                        listRevenueProducts.map((item, index) => {
                                                            return (
                                                                <tr key={`revenue-product-${index}`}>
                                                                    <td style={{ textAlign: 'center' }}>{index + 1}</td>
                                                                    <td>{item.productName}</td>
                                                                    <td style={{ textAlign: 'center' }}>{formatNumber(item.totalRevenue)}</td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    }
                    <div className='row'>
                        <div className='col'>
                            <section className="mb-4">
                                <div className="card">
                                    <div className="card-header py-3">
                                        <h5 className="mb-0 text-center"><strong>Báo cáo tổng doanh số bán hàng</strong></h5>
                                        <br />
                                        <div className='col d-flex'>
                                            <span className='me-2'>Từ: </span>
                                            <input
                                                className='form-control me-2' type='date'
                                                value={startDate}
                                                onChange={(event) => setStartDate(event.target.value)}
                                            />
                                            <span className='me-2'>Đến: </span>
                                            <input
                                                className='form-control me-2' type='date'
                                                value={endDate}
                                                onChange={(event) => setEndDate(event.target.value)}
                                            />
                                            {localStorage.getItem("role") === null &&
                                            <select
                                                className="form-control me-2"
                                                aria-label="Default select example"
                                                value={staffId}
                                                onChange={(event) => setStaffId(event.target.value)}
                                            >
                                                <option value="">--Tất cả nhân viên--</option>
                                                {!listStaff && <p>Đang tải dữ liệu...</p>}
                                                {Array.isArray(listStaff) && listStaff.length === 0 && "Không có dữ liệu"}
                                                {Array.isArray(listStaff) && listStaff.length > 0 &&
                                                    listStaff.map((item) => {
                                                        return (
                                                            <option key={item.id} value={item.id} >{item.fullName}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            }
                                            <Button onClick={() => handleViewReportSaleProduct()}>Xem</Button>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table table-hover text-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">STT</th>
                                                        <th scope="col">Tên sản phẩm</th>
                                                        <th scope="col">SL</th>
                                                        <th scope="col">Giá</th>
                                                        <th scope="col">Thành tiền</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {Array.isArray(listReportSaleProduct) && listReportSaleProduct.length > 0 &&
                                                        listReportSaleProduct.map((item, index) => {
                                                            return (
                                                                <tr key={`report-sale-product-${index}`}>
                                                                    <td>{index + 1}</td>
                                                                    <td>{item.productName}</td>
                                                                    <td>{item.quantity}</td>
                                                                    <td>{formatNumber(item.unitPrice)}</td>
                                                                    <td>{formatNumber(item.totalMoney)}</td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                        < div className="d-flex justify-content-center  mt-3">
                                            <ReactPaginate
                                                breakLabel="..."
                                                nextLabel="Sau >"
                                                onPageChange={handleReportSaleProductPageClick}
                                                forcePage={currentReportSaleProductPage}
                                                pageCount={Math.ceil(totalReportSaleProductPages)}
                                                previousLabel="< Trước"
                                                pageClassName="page-item"
                                                pageLinkClassName="page-link"
                                                previousClassName="page-item"
                                                previousLinkClassName="page-link"
                                                nextClassName="page-item"
                                                nextLinkClassName="page-link"
                                                breakClassName="page-item"
                                                breakLinkClassName="page-link"
                                                containerClassName="pagination"
                                                activeClassName="active"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        {localStorage.getItem("role") === null && 
                        <div className='col'>
                            <section className="mb-4">
                                <div className="card">
                                    <div className="card-header py-3">
                                        <h5 className="mb-0 text-center"><strong>Báo cáo nhập mua hàng hoá</strong></h5>
                                        <br />
                                        <div className='col d-flex'>
                                            <span className='me-2'>Từ: </span>
                                            <input
                                                className='form-control me-2' type='date'
                                                value={staffReportProductStartDate}
                                                onChange={(event) => setStaffReportProductStartDate(event.target.value)}
                                            />
                                            <span className='me-2'>Đến: </span>
                                            <input
                                                className='form-control me-2' type='date'
                                                value={staffReportProductEndDate}
                                                onChange={(event) => setStaffReportProductEndDate(event.target.value)}
                                            />
                                            <Button onClick={() => handleViewReportPurchaseProduct()}>Xem</Button>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table table-hover text-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">STT</th>
                                                        <th scope="col">Tên sản phẩm</th>
                                                        <th scope="col">SL Nhập</th>
                                                        <th scope="col">SL Tồn kho</th>
                                                        <th scope="col">Giá</th>
                                                        <th scope="col">Thành tiền</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {Array.isArray(listReportPurchaseProduct) && listReportPurchaseProduct.length > 0 &&
                                                        listReportPurchaseProduct.map((item, index) => {
                                                            return (
                                                                <tr key={`report-purchase-product-${index}`}>
                                                                    <td>{index + 1}</td>
                                                                    <td>{item.productName}</td>
                                                                    <td>{item.quantity}</td>
                                                                    <td>{item.unitInStock}</td>
                                                                    <td>{formatNumber(item.unitPrice)}</td>
                                                                    <td>{formatNumber(item.totalMoney)}</td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                        < div className="d-flex justify-content-center  mt-3">
                                            <ReactPaginate
                                                breakLabel="..."
                                                nextLabel="Sau >"
                                                onPageChange={handleStaffReportProductPageClick}
                                                forcePage={currentReportPurchaseProductPage}
                                                pageCount={Math.ceil(totalStaffReportProductPages)}
                                                previousLabel="< Trước"
                                                pageClassName="page-item"
                                                pageLinkClassName="page-link"
                                                previousClassName="page-item"
                                                previousLinkClassName="page-link"
                                                nextClassName="page-item"
                                                nextLinkClassName="page-link"
                                                breakClassName="page-item"
                                                breakLinkClassName="page-link"
                                                containerClassName="pagination"
                                                activeClassName="active"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        }
                    </div>
                    <section className="mb-4">
                        <div className="card">
                            <div className="card-header py-3">
                                <h5 className="mb-0 text-center"><strong>Doanh thu theo tháng của nhân viên</strong></h5>
                                <br />
                                <div className='col d-flex'>
                                    <span className='me-2'>Từ: </span>
                                    <input
                                        className='form-control me-2' type='date'
                                        value={staffMonthlyRevenueStartDate}
                                        onChange={(event) => setStaffMonthlyRevenueStartDate(event.target.value)}
                                    />
                                    <span className='me-2'>Đến: </span>
                                    <input
                                        className='form-control me-2' type='date'
                                        value={staffMonthlyRevenueEndDate}
                                        onChange={(event) => setStaffMonthlyRevenueEndDate(event.target.value)}
                                    />
                                    {localStorage.getItem("role") === null && 
                                    <select
                                        className="form-control me-2"
                                        aria-label="Default select example"
                                        value={staffMonthlyRevenueId}
                                        onChange={(event) => setStaffMonthlyRevenueId(event.target.value)}
                                    >
                                        <option value="">--Tất cả nhân viên--</option>
                                        {!listStafffMonthlyRevenue && <p>Đang tải dữ liệu...</p>}
                                        {Array.isArray(listStafffMonthlyRevenue) && listStafffMonthlyRevenue.length === 0 && "Không có dữ liệu"}
                                        {Array.isArray(listStafffMonthlyRevenue) && listStafffMonthlyRevenue.length > 0 &&
                                            listStafffMonthlyRevenue.map((item) => {
                                                return (
                                                    <option key={item.id} value={item.id} >{item.fullName}</option>
                                                )
                                            })
                                        }
                                    </select>
                                    }
                                    <Button onClick={() => handleViewRevenueMonthByStaff()}>Xem</Button>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table className="table table-hover text-nowrap">
                                        <thead>
                                            <tr>
                                                <th scope="col">STT</th>
                                                <th scope="col">Tên nhân viên</th>
                                                <th scope="col">Tổng số tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {Array.isArray(listStaffReport) && listStaffReport.length > 0 &&
                                                listStaffReport.map((item, index) => {
                                                    return (
                                                        <tr key={`customer-report-${index}`}>
                                                            <td>{index + 1}</td>
                                                            <td>{item.staffName}</td>
                                                            <td>{formatNumber(item.totalMoney)}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <h2 className='d-flex justify-content-end '>Tổng số tiền: {formatNumber(benefit)}</h2>
                                </div>
                            </div>
                        </div>
                    </section>
                    {localStorage.getItem("role") === null && 
                    <section className="mb-4">
                        <div className="card">
                            <div className="card-header text-center py-3">
                                <h5 className="mb-0 text-center">
                                    <strong>Khách hàng đã mua</strong>
                                </h5>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table className="table table-hover text-nowrap">
                                        <thead>
                                            <tr>
                                                <th scope="col">STT</th>
                                                <th scope="col">Tên khách hàng</th>
                                                <th scope="col">Số điện thoại</th>
                                                <th scope="col">Địa chỉ</th>
                                                <th scope="col">Số tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {Array.isArray(listCustomerReport) && listCustomerReport.length > 0 &&
                                                listCustomerReport.map((item, index) => {
                                                    return (
                                                        <tr key={`customer-report-${index}`}>
                                                            <td>{index + 1}</td>
                                                            <td>{item.customerName}</td>
                                                            <td>{item.phone}</td>
                                                            <td>{item.address}</td>
                                                            <td>{formatNumber(item.totalMoney)}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <div className="d-flex justify-content-center  mt-3">
                                        <ReactPaginate
                                            breakLabel="..."
                                            nextLabel="Sau >"
                                            onPageChange={handlePageClick}
                                            forcePage={currentCustomerReportPage}
                                            pageCount={Math.ceil(totalCustomerReportPages)}
                                            previousLabel="< Trước"
                                            pageClassName="page-item"
                                            pageLinkClassName="page-link"
                                            previousClassName="page-item"
                                            previousLinkClassName="page-link"
                                            nextClassName="page-item"
                                            nextLinkClassName="page-link"
                                            breakClassName="page-item"
                                            breakLinkClassName="page-link"
                                            containerClassName="pagination"
                                            activeClassName="active"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    }
                </div >
            </main >
        </>
    );
}

export default DashBoard;