import React from 'react';


function Footer() {
    return (
        <>
            <div className='py-5'></div>
            <div className='py-4'></div>
            <footer className="text-center text-lg-start bg-body-tertiary text-muted ">
                <section className="d-flex justify-content-center justify-content-lg-between p-4 ">
                </section>
                <section className="">
                    <div className="container text-center text-md-start mt-5">
                        <div className="row mt-3">
                            <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                                <h6 className="text-uppercase fw-bold mb-4">
                                    <i className="fas fa-gem me-3"></i>VietBiz
                                </h6>
                                <p>
                                    Phần mềm quản lý bán hàng
                                </p>
                            </div>
                            <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                                <h6 className="text-uppercase fw-bold mb-4">
                                    lập trình
                                </h6>
                                <p>
                                    React JS
                                </p>
                                <p>
                                    C#
                                </p>
                            </div>

                            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                                <h6 className="text-uppercase fw-bold mb-4">Liên hệ</h6>
                                <p><i className="fas fa-home me-3"></i> Đại học FPT</p>
                                <p>
                                    <i className="fas fa-envelope me-3"></i>
                                    ducvnhe163208@fpt.edu.vn
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </footer>
        </>
    );
}

export default Footer;