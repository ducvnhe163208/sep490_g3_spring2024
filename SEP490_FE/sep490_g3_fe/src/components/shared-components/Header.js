import { React, useContext, useState } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import { toast } from 'react-toastify';
import { UserContext } from '../../context/UserContext';
import { useNavigate } from 'react-router-dom'
import ShiftAttend from '../attendance/ShiftAttend';

function Header() {
    const { logout, user } = useContext(UserContext);
    const [activeLink, setActiveLink] = useState("");
    const navigate = useNavigate();
    const [isShowModalAddShift, setIsShowModalAddShift] = useState(false);

    const handleLogout = () => {
        logout();
        toast.success("Đăng xuất thành công");
        navigate("/");
    }

    const handleProfile = () => {
        navigate("/profile");
    }

    const handleCloseAddShift = () => {
        setIsShowModalAddShift(false);
    }

    const handleAttendace = () => {
        setIsShowModalAddShift(true);
    }

    return (
        <>
            <Navbar expand="lg" className="bg-body-tertiary">
                <Container>
                    <NavLink className="mx-2 px-2 my-1 logo" to={user && user.auth === true ? '/dashboard' : '/'} onClick={() => setActiveLink('homepage')}>VietBiz</NavLink>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        {((user && user.auth) || [
                            '/', '/dashboard', '/customer', '/category',
                            '/product', '/supplier', '/sale', '/staff'
                        ].includes(window.location.pathname)) &&
                            <>
                                <Nav className="me-auto">
                                    {user.auth === false ? null :
                                        <>
                                            {localStorage.getItem("role") === null &&
                                                <>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'dashboard' ? 'active' : ''}`} to="/dashboard" onClick={() => setActiveLink('dashboard')}>Tổng quan</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'customer' ? 'active' : ''}`} to="/customer" onClick={() => setActiveLink('customer')}>Khách hàng</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'category' ? 'active' : ''}`} to="/category" onClick={() => setActiveLink('category')}>Danh mục</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'product' ? 'active' : ''}`} to="/product" onClick={() => setActiveLink('product')}>Sản phẩm</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'receipt' ? 'active' : ''}`} to="/receipt" onClick={() => setActiveLink('receipt')}>Mua hàng</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'supplier' ? 'active' : ''}`} to="/supplier" onClick={() => setActiveLink('supplier')}>Nhà cung cấp</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'staff' ? 'active' : ''}`} to="/staff" onClick={() => setActiveLink('staff')}>Nhân Viên</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'sale' ? 'active' : ''}`} to="/sale" onClick={() => setActiveLink('sale')}>Bán hàng</NavLink>
                                                    <NavLink className={`nav-link mx-2 px-2 my-1 ${activeLink === 'order' ? 'active' : ''}`} to="/order" onClick={() => setActiveLink('order')}>Đơn hàng</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'attendance' ? 'active' : ''}`} to="/attendance" onClick={() => setActiveLink('attendance')}>Chấm công</NavLink>
                                                </>
                                            }
                                            {localStorage.getItem("role") !== null &&
                                                <>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'dashboard' ? 'active' : ''}`} to="/dashboard" onClick={() => setActiveLink('dashboard')}>Tổng quan</NavLink>
                                                    <NavLink className={`nav-link mx-1 px-2 my-1 ${activeLink === 'sale' ? 'active' : ''}`} to="/sale" onClick={() => setActiveLink('sale')}>Bán hàng</NavLink>
                                                </>
                                            }

                                        </>

                                    }
                                </Nav>
                                <Nav>
                                    {user.auth === true && <span className='nav-link mx-2 px-2 my-1'>Xin chào, {localStorage.getItem("email")}</span>}
                                    {user && user.auth === true ?
                                        <NavDropdown className='mx-2 px-2 my-1' title="Cài đặt" id="basic-nav-dropdown">
                                            {localStorage.getItem("role") !== null && <NavDropdown.Item onClick={() => handleAttendace()}>Điểm danh</NavDropdown.Item>}
                                            {localStorage.getItem("role") === null && <NavDropdown.Item onClick={() => handleProfile()}>Thông tin người dùng</NavDropdown.Item>}
                                            <NavDropdown.Item onClick={() => handleLogout()}>
                                                Đăng xuất
                                            </NavDropdown.Item>
                                        </NavDropdown>
                                        :
                                        <>
                                            <NavLink className={`nav-link mx-2 px-2 my-1 ${activeLink === 'login' ? 'active' : ''}`} to="/login" onClick={() => setActiveLink('login')}>Đăng nhập</NavLink>
                                            <NavLink className={`nav-link mx-2 px-2 my-1 ${activeLink === 'register' ? 'active' : ''}`} to="/register" onClick={() => setActiveLink('register')}>Đăng ký</NavLink>
                                        </>
                                    }

                                </Nav>
                            </>
                        }
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <ShiftAttend
                isShow={isShowModalAddShift}
                handleCloseAddShift={handleCloseAddShift}
            />
        </>
    );
}

export default Header;