import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom'


function HomePage() {
    useEffect(() => {
        document.title = "Trang chủ";
    }, [])
    return (
        <>
            <div className="py-5">
                <div className="container px-5 pb-5">
                    <div className="row gx-5 align-items-center">
                        <div className="col-xxl-5">
                            <div className="text-center text-xxl-start">
                                <h1 className="display-3 fw-bolder mb-5"><span className="text-gradient d-inline">Phần mềm quản lý<br /> bán hàng VietBiz</span></h1>
                                <div className="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xxl-start mb-3">
                                    <NavLink className="btn btn-primary btn-lg px-5 py-3 me-sm-3 fs-6 fw-bolder" to="/register">Đăng ký ngay</NavLink>
                                    <NavLink className="btn btn-outline-dark btn-lg px-5 py-3 fs-6 fw-bolder" to="/login">Đăng nhập</NavLink>
                                </div>
                            </div>
                        </div>
                        <div className="col-xxl-7">
                            <div className="d-flex justify-content-center mt-5 mt-xxl-0">
                                <div className="profile bg-gradient-primary-to-secondary">
                                    <img className="profile-img" src="homepage.png" width={600} alt="Homepage" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default HomePage;