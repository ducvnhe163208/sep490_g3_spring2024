import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { getAllTimeSlot, postCreateSessions } from '../../services/SalaryServices';
import { toast } from 'react-toastify';


function ShiftAttend({ isShow, handleCloseAddShift }) {

    const [listTimeSlot, setListTimeSlot] = useState([]);
    const [timeSlotId, setTimeSlotId] = useState("");
    const [note, setNote] = useState("");

    const handleReset = () => {
        handleCloseAddShift();
    }

    useEffect(() => {
        getTimeSlot();
    }, [])

    const getTimeSlot = async () => {
        let res_time_slot = await getAllTimeSlot();
        if (res_time_slot) {
            setListTimeSlot(res_time_slot);
        }
    }

    const handleAttend = async () => {
        let res_attend = await postCreateSessions(timeSlotId, note, localStorage.getItem("userid"));
        if (res_attend.statusCode === 200) {
            handleReset();
            toast.success("Điểm thành công!");
        }
        else if (res_attend.status === 400) {
            toast.error("Điểm danh thất bại!");
        }
        else {
            toast.error("Đã điểm danh!");
        }
    }


    return (
        <>
            <Modal
                show={isShow}
                onHide={() => handleReset()}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Điểm danh</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Ca</label>
                            <select
                                className="form-control"
                                aria-label="Default select example"
                                value={timeSlotId}
                                onChange={(event) => setTimeSlotId(event.target.value)}
                            >
                                <option value="">--Ca làm việc--</option>
                                {!listTimeSlot && <p>Đang tải dữ liệu...</p>}
                                {Array.isArray(listTimeSlot) && listTimeSlot.length === 0 && <p>Không có dữ liệu</p>}
                                {Array.isArray(listTimeSlot) && listTimeSlot.length > 0 &&
                                    listTimeSlot.map((item) => {
                                        return (
                                            <option key={item.timeslotId} value={item.timeslotId} >{item.timeslot1}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Ghi chú(nếu có)</label>
                            <input
                                type="text"
                                className="form-control"
                                onChange={(event) => setNote(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleReset()}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleAttend()}>
                        Điểm danh
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ShiftAttend;