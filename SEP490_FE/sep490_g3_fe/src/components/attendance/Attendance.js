import React, { useEffect, useState } from 'react';
import { Button, Table, } from 'react-bootstrap';
import { getAttendByDate, getAttendByMonth, getAttendInDay, getReport } from '../../services/SalaryServices';
import { toast } from 'react-toastify';
import { NumericFormat } from 'react-number-format';
import ReactPaginate from 'react-paginate';


function Attendance() {
    const [startDate, setStartDate] = useState(new Date().toISOString().split('T')[0]);
    const [endDate, setEndDate] = useState(new Date().toISOString().split('T')[0]);

    const [totalStaffAttendByDatePages, setTotalStaffAttendByDatePages] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const [online, setOnline] = useState("");
    const [offline, setOffline] = useState("");

    const [listStaffAttendByDate, setListStaffAttendByDate] = useState([]);

    const [listReport, setListReport] = useState([]);
    const [totalReportStaffPages, setReportStaffPages] = useState(1);
    const [currentReportStaffPage, setCurrentReportStaffPage] = useState(0);

    const [month, setMonth] = useState(new Date().toISOString().split('-').slice(0, 2).join('-'));
    const [commission, setCommission] = useState("");
    const [hardSalary, setHardSalary] = useState("");
    const [listStaffSalaryMonth, setStaffListSalaryMonth] = useState([]);
    const [totalSalaryMonthPages, setSalaryMonthPages] = useState(1);
    const [currentSalaryMonthPage, setCurrentSalaryMonthPage] = useState(0);

    const formatNumber = (number) => {
        return new Intl.NumberFormat('en', { useGrouping: true }).format(number);
    };

    useEffect(() => {
        document.title = "Chấm công";
        getAttendStaffInDay();
        getStaffAttendByDate(startDate, endDate, 1);
        getReportStaff(1);
    }, [])

    const getAttendStaffInDay = async () => {
        let res = await getAttendInDay();
        if (res) {
            setOnline(res.online);
            setOffline(res.offline);
        }
    }

    const getStaffAttendByDate = async (startDate, endDate, pageNumber) => {
        let res_list_attendance_owner = await getAttendByDate(startDate, endDate, pageNumber);
        if (res_list_attendance_owner) {
            setListStaffAttendByDate(res_list_attendance_owner.data);
            setTotalStaffAttendByDatePages(res_list_attendance_owner.totalPages);
        }
    }

    const getReportStaff = async (pageNumber) => {
        let res_list_report_staff = await getReport(pageNumber);
        if (res_list_report_staff) {
            setListReport(res_list_report_staff.data);
            setReportStaffPages(res_list_report_staff.totalPages);
        }
    }

    const getStaffAttendByMonth = async (month, pageNumber, commission, hardSalary) => {
        let res_list_staff_month = await getAttendByMonth(month, pageNumber, commission, hardSalary);
        if (res_list_staff_month) {
            setStaffListSalaryMonth(res_list_staff_month.data);
            setSalaryMonthPages(res_list_staff_month.totalPages);
        }
    }

    const handlePageClick = (event) => {
        getStaffAttendByDate(startDate, endDate, +event.selected + 1);
        setCurrentPage(+event.selected);
    }

    const handleReportStaffPageClick = (event) => {
        getReportStaff(+event.selected + 1);
        setCurrentReportStaffPage(+event.selected);
    }

    const handleSalaryStaffPageClick = (event) => {
        getStaffAttendByMonth(month, +event.selected + 1, commission, hardSalary);
        setCurrentSalaryMonthPage(+event.selected);
    }

    const handleFilterDate = () => {
        if (endDate < startDate) {
            toast.error("Ngày kết thúc nhỏ hơn ngày bắt đầu");
            return;
        }
        getStaffAttendByDate(startDate, endDate, 1);
    }

    const handleView = () => {
        getStaffAttendByMonth(month, 1, commission, hardSalary);
    }

    return (
        <>
            <span className='d-flex justify-content-center h1'><b>Chấm công</b></span>
            <div className='col-12 col-sm-4 my-3'>
                Từ
                <input
                    className='form-control' type='date'
                    value={startDate}
                    onChange={(event) => setStartDate(event.target.value)}
                />
                Đến
                <input
                    className='form-control' type='date'
                    value={endDate}
                    onChange={(event) => setEndDate(event.target.value)}
                />
                <br />
                <Button onClick={() => handleFilterDate()}>Tìm kiếm</Button>
                <br />
                <br />
                <span>  <i className="fa-solid fa-circle status" style={{ color: 'green' }}></i> Online: {online} </span> <br />
                <span>  <i className="fa-solid fa-circle status" style={{ color: 'red' }}> </i> Offline: {offline}</span>
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên nhân viên</span>
                            </th>
                            <th>
                                <span>Ca làm việc</span>
                            </th>
                            <th>
                                <span>Ngày</span>
                            </th>
                            <th>
                                <span>Tình trạng</span>
                            </th>
                            <th>
                                <span>Ghi chú</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listStaffAttendByDate) && listStaffAttendByDate.length > 0 ? (
                            listStaffAttendByDate.map((item, index) => {
                                return (
                                    <tr key={`list-staff-attendance-for-shop-owner-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.fullName}</td>
                                        <td>{item.timeslot1}</td>
                                        <td>{item.date}</td>
                                        <td>{item.status}</td>
                                        <td>{item.note}</td>
                                    </tr>
                                );
                            })
                        ) : (
                            <tr>
                                <td colSpan="6">Không có dữ liệu</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </div>
            <div className="d-flex justify-content-center  mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalStaffAttendByDatePages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <div className='row'>
                <div className='col-4'>
                    <div style={{ textAlign: 'center' }}>
                        <span className='d-flex justify-content-center h4 my-4'><b>Báo cáo quản lý<br /> nhân viên bán hàng trong tháng</b></span>
                    </div>
                    <div className='customize-table'>
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                    <th>
                                        <span>Tên nhân viên</span>
                                    </th>
                                    <th>
                                        <span>Số ngày làm</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {Array.isArray(listReport) && listReport.length > 0 ? (
                                    listReport.map((item, index) => {
                                        return (
                                            <tr key={`report-staff-${index}`}>
                                                <td>{item.staffName}</td>
                                                <td>{item.present}</td>
                                            </tr>
                                        );
                                    })
                                ) : (
                                    <tr>
                                        <td colSpan="6">Không có dữ liệu</td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        <div className="d-flex justify-content-center  mt-3">
                            <ReactPaginate
                                breakLabel="..."
                                nextLabel="Sau >"
                                onPageChange={handleReportStaffPageClick}
                                forcePage={currentReportStaffPage}
                                pageCount={totalReportStaffPages}
                                previousLabel="< Trước"
                                pageClassName="page-item"
                                pageLinkClassName="page-link"
                                previousClassName="page-item"
                                previousLinkClassName="page-link"
                                nextClassName="page-item"
                                nextLinkClassName="page-link"
                                breakClassName="page-item"
                                breakLinkClassName="page-link"
                                containerClassName="pagination"
                                activeClassName="active"
                            />
                        </div>
                    </div>
                </div>
                <div className='col'>
                    <span className='d-flex justify-content-center h4 my-4'><b>Lương nhân viên theo tháng</b></span>
                    <div className='col d-flex'>
                        <input className='form-control me-2'
                            value={month}
                            type='month'
                            onChange={(event) => setMonth(event.target.value)} />
                        <input
                            className='form-control me-2'
                            value={commission} type='text'
                            onChange={(event) => setCommission(event.target.value)}
                            placeholder='Hoa hồng(%)'
                        />
                        <NumericFormat
                            className='form-control me-2'
                            value={hardSalary} type='text'
                            onChange={(event) => setHardSalary(event.target.value)}
                            placeholder='Lương/giờ'
                            thousandSeparator=","
                        />
                        <Button onClick={() => handleView()}>Xem</Button>
                    </div>
                    <div className='customize-table my-2'>
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                    <th>
                                        <span>Tên nhân viên</span>
                                    </th>
                                    <th>
                                        <span>Lương tháng</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {Array.isArray(listStaffSalaryMonth) && listStaffSalaryMonth.length > 0 ? (
                                    listStaffSalaryMonth.map((item, index) => {
                                        return (
                                            <tr key={`salary-staff-month-${index}`}>
                                                <td>{item.staffName}</td>
                                                <td>{formatNumber(item.salary)}</td>
                                            </tr>
                                        );
                                    })
                                ) : (
                                    <tr>
                                        <td colSpan="6">Không có dữ liệu</td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        <div className="d-flex justify-content-center  mt-3">
                            <ReactPaginate
                                breakLabel="..."
                                nextLabel="Sau >"
                                onPageChange={handleSalaryStaffPageClick}
                                forcePage={currentSalaryMonthPage}
                                pageCount={totalSalaryMonthPages}
                                previousLabel="< Trước"
                                pageClassName="page-item"
                                pageLinkClassName="page-link"
                                previousClassName="page-item"
                                previousLinkClassName="page-link"
                                nextClassName="page-item"
                                nextLinkClassName="page-link"
                                breakClassName="page-item"
                                breakLinkClassName="page-link"
                                containerClassName="pagination"
                                activeClassName="active"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Attendance;