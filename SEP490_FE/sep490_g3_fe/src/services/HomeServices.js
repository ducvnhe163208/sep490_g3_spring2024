import axios from './customize-axios';

const postRegister = (fullName, username, password, phone, email, taxCode, citizenIdentificationCard, bankAccountNumber, bankBin) => {
    return axios.post("/api/Home/Register", { fullName, username, password, phone, email, taxCode, citizenIdentificationCard, bankAccountNumber, bankBin })
}

const getLogin = (username, password) => {
    return axios.get(`/api/Home/Login?username=${username}&password=${password}`)
}

const getStaffLogin = (phone, username, password) => {
    return axios.get(`/api/Home/StaffLogin?phone=${phone}&username=${username}&password=${password}`)
}

export { postRegister, getLogin, getStaffLogin }