import axios from '../services/customize-axios';

const getAllStaff = (pageNumber, search) => {
    return axios.get(`/api/Employee/GetAll?pageNumber=${pageNumber}${search ? `&search=${search}` : ''}`);
}

const getAllStaffNoPaging = () => {
    return axios.get(`/api/Employee/GetAllStaff`);
}

const postAddStaff = (roleId, fullName, username, password, phone, email, avatar, address, taxCode, citizenIdentificationCard, bankAccountNumber) => {
    return axios.post(`/api/Employee/Add`, { roleId, fullName, username, password, phone, email, avatar, address, taxCode, citizenIdentificationCard, bankAccountNumber });
}

const putEditStaff = (id, roleId, fullName, username, password, phone, email, avatar, address, taxCode, citizenIdentificationCard, bankAccountNumber) => {
    return axios.put(`/api/Employee/Update?id=${id}`, { roleId, fullName, username, password, phone, email, avatar, address, taxCode, citizenIdentificationCard, bankAccountNumber });
}
const postAddRole = (roleName) => {
    return axios.post(`/api/Employee/AddRole`, { roleName });
}

const getAllRole = () => {
    return axios.get(`/api/Employee/GetAllRole`);
}

const deleteDeleteStaff = (id) => {
    return axios.delete(`/api/Employee/Block?id=${id}`);
}

export { getAllStaff, postAddStaff, putEditStaff, postAddRole, getAllRole, deleteDeleteStaff, getAllStaffNoPaging };