import axios from '../services/customize-axios';

const getAllSupplier = (pageNumber, search) => {
    return axios.get(`/api/Supplier/GetAll?pageNumber=${pageNumber}${search ? `&search=${search}` : ''}`);
}

const getAllSupplierToAdd = () => {
    return axios.get(`/api/Supplier/GetAllSupplier`);
}

const postAddSupplier = (supplierName, phone, address, email, logo, taxCode) => {
    return axios.post(`/api/Supplier/Add`, { supplierName, phone, address, email, logo, taxCode });
}

const putEditSupplier = (id, supplierName, phone, address, email, logo, taxCode) => {
    return axios.put(`/api/Supplier/Update?id=${id}`, { supplierName, phone, address, email, logo, taxCode });
}

const putDeleteSupplier = (id) => {
    return axios.put(`/api/Supplier/Block?id=${id}`);
}

const getSupIdByName = (search) => {
    return axios.get(`/api/Supplier/GetIdBySupName?search=${search}`);
}

export { getAllSupplier, postAddSupplier, putEditSupplier, putDeleteSupplier, getAllSupplierToAdd, getSupIdByName };