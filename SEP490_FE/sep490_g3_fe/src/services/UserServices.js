import axios from '../services/customize-axios';

const getAdminById = (id) => {
    return axios.get(`/api/Admin/GetAdminById?id=${id}`);
}

const postForgotPassword = (username) => {
    return axios.post(`/api/Home/ForgetPassword?username=${username}`);
}

const putChangePassword = (userid, oldpass, newpass) => {
    return axios.put(`/api/Home/ChangePassword?userid=${userid}&oldpass=${oldpass}&newpass=${newpass}`);
}

const putSaveProfile = (id, fullName, avatar, phone, email, taxCode, citizenIdentificationCard, bankAccountNumber, bankBin) => {
    return axios.put(`/api/Admin/UpdateProfile?id=${id}`, { fullName, avatar, phone, email, taxCode, citizenIdentificationCard, bankAccountNumber, bankBin });
}

const getAdmin = () => {
    return axios.get(`/api/Admin/GetAdmin`);
}

export { getAdminById, postForgotPassword, putChangePassword, putSaveProfile, getAdmin };