import axios from '../services/customize-axios';

const getAllTimeSlot = () => {
    return axios.get(`/api/Salary/GetAllTimeSlot`);
}

const getAttendInDay = () => {
    return axios.get(`/api/Salary/GetAttendInDay`);
}

const getAttendByDate = (start, end, pageNumber) => {
    return axios.get(`/api/Salary/GetAttendByDate?start=${start}&end=${end}&pageNumber=${pageNumber}`);
}

const getReport = (pageNumber) => {
    return axios.get(`/api/Salary/ReportStaff?pageNumber=${pageNumber}`);
}

const getAttendByMonth = (month, pageNumber, commission, hard_salary) => {
    return axios.get(`/api/Salary/GetAttendByMonth?time=${month}&pageNumber=${pageNumber}&commission=${commission}&hard_salary=${hard_salary}`);
}

const postCreateSessions = (timeslotid, note, staffid) => {
    return axios.post(`/api/Salary/CreateSessions?timeslotid=${timeslotid}${note ? `&note=${note}` : ''}&staffid=${staffid}`)
}


export { getAttendInDay, getAllTimeSlot, getAttendByDate, getReport, getAttendByMonth, postCreateSessions };