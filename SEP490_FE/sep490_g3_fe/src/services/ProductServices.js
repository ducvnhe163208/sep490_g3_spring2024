import axios from '../services/customize-axios';

const getAllProductWithPaging = (pageNumber, search, categoryId) => {
    return axios.get(`/api/Product/GetAllProductPage?pageNumber=${pageNumber}${search ? `&search=${search}` : ''}${categoryId ? `&categoryId=${categoryId}` : ''}`);
}

const getAllProduct = () => {
    return axios.get(`/api/Product/GetAllProduct`);
}

const postAddProduct = (phone, cateId, supId, productName, unitPrice, unitInStock, description, image, unit) => {
    return axios.post(`/api/Product/CreateProduct?phone=${phone}`, { cateId, supId, productName, unitPrice, unitInStock, description, image, unit });
}

const putEditProduct = (id, cateId, supId, barcode, productName, unitPrice, unitInStock, description, image, unit) => {
    return axios.put(`/api/Product/UpdateProduct?id=${id}`, { cateId, supId, barcode, productName, unitPrice, unitInStock, description, image, unit, active: true });
}

const deleteDeleteProduct = (id) => {
    return axios.put(`/api/Product/Block?id=${id}`);
}

const getTopSaleQuantity = () => {
    return axios.get(`api/Product/TopSaleQuantity`);
}

const getTopSaleRevenue = () => {
    return axios.get(`Product/GetTopRevenueProducts`);
}

export { getAllProductWithPaging, getAllProduct, postAddProduct, putEditProduct, deleteDeleteProduct, getTopSaleQuantity, getTopSaleRevenue };