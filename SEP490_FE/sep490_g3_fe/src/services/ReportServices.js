import axios from '../services/customize-axios';

const getReportStaffDate = (startDate, endDate, staffId) => {
    return axios.get(`/api/Report/DoanhThuTheoThangCuaNhanVien?startDate=${startDate}&endDate=${endDate}${staffId ? `&staffId=${staffId}` : ''}`);
}

const getAllCustomerReport = (pageNumber) => {
    return axios.get(`/api/Report/CustomerReport?pageNumber=${pageNumber}`);
}

const getTopSaleQuantity = () => {
    return axios.get(`/api/Product/TopSaleQuantity`);
}

const getTopRevenueProducts = () => {
    return axios.get(`/api/Product/GetTopRevenueProducts`);
}

const getReportPurchaseProduct = (startDate, endDate, pageNumber) => {
    return axios.get(`/api/Report/Tongdoanhsomuahang?startDate=${startDate}&endDate=${endDate}&pageNumber=${pageNumber}`);
}

const getReportSaleProduct = (startDate, endDate, pageNumber, staffId) => {
    return axios.get(`/api/Report/Baocaotongdoanhsobanhang?startDate=${startDate}&endDate=${endDate}&pageNumber=${pageNumber}${staffId ? `&staffId=${staffId}` : ''}`);
}


export { getReportStaffDate, getAllCustomerReport, getTopSaleQuantity, getTopRevenueProducts, getReportPurchaseProduct, getReportSaleProduct };