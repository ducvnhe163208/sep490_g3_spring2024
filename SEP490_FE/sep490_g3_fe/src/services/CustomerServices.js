import axios from '../services/customize-axios';

const getAllCustomer = (pageNumber, keyword) => {
    return axios.get(`/api/Customer/GetAll?pageNumber=${pageNumber}${keyword ? `&search=${keyword}` : ''}`);
}

const getAllCustomerToOrder = () => {
    return axios.get(`/api/Customer/GetAllCustomer`);
}

const postAddCustomer = (customerName, phone) => {
    return axios.post(`/api/Customer/Add`, { customerName, phone });
}

const putEditCustomer = (id, customerName, phone, email, address) => {
    return axios.put(`/api/Customer/Update?id=${id}`, {customerName, phone, email, address});    
}

export { getAllCustomer, getAllCustomerToOrder, postAddCustomer, putEditCustomer };
