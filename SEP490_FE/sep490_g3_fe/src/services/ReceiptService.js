import axios from '../services/customize-axios';

const getAllReceipt = (pageNumber) => {
    return axios.get(`/api/Receipt/GetAllReceipt?pageNumber=${pageNumber}`);
}

const postNewReceipt = (productId, supplierId, quantity, totalMoney, note) => {
    return axios.post(`/api/Receipt/AddNewReceipt`, { productId, supplierId, quantity, totalMoney, note });
}

const putEditReceipt = (id, productId, supplierId, quantity, totalMoney, note) => {
    return axios.put(`/api/Receipt/UpdateReceipt?id=${id}`, { productId, supplierId, quantity, totalMoney, note });
}

export { getAllReceipt, postNewReceipt, putEditReceipt };