import axios from '../services/customize-axios';

const getAllCategory = (pageNumber, keyword) => {
    return axios.get(`/api/Category/GetAll?pageNumber=${pageNumber}${keyword ? `&search=${keyword}` : ''}`);
}

const postAddCategory = (categoryName) => {
    return axios.post(`/api/Category/Add`, { categoryName });
}

const putEditCategory = (categoryId, categoryName) => {
    return axios.put(`/api/Category/Update?id=${categoryId}`, { categoryName });
}

const putDeleteCategory = (id) => {
    return axios.put(`/api/Category/Block?id=${id}`);
}

const getAllCategoryToAdd = () => {
    return axios.get(`/api/Category/GetAllCategory`);
}

const getCateIdByname = (search) => {
    return axios.get(`/api/Category/GetIdByName?name=${search}`);
}

export { getAllCategory, postAddCategory, putEditCategory, putDeleteCategory, getAllCategoryToAdd, getCateIdByname };