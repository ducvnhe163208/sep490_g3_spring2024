import axios from '../services/customize-axios';

const postAddOrder = (customerId, staffId, orderDetails) => {
    return axios.post(`/api/Order/NewOrder`, {
        order: {
            customerId: customerId,
            staffId: staffId
        },
        orderDetails: orderDetails
    });
}

const getAllOrder = (pageNumber, searchDate) => {
    return axios.get(`api/Order/GetAllOrders?pageNumber=${pageNumber}${searchDate ? `&date=${searchDate}` : ''}`);
}

const getOrderInDay = () => {
    return axios.get(`api/Order/GetOrderByDay`);
}

const getOrderInMonth = () => {
    return axios.get(`api/Order/GetOrderByMonth`);
}

export { postAddOrder, getAllOrder, getOrderInDay, getOrderInMonth };