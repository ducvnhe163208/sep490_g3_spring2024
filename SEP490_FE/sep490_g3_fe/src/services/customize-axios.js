import axios from "axios"

const instance = axios.create({
    baseURL: 'https://localhost:7176',
});

// Add a response interceptor
instance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response 
    return response.data ? response.data : { statusCode: response.status };
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    let res = {};
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        res.data = error.response.data;
        res.status = error.response.status;
        res.headers = error.response.headers
    } else if (error.request) {
        console.log(error.request);
    } else {
        console.log('Error', error.message);
    }
    return res;
    //return Promise.reject(error);
});

instance.interceptors.request.use(function (config) {
    const subdomain = localStorage.getItem("phone");
    // Thêm subdomain vào header của request
    config.headers['Subdomain'] = subdomain;
    return config;
}, function (error) {
    // Xử lý lỗi nếu cần
    return Promise.reject(error);
});

export default instance;