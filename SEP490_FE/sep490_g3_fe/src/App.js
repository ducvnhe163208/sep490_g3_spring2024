import './App.scss';
import { ToastContainer } from 'react-toastify';
import Container from 'react-bootstrap/Container';
import Header from './components/shared-components/Header';
import Footer from './components/shared-components/Footer';
import { useContext, useEffect } from 'react';
import { UserContext } from './context/UserContext';
import AppRoutes from './components/routes/AppRoutes';
function App() {

  const { user, loginContext } = useContext(UserContext);

  useEffect(() => {
    if (localStorage.getItem("token")) {
      loginContext(localStorage.getItem("userid"));
      loginContext(localStorage.getItem("email"));
      loginContext(localStorage.getItem("token"));
    }
  }, [])

  return (
    <>
      <div className='app-container'>
        <Header />
        <Container>
          <AppRoutes />
        </Container>
        <Footer />
      </div>

      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover />
    </>
  );
}

export default App;
