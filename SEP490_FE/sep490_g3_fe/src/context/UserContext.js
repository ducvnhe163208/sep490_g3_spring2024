import React from 'react';

const UserContext = React.createContext({ userid: '', email: '', auth: false });

const UserProvider = ({ children }) => {
    const [user, setUser] = React.useState({ userid: '', email: '', auth: false });

    const loginContext = (userid, email, token, phoneNumber) => {
        // setUser((user) => ({
        //     userid: userid,
        //     email: email,
        //     auth: true,
        // }));
        
        if (!localStorage.getItem('userid') &&
            !localStorage.getItem("email") &&
            !localStorage.getItem("token") &&
            !localStorage.getItem("phone")) {
            localStorage.setItem("token", token);
            localStorage.setItem("userid", userid);
            localStorage.setItem("email", email);
            localStorage.setItem("phone", phoneNumber)
        }

        setUser((user) => ({
            userid: localStorage.getItem("userid", userid),
            email: localStorage.getItem("email", email),
            auth: true,
        }));
    };

    const logout = () => {
        localStorage.removeItem("userid");
        localStorage.removeItem("email");
        localStorage.removeItem("token");
        localStorage.removeItem("phone");
        localStorage.removeItem("role");
        localStorage.removeItem("id");
        setUser((user) => ({
            userid: '',
            email: '',
            auth: false,
        }));
    };

    return (
        <UserContext.Provider value={{ user, loginContext, logout }}>
            {children}
        </UserContext.Provider>
    );
};

export { UserContext, UserProvider };