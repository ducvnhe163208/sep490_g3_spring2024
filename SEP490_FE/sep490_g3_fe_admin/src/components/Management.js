import { React, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { getAllShopOwner, getTotalMoney } from '../services/AdminServices';
import RegisterExtend from './RegisterExtend';

function Management() {

    const [listShopOwner, setListShopOwner] = useState([]);
    const [totalMoney, setTotalMoney] = useState([]);

    const [totalManagePages, setTotalManagePages] = useState(1);
    const [currentPage, setCurrentPage] = useState(0);

    const [isShowModalRegisterExtend, setIsShowModalRegisterExtend] = useState(false);
    const [dataShopOwner, setDataShopOwner] = useState("");

    const handleClose = () => {
        setIsShowModalRegisterExtend(false);
    }

    useEffect(() => {
        document.title = "Quản lý";
        getAll(1);
    }, [])

    const getAll = async (page) => {
        let res = await getAllShopOwner(page);
        let res1 = await getTotalMoney();
        if (res && res1) {
            setTotalMoney(res1);
            setTotalManagePages(res.totalPages);
            setListShopOwner(res.data);
        }
    }

    const handlePageClick = (event) => {
        getAll(+event.selected + 1);
        setCurrentPage(+event.selected);
    }

    const handleRegisterExtend = (item) => {
        console.log("ok:", item.userId);
        setDataShopOwner(item.userId);
        console.log("ok:", dataShopOwner);
        setIsShowModalRegisterExtend(true);
    }

    const updateShopOwner = () => {
        getAllShopOwner(currentPage + 1);
    }

    return (
        <>
            <span className='d-flex justify-content-center my-3 h1'><b>Danh sách chủ shop</b></span>
            <div className='col-12 col-sm-4 my-3'>
                <h4>Tổng số tiền: {totalMoney} VNĐ</h4>
            </div>
            <div className='customize-table'>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>
                                <span>STT</span>
                            </th>
                            <th>
                                <span>Tên site</span>
                            </th>
                            <th>
                                <span>Logo</span>
                            </th>
                            <th>
                                <span>Số điện thoại</span>
                            </th>
                            <th>
                                <span>Email</span>
                            </th>
                            <th>
                                <span>Mã số thuế</span>
                            </th>
                            <th>
                                <span>Số CCCD</span>
                            </th>
                            <th>
                                <span>STK Ngân hàng</span>
                            </th>
                            <th>
                                <span>Ngày đăng ký</span>
                            </th>
                            <th>
                                <span>Ngày hết hạn</span>
                            </th>
                            <th>
                                <span>Tình trạng</span>
                            </th>
                            <th>
                                <span>Tuỳ chỉnh</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {Array.isArray(listShopOwner) && listShopOwner.length > 0 &&
                            listShopOwner.map((item, index) => {
                                const endDate = (item.endDate).replace(/\(dd\/mm\/yyyy\)/, '');
                                const parts = endDate.split('/');
                                const day = parseInt(parts[0], 10);
                                const month = parseInt(parts[1], 10) - 1; // Trừ đi 1 vì tháng bắt đầu từ 0
                                const year = parseInt(parts[2], 10);

                                // Tạo đối tượng Date từ ngày, tháng, năm
                                const date = new Date(year, month, day);
                                const currentDate = new Date();
                                const isExpired = date > currentDate;
                                console.log(endDate)
                                return (
                                    <tr key={`shop-owners-${index}`}>
                                        <td>{index + 1}</td>
                                        <td>{item.siteName}</td>
                                        <td>{item.logo}</td>
                                        <td>{item.phone}</td>
                                        <td>{item.email}</td>
                                        <td>{item.taxCode}</td>
                                        <td>{item.citizenIdentificationCard}</td>
                                        <td>{item.bankAccountNumber}</td>
                                        <td>{item.startDate}</td>
                                        <td>{item.endDate}</td>
                                        <td>
                                            {isExpired ?
                                                <span style={{ color: 'green' }}>Còn hạn</span> :
                                                <span style={{ color: 'red' }}>Hết hạn</span>
                                            }
                                        </td>
                                        <td>
                                            <Button
                                                className='btn btn-warning mx-1 my-1'
                                                onClick={() => handleRegisterExtend(item)}
                                            >
                                                Gia hạn
                                            </Button>
                                            <Button
                                                className='btn btn-danger mx-1 my-1'
                                            >
                                                Cấm
                                            </Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div >
            <div className="d-flex justify-content-center mt-3">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Sau >"
                    onPageChange={handlePageClick}
                    forcePage={currentPage}
                    pageCount={totalManagePages}
                    previousLabel="< Trước"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </div>
            <RegisterExtend
                isShow={isShowModalRegisterExtend}
                handleClose={handleClose}
                dataShopOwnerEdit={dataShopOwner}
                updateShopOwner={updateShopOwner}
            />
        </>
    );
}

export default Management;