import { React, useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { putRegisterExtend } from '../services/AdminServices';
import { toast } from 'react-toastify';

function RegisterExtend({ isShow, handleClose, dataShopOwnerEdit, updateShopOwner }) {

    const [shopOwnerId, setShopOwnerId] = useState("");
    const [month, setMonth] = useState("");

    const handleRegisterExtend = async () => {
        if (!month) {
            toast.error("Vui lòng nhập tháng gia hạn");
        }
        let res = await putRegisterExtend(shopOwnerId, month);
        if (res && res.statusCode == 200) {
            handleClose();
            toast.success("Gia hạn thành công!");
            updateShopOwner();
        }
        else {
            toast.error("Gia hạn thất bại!");
        }
    }


    useEffect(() => {
        if (isShow) {
            setShopOwnerId(dataShopOwnerEdit);
        }
    }, [dataShopOwnerEdit])
    return (
        <>
            <Modal
                show={isShow}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Gia hạn cho khác hàng</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <div className="mb-3">
                            <label className="form-label">Nhập số tháng gia hạn</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập số tháng gia hạn"
                                value={month}
                                onChange={(event) => setMonth(event.target.value)}
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Đóng
                    </Button>
                    <Button variant="primary" onClick={() => handleRegisterExtend()}>Gia hạn</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default RegisterExtend;