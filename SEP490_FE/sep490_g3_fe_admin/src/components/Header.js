import { React, useState } from 'react';
import { Container, Nav, Navbar, NavDropdown, NavLink } from 'react-bootstrap';
import { toast } from 'react-toastify';

function Header() {
    const [activeLink, setActiveLink] = useState("");

    const handleLogout = () => {
        toast.success("Đăng xuất thành công");
    }

    return (
        <>
            <Navbar expand="lg" className="bg-body-tertiary">
                <Container>
                    <NavLink className="mx-2 px-2 my-1 logo" onClick={() => setActiveLink('homepage')} to='/'>VietBiz</NavLink>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <>
                            <Nav className="me-auto">
                            </Nav>
                        </>
                    </Navbar.Collapse>
                </Container>
            </Navbar >
        </>
    );
}

export default Header;