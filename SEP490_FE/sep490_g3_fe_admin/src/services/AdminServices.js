import axios from '../services/customize-axios';

const getAllShopOwner = (pageNumber) => {
    return axios.get(`/GetAllShopOwner?pageNumber=${pageNumber}`);
}

const getTotalMoney = () => {
    return axios.get(`/TotalMoney`);
}

const putRegisterExtend = (userid, month) => {
    return axios.put(`/ExtendRegistration?id=${userid}&extend=${month}`);
}

export { getAllShopOwner, getTotalMoney, putRegisterExtend };