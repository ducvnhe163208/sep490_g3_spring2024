import './App.scss';
import { ToastContainer } from 'react-toastify';
import Container from 'react-bootstrap/Container';
import { Route, Routes } from 'react-router-dom';
import Management from './components/Management';
import Header from './components/Header';

function App() {
  return (
    <>
      <div className='app-container'>
        <Header />
        <Container>
          <Routes>
            <Route path='/' element={<Management />} />
          </Routes>
        </Container>
      </div>

      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover />
    </>
  );
}

export default App;
