﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Timeslot
    {
        public Timeslot()
        {
            Sessions = new HashSet<Session>();
        }

        public int TimeslotId { get; set; }
        public string? Timeslot1 { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }
    }
}
