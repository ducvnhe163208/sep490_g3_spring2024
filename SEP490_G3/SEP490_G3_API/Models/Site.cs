﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Site
    {
        public int SiteId { get; set; }
        public string? SiteName { get; set; }
        public int? UserId { get; set; }

        public virtual User? User { get; set; }
    }
}
