﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
            Receipts = new HashSet<Receipt>();
        }

        public int ProductId { get; set; }
        public int? CategoryId { get; set; }
        public int? SupplierId { get; set; }
        public string? Barcode { get; set; }
        public string? ProductName { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? UnitInStock { get; set; }
        public string? Description { get; set; }
        public string? Image { get; set; }
        public string? Unit { get; set; }
        public bool? Active { get; set; }
        public decimal? UnitCost { get; set; }

        public virtual Category? Category { get; set; }
        public virtual Supplier? Supplier { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<Receipt> Receipts { get; set; }
    }
}
