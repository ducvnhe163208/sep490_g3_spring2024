﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class User
    {
        public User()
        {
            Sites = new HashSet<Site>();
        }

        public int UserId { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? Logo { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string TaxCode { get; set; } = null!;
        public string CitizenIdentificationCard { get; set; } = null!;
        public string BankAccountNumber { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual ICollection<Site> Sites { get; set; }
    }
}
