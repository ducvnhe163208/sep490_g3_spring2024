﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Admin
    {
        public Admin()
        {
            staff = new HashSet<staff>();
        }

        public int AdminId { get; set; }
        public string FullName { get; set; } = null!;
        public string? Avatar { get; set; }
        public string Phone { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string TaxCode { get; set; } = null!;
        public string CitizenIdentificationCard { get; set; } = null!;
        public string BankAccountNumber { get; set; } = null!;
        public string BankBin { get; set; } = null!;
        public virtual ICollection<staff> staff { get; set; }
    }
}
