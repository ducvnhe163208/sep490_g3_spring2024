﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Session
    {
        public Session()
        {
            Attendances = new HashSet<Attendance>();
        }

        public int SessionId { get; set; }
        public DateTime Date { get; set; }
        public int? TimeslotId { get; set; }

        public virtual Timeslot? Timeslot { get; set; }
        public virtual ICollection<Attendance> Attendances { get; set; }
    }
}
