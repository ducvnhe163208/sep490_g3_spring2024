﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Attendance
    {
        public int AttendanceId { get; set; }
        public int? StaffId { get; set; }
        public bool? Status { get; set; }
        public int? SessionId { get; set; }
        public string? Note { get; set; }

        public virtual Session? Session { get; set; }
        public virtual staff? Staff { get; set; }
    }
}
