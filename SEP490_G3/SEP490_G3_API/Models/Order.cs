﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderId { get; set; }
        public int? CustomerId { get; set; }
        public int? StaffId { get; set; }
        public DateTime OderDate { get; set; }
        public decimal? TotalMoney { get; set; }

        public virtual Customer? Customer { get; set; }
        public virtual staff? Staff { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
