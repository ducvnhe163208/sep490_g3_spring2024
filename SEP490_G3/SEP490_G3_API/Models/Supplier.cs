﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            Products = new HashSet<Product>();
            Receipts = new HashSet<Receipt>();
        }

        public int SupplierId { get; set; }
        public string? SupplierName { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? Logo { get; set; }
        public string? TaxCode { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Receipt> Receipts { get; set; }
    }
}
