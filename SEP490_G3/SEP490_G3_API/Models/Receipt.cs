﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Receipt
    {
        public int ProductId { get; set; }
        public int SupplierId { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalMoney { get; set; }
        public string? Note { get; set; }
        public DateTime Date { get; set; }

        public virtual Product Product { get; set; } = null!;
        public virtual Supplier Supplier { get; set; } = null!;
    }
}
