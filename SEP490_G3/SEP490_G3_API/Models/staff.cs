﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class staff
    {
        public staff()
        {
            Attendances = new HashSet<Attendance>();
            Orders = new HashSet<Order>();
        }

        public int StaffId { get; set; }
        public int? AdminId { get; set; }
        public int? RoleId { get; set; }
        public string? FullName { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string? Avatar { get; set; }
        public string? Address { get; set; }
        public string? TaxCode { get; set; }
        public string? CitizenIdentificationCard { get; set; }
        public string? BankAccountNumber { get; set; }
        public bool? Active { get; set; }

        public virtual Admin? Admin { get; set; }
        public virtual Role? Role { get; set; }
        public virtual ICollection<Attendance> Attendances { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
