﻿using System;
using System.Collections.Generic;

namespace SEP490_G3_API.Models
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public int CategoryId { get; set; }
        public string? CategoryName { get; set; } 
        public bool? Active { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
