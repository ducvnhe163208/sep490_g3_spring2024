﻿using Microsoft.EntityFrameworkCore;
using SaasKit.Multitenancy;
using SEP490_G3_API.Models;

namespace SEP490_G3_API
{
    public interface ITenantResolver
    {
        Task<TenantContext<Site>> ResolveAsync(HttpContext context);
    }
    public class TenantResolver : ITenantResolver<Site>
    {
        private readonly SEP490_G3_SiteContext _context;

        public TenantResolver(SEP490_G3_SiteContext context)
        {
            _context = context;
        }

        public async Task<TenantContext<Site>> ResolveAsync(HttpContext context)
        {
            // Lấy số điện thoại từ yêu cầu HTTP
            string subDomainFromUrl = context.Request.Headers["Subdomain"];

            // Tìm trang web tương ứng trong database
            var result = await _context.Sites.FirstOrDefaultAsync(x => x.SiteName == subDomainFromUrl);
            Site site = new();
            if (result != null)
            {
                // Trả về thông tin trang web nếu được tìm thấy
                site.SiteId = result.SiteId;
                site.SiteName = result.SiteName;
                site.UserId = result.UserId;
                return await Task.FromResult(new TenantContext<Site>(site));
            }

            // Trả về null nếu không tìm thấy trang web
            return await Task.FromResult(new TenantContext<Site>(site));
        }
    }
}
