﻿using SEP490_G3_API.IServiceHelper;
using System.Net.Mail;
using System.Net;

namespace SEP490_G3_API.HelperServices
{
    public class MailHelper : IMailKitService
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            var mail = "duca2k52ls@gmail.com";
            var pw = "opuf vdem qwqw gyup";

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = new NetworkCredential(mail, pw)
            };
            return client.SendMailAsync(
                new MailMessage(from: mail, to: email, subject, message));
        }
        public string GenerateRandomPassword()
        {
            const string chars = "0123456789";
            var random = new Random();
            var password = new string(Enumerable.Repeat(chars, 8)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return password;
        }
    }
}
