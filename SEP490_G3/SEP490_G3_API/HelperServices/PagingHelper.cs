﻿using SEP490_G3_API.Models;

namespace SEP490_G3_API.HelperServices
{
    public class PagingHelper<T>
    {
        /*
         Created by: Duc VN
         Created on: 14/3/2024 (dd/mm/yyyy)
         Description: This method paging 
         File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\HelperServices\PagingHelper.cs
         ---
         Modify by: Duc VN
         Created on: 19/3/2024 (dd/mm/yyyy)
       */
        public List<T> GetPagedData(List<T> source, int pageNumber, int pageSize)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (pageNumber < 1)
                throw new ArgumentException("Invalid page number", nameof(pageNumber));

            if (pageSize < 1)
                throw new ArgumentException("Invalid page size", nameof(pageSize));

            return source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }
        public int CalculateTotalPages(List<T> list, int pageSize)
        {
            var totalItems = list.Count();
            var totalPages = (int)Math.Ceiling((double)totalItems / pageSize);

            return totalPages;
        }
    }
}
