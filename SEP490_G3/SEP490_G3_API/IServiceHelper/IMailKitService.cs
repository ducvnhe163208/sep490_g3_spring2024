﻿namespace SEP490_G3_API.IServiceHelper
{
    public interface IMailKitService
    {
        string GenerateRandomPassword();
        Task SendEmailAsync(string email, string subject, string message);
    }
}
