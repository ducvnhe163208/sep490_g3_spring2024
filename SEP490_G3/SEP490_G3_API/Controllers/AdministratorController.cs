﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    public class AdministratorController : ControllerBase
    {
        private readonly SEP490_G3_SiteContext _context;
        private readonly SEP490_G3_test4Context _context2;

        public AdministratorController(SEP490_G3_SiteContext context, SEP490_G3_test4Context context2)
        {
            _context = context;
            _context2 = context2;
        }
        [HttpGet("GetAllShopOwner")]
        public async Task<ResponsePagingSite?> GetAllShopOwner(int pageNumber)
        {
            try
            {
                var pagingHelper = new PagingHelper<SiteDTO>();
                int pageSize = 5;
                
                var shopOwner = await _context.Sites.Include(x => x.User).Select(x => new SiteDTO
                {
                    UserId = x.UserId,
                    SiteName = x.SiteName,
                    Logo = x.User.Logo,
                    Phone = x.User.Phone,
                    Email = x.User.Email,
                    TaxCode = x.User.TaxCode,
                    CitizenIdentificationCard = x.User.CitizenIdentificationCard,
                    BankAccountNumber = x.User.BankAccountNumber,
                    StartDate = (x.User.StartDate.Day+"/"+ x.User.StartDate.Month+"/"+ x.User.StartDate.Year + " (dd/mm/yyyy)"),
                    EndDate = (x.User.EndDate.Day + "/" + x.User.EndDate.Month + "/" + x.User.EndDate.Year + " (dd/mm/yyyy)")
                }).ToListAsync();
                var pagedData = pagingHelper.GetPagedData(shopOwner, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(shopOwner, pageSize);
                return new ResponsePagingSite { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("TotalMoney")]
        public async Task<IActionResult> TotalMoney()
        {
            var totalUser = await _context.Sites.Include(x => x.User).ToListAsync();

            var sumTime = 0;
            foreach (var item in totalUser)
            {
                int totalTime = item.User.EndDate.Month - item.User.StartDate.Month;
                sumTime += totalTime;
            };

            var totalRevenue = sumTime * 20000;
            return Ok(totalRevenue);
        }
        [HttpPut("ExtendRegistration")]
        public async Task<IActionResult> ExtendRegistration(int id, int extend)
        {
            try
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.UserId == id);
                if (user != null)
                {
                    user.EndDate = user.EndDate.AddMonths(extend);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
