﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;
        public SupplierController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method get list Supplier 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\SupplierController.cs
          ---
          Modify by: Duc VN
          Created on: 1/3/2024 (dd/mm/yyyy)
          ---
          Modify by: Duc VN
          Created on: 13/3/2024 (dd/mm/yyyy)
          ---
          Modify by: Duc VN
          Created on: 14/3/2024 (dd/mm/yyyy)
        */
        [HttpGet("GetAll")]
        public async Task<ResponsePagingSupplier?> GetAllSupplier(int pageNumber, string? search)
        {
            try
            {
                var pagingHelper = new PagingHelper<SupplierDTO>();
                int pageSize = 5;
                if (search == null)
                {
                    var suppliers = await _context.Suppliers.Select(x => new SupplierDTO
                    {
                        SupplierId = x.SupplierId,
                        SupplierName = x.SupplierName,
                        Address = x.Address,
                        Email = x.Email,
                        Logo = x.Logo,
                        Phone = x.Phone,
                        TaxCode = x.TaxCode,
                        Active = x.Active
                    }).ToListAsync();

                    var pagedData = pagingHelper.GetPagedData(suppliers, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(suppliers, pageSize);

                    return new ResponsePagingSupplier { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var suppliers = await _context.Suppliers.Where(x => x.Active == true && x.SupplierName.ToLower().Contains(search.ToLower()) || x.Phone.Contains(search)).Select(x => new SupplierDTO
                    {
                        SupplierName = x.SupplierName,
                        Address = x.Address,
                        Email = x.Email,
                        Logo = x.Logo,
                        Phone = x.Phone,
                        TaxCode = x.TaxCode,
                        Active = x.Active
                    }).ToListAsync();
                    if (suppliers.Count == 0)
                    {
                        return new ResponsePagingSupplier { };
                    }
                    var pagedData = pagingHelper.GetPagedData(suppliers, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(suppliers, pageSize);
                    return new ResponsePagingSupplier { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetAllSupplier")]
        public async Task<IActionResult> GetAllSupplier()
        {
            try
            {
                var suppliers = await _context.Suppliers.Where(x => x.Active == true).Select(x => new SupplierDTO
                {
                    SupplierId = x.SupplierId,
                    SupplierName = x.SupplierName,
                    Address = x.Address,
                    Email = x.Email,
                    Logo = x.Logo,
                    Phone = x.Phone,
                    TaxCode = x.TaxCode,
                    Active = x.Active
                }).ToListAsync();
                return Ok(suppliers);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method search Supplier by name or phone 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\SupplierController.cs
          ---
          Modify by: Duc VN
          Created on: 1/3/2024 (dd/mm/yyyy)
        */
        [HttpGet("SearchByNameOrPhone")]
        public IActionResult SearchSupplier(string search)
        {
            try
            {
                var list = _context.Suppliers.Where(x => x.Active == true).ToList();
                var supplier = list.Where(x => x.SupplierName.ToLower().Contains(search.ToLower()) || x.Phone.Contains(search)).ToList();
                if (supplier.Count == 0)
                {
                    return NotFound("Supplier don't exist!!!");
                }
                return Ok(supplier);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method add new Supplier 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\SupplierController.cs
          ---
          Modify by: Duc VN
          Created on: 1/3/2024 (dd/mm/yyyy)
        */
        [HttpPost("Add")]
        public async Task<IActionResult> AddSupplier([FromBody]SupplierDTO supplierDTO)
        {
            try
            {
                var supplier = await _context.Suppliers.FirstOrDefaultAsync(x => x.Phone == supplierDTO.Phone);
                if (supplier == null)
                {
                    var newSupplier = new Supplier
                    {
                        SupplierName = supplierDTO.SupplierName,
                        Phone = supplierDTO.Phone,
                        Email = supplierDTO.Email,
                        Address = supplierDTO.Address,
                        TaxCode = supplierDTO.TaxCode,
                        Logo = supplierDTO.Logo,
                        Active = true
                    };
                    _context.Suppliers.Add(newSupplier);
                    _context.SaveChanges();
                    return Ok();
                }
                return BadRequest("Supplier existed!!!");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method update Supplier 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\SupplierController.cs
          ---
          Modify by: Duc VN
          Created on: 19/3/2024 (dd/mm/yyyy)
        */
        [HttpPut("Update")]
        public async Task<IActionResult> Update(int id, SupplierDTO supplierDTO)
        {
            try
            {
                var supplier = await _context.Suppliers.FirstOrDefaultAsync(x => x.SupplierId == id);
                if (supplier == null)
                {
                    return NotFound("Supplier don't exist!!!");
                }
                supplier.SupplierName = supplierDTO.SupplierName;
                supplier.Phone = supplierDTO.Phone;
                supplier.Email = supplierDTO.Email;
                supplier.Address = supplierDTO.Address;
                supplier.Logo = supplierDTO.Logo;
                supplier.TaxCode = supplierDTO.TaxCode;
                _context.SaveChanges();
                return Ok();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 1/3/2024 (dd/mm/yyyy)
          Description: This method block Supplier 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\SupplierController.cs
          ---
        */
        [HttpPut("Block")]
        public async Task<IActionResult> Block(int id)
        {
            try
            {
                var supplier = await _context.Suppliers.FirstOrDefaultAsync(x => x.SupplierId == id);
                if (supplier != null)
                {
                    if (supplier.Active == true)
                    {
                        supplier.Active = false;
                    }
                    else
                    {
                        supplier.Active = true;
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                return NotFound("Supplier don't exist");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method search Supplier by name or phone 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\SupplierController.cs
          ---
          Modify by: Duc VN
          Created on: 1/3/2024 (dd/mm/yyyy)
        */
        [HttpGet("GetIdBySupName")]
        public IActionResult GetIdBySupName(string search)
        {
            try
            {
                var supplier = _context.Suppliers.FirstOrDefault(x => x.SupplierName.ToLower().Equals(search.ToLower()) && x.Active == true);
                if (supplier == null)
                {
                    return NotFound("Supplier don't exist!!!");
                }
                return Ok(supplier.SupplierId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
