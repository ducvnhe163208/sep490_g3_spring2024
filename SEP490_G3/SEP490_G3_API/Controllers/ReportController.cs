﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;
using System;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;
        private readonly ILogger<ReportController> _logger;
        public ReportController(SEP490_G3_test4Context context, ILogger<ReportController> logger)
        {
            _context = context;
            _logger = logger;
        }
        /*
        [HttpGet("DoanhThuTheoThangCuaNhanVien")]
        public async Task<IActionResult> BenefitByMonth(string monthyear)
        {
            try
            {
                var datetime = DateTime.Parse(monthyear);
                var liststaff = await _context.staff.ToListAsync();
                List<ReportDTO> list = new List<ReportDTO>();

                foreach (var s in liststaff)
                {
                    decimal? sum = 0;
                    var listorder = await _context.Orders
                        .Where(x => x.OderDate.Month == datetime.Month && x.OderDate.Year == datetime.Year && x.StaffId == s.StaffId)
                        .ToListAsync();

                    foreach (var o in listorder)
                    {
                        sum += o.TotalMoney;
                    }

                    var newBenefit = new ReportDTO
                    {
                        StaffName = s.FullName,
                        totalMoney = sum
                    };
                    list.Add(newBenefit);
                }

                decimal? benefit = 0;
                foreach (var s in list)
                {
                    benefit += s.totalMoney;
                }

                return Ok(new { list, benefit });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        */
        /*
        [HttpGet("Baocaotongdoanhsobanhang")]
        public async Task<ResponsePagingReportOrderByStaff> Baocaotongdoanhsobanhang(string startDate, string endDate, int pageNumber, int? staffId)
        {
            try
            {
                var pagingHelper = new PagingHelper<ReportOrderDTO>();
                int pageSize = 10;

                var fromDate = DateTime.Parse(startDate);
                var toDate = DateTime.Parse(endDate);
                List<ReportOrderDTO> list = new List<ReportOrderDTO>();

                if (staffId == null)
                {
                    var listOrder = await _context.OrderDetails.Include(x => x.Product).Include(x => x.Order)
                        .Where(x => x.Order.OderDate >= fromDate && x.Order.OderDate <= toDate)
                        .ToListAsync();

                    foreach(var o in listOrder)
                    {
                        var newOrder = new ReportOrderDTO
                        {
                            ProductName = o.Product.ProductName,
                            Quantity = o.Quantity,
                            UnitPrice = o.UnitPrice,
                            TotalMoney = o.Order.TotalMoney
                        };

                        list.Add(newOrder);
                    }

                    var pagedData = pagingHelper.GetPagedData(list, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(list, pageSize);
                    if (list == null)
                    {
                        return new ResponsePagingReportOrderByStaff { };
                    }
                    return new ResponsePagingReportOrderByStaff { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var listOrder = await _context.OrderDetails.Include(x => x.Product).Include(x => x.Order)
                        .Where(x => x.Order.OderDate >= fromDate && x.Order.OderDate <= toDate && x.Order.StaffId == staffId)
                        .ToListAsync();

                    foreach (var o in listOrder)
                    {
                        var newOrder = new ReportOrderDTO
                        {
                            ProductName = o.Product.ProductName,
                            Quantity = o.Quantity,
                            UnitPrice = o.UnitPrice,
                            TotalMoney = o.UnitPrice * o.Quantity
                        };

                        list.Add(newOrder);
                    }

                    var pagedData = pagingHelper.GetPagedData(list, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(list, pageSize);
                    if (list == null)
                    {
                        return new ResponsePagingReportOrderByStaff { };
                    }
                    return new ResponsePagingReportOrderByStaff { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        */
        [HttpGet("Baocaotongdoanhsobanhang")]
        public async Task<ResponsePagingReportOrderByStaff> Baocaotongdoanhsobanhang(string startDate, string endDate, int pageNumber, int? staffId)
        {
            try
            {
                var pagingHelper = new PagingHelper<ReportOrderDTO>();
                int pageSize = 10;

                var fromDate = DateTime.Parse(startDate);
                var toDate = DateTime.Parse(endDate);
                List<ReportOrderDTO> list = new List<ReportOrderDTO>();

                IQueryable<OrderDetail> query = _context.OrderDetails.Include(x => x.Product).Include(x => x.Order)
                                                        .Where(x => x.Order.OderDate >= fromDate && x.Order.OderDate <= toDate);

                if (staffId != null)
                {
                    query = query.Where(x => x.Order.StaffId == staffId);
                }

                var listOrder = await query.ToListAsync();

                foreach (var orderDetail in listOrder)
                {
                    var existingOrder = list.FirstOrDefault(o => o.ProductName == orderDetail.Product.ProductName);
                    if (existingOrder != null)
                    {
                        existingOrder.Quantity += orderDetail.Quantity;
                        existingOrder.TotalMoney += orderDetail.UnitPrice * orderDetail.Quantity;
                    }
                    else
                    {
                        var newOrder = new ReportOrderDTO
                        {
                            ProductName = orderDetail.Product.ProductName,
                            Quantity = orderDetail.Quantity,
                            UnitPrice = orderDetail.UnitPrice,
                            TotalMoney = orderDetail.UnitPrice * orderDetail.Quantity
                        };
                        list.Add(newOrder);
                    }
                }

                var pagedData = pagingHelper.GetPagedData(list, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(list, pageSize);
                if (list == null)
                {
                    return new ResponsePagingReportOrderByStaff { };
                }
                return new ResponsePagingReportOrderByStaff { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("Tongdoanhsomuahang")]
        public async Task<ResponsePagingReportOrder> Tongdoanhsomuahang(string startDate, string endDate, int pageNumber)
        {
            try
            {
                var pagingHelper = new PagingHelper<ReportOrderByDateDTO>();
                int pageSize = 10;

                var fromDate = DateTime.Parse(startDate);
                var toDate = DateTime.Parse(endDate);
                List<ReportOrderByDateDTO> list = new List<ReportOrderByDateDTO>();
                var listReceipt = _context.Receipts.Where(x => x.Date >= fromDate && x.Date <= toDate).ToList();
                if (listReceipt == null)
                {
                    return new ResponsePagingReportOrder { };
                }
                foreach (var r in listReceipt)
                {
                    var product = await _context.Products.FirstOrDefaultAsync(x => x.ProductId == r.ProductId);
                    if(product == null)
                    {
                        return new ResponsePagingReportOrder { };
                    }
                    var newReport = new ReportOrderByDateDTO
                    {
                        ProductName = product.ProductName,
                        Quantity = r.Quantity,
                        TotalMoney = r.TotalMoney,
                        UnitPrice = r.TotalMoney / r.Quantity,
                        UnitInStock = product.UnitInStock
                    };
                    list.Add(newReport);
                }
                decimal? benefit = 0;
                foreach (var l in list)
                {
                    benefit += l.TotalMoney;
                }
                var pagedData = pagingHelper.GetPagedData(list, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(list, pageSize);
                if (list == null)
                {
                    return new ResponsePagingReportOrder { };
                }
                return new ResponsePagingReportOrder { Data = pagedData, PageSize = pageSize, TotalPages = totalPages, TotalMoney = benefit };
            }
            catch (Exception ex)
            {
                // Ghi nhật ký hoặc trả về một phản hồi lỗi cụ thể cho client
                _logger.LogError(ex, "Error occurred in Tongdoanhsomuahang method.");
                throw; // hoặc trả về một phản hồi lỗi
            }
        }

        [HttpGet("DoanhThuTheoThangCuaNhanVien")]
        public async Task<IActionResult> FilterDoanhThuTheoThangCuaNhanVien(string startDate, string endDate, int? staffId)
        {
            try
            {
                var fromDate = DateTime.Parse(startDate);
                var toDate = DateTime.Parse(endDate);

                if (staffId == null)
                {
                    var liststaff = await _context.staff.ToListAsync();
                    List<ReportDTO> list = new List<ReportDTO>();

                    foreach (var s in liststaff)
                    {
                        decimal? sum = 0;
                        var listorder = await _context.Orders
                        .Where(x => x.OderDate >= fromDate && x.OderDate <= toDate && x.StaffId == s.StaffId)
                        .ToListAsync();

                        foreach (var o in listorder)
                        {
                            sum += o.TotalMoney;
                        }

                        var newBenefit = new ReportDTO
                        {
                            StaffName = s.FullName,
                            totalMoney = sum
                        };
                        list.Add(newBenefit);
                    }

                    decimal? benefit = 0;
                    foreach (var s in list)
                    {
                        benefit += s.totalMoney;
                    }

                    return Ok(new { list, benefit });
                }
                else
                {
                    List<ReportDTO> list = new List<ReportDTO>();

                    decimal? sum = 0;
                    var listorder = await _context.Orders
                        .Where(x => x.OderDate >= fromDate && x.OderDate <= toDate && x.StaffId == staffId)
                        .ToListAsync();
                    var staff = await _context.staff.FirstOrDefaultAsync(x => x.StaffId == staffId);
                    foreach (var o in listorder)
                    {
                        sum += o.TotalMoney;
                    }

                    var newBenefit = new ReportDTO
                    {
                        StaffName = staff.FullName,
                        totalMoney = sum
                    };
                    list.Add(newBenefit);
                    decimal? benefit = sum;

                    return Ok(new { list, benefit });
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("CustomerReport")]
        public async Task<ResponsePagingReportCustomer> CustomerReport(int pageNumber)
        {
            try
            {
                var pagingHelper = new PagingHelper<ReportCustomerDTO>();
                int pageSize = 10;
                var customers = await _context.Customers.ToListAsync();
                List<ReportCustomerDTO> list = new List<ReportCustomerDTO>();
                foreach (var c in customers)
                {
                    var listorder = await _context.Orders.Where(x => x.CustomerId == c.CustomerId).ToListAsync();
                    decimal? sum = 0;
                    foreach (var o in listorder)
                    {
                        sum += o.TotalMoney;
                    }
                    var newReport = new ReportCustomerDTO
                    {
                        CustomerName = c.CustomerName,
                        TotalMoney = sum,
                        Address = c.Address,
                        Phone = c.Phone
                    };
                    list.Add(newReport);
                }

                var pagedData = pagingHelper.GetPagedData(list, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(list, pageSize);
                if (list == null)
                {
                    return new ResponsePagingReportCustomer { };
                }
                return new ResponsePagingReportCustomer { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("Doanhthutheothang")]
        public async Task<IActionResult> Doanhthutheothang(string monthyear)
        {
            var time = DateTime.Parse(monthyear);
            var listorder = await _context.Orders.Where(x => x.OderDate.Month == time.Month && x.OderDate.Year == time.Year)
                .ToListAsync();
            var listreceipt = await _context.Receipts.Where(x => x.Date.Month == time.Month && x.Date.Year == time.Year)
                .ToListAsync();
            decimal? sumOrder = 0;
            decimal? sumReceipt = 0;

            foreach (var o in listorder)
            {
                sumOrder += o.TotalMoney;
            }

            foreach (var o in listreceipt)
            {
                sumReceipt += o.TotalMoney;
            }

            decimal? benefit = sumOrder - sumReceipt;

            return Ok(benefit);
        }
        /*
        //thống kê doanh thu và số lượng product so với tháng trước
        [HttpGet("ProductReport")]
        public async Task<ResponsePagingReportProduct> ProductReport(int pageNumber)
        {
            try
            {
                var pagingHelper = new PagingHelper<ReportProductDTO>();
                int pageSize = 10;

                var monthNow = DateTime.Now.Month;
                var year = DateTime.Now.Year;
                var monthOld = monthNow - 1;

                List<ReportProductDTO> reportProductDTOs = new List<ReportProductDTO>();

                var listOrderNow = await _context.OrderDetails.Include(x => x.Product).Include(x => x.Order)
                    .Where(x => x.Order.OderDate.Month == monthNow && x.Order.OderDate.Year == year)
                    .ToListAsync();

                var listReceiptNow = await _context.Receipts.Include(x => x.Product).Include(x => x.Supplier)
                    .Where(x => x.Date.Month == monthNow && x.Date.Year == year)
                    .ToListAsync();

                var listOrderOld = await _context.OrderDetails.Include(x => x.Product).Include(x => x.Order)
                    .Where(x => x.Order.OderDate.Month == monthOld && x.Order.OderDate.Year == year)
                    .ToListAsync();

                var listReceiptOld = await _context.Receipts.Include(x => x.Product).Include(x => x.Supplier)
                    .Where(x => x.Date.Month == monthNow && x.Date.Year == year)
                    .ToListAsync();

                var listProduct = await _context.Products.ToListAsync();

                foreach (var p in listProduct)
                {
                    decimal? sumOrderNow = 0;
                    decimal? sumOrderOld = 0;

                    decimal? sumReceiptNow = 0;
                    decimal? sumReceiptOld = 0;

                    var listBenefitNow = listOrderNow.Where(x => x.ProductId == p.ProductId).ToList();
                    var listBenefitOld = listOrderOld.Where(x => x.ProductId == p.ProductId).ToList();
                    foreach (var Onow in listBenefitNow)
                    {
                        sumOrderNow += Onow.Order.TotalMoney;
                    }
                    foreach (var Oold in listBenefitOld)
                    {
                        sumOrderOld += Oold.Order.TotalMoney;
                    }

                    var receiptNow = listReceiptNow.Where(x => x.Product.ProductId == p.ProductId).ToList();
                    var receiptOld = listReceiptOld.Where(x => x.Product.ProductId == p.ProductId).ToList();

                    var countProductNow = listBenefitNow.Count();
                    var countProductOld = listBenefitOld.Count();

                    foreach (var Rnow in receiptNow)
                    {
                        sumReceiptNow += Rnow.TotalMoney;
                    }
                    foreach (var Rold in receiptOld)
                    {
                        sumReceiptOld += Rold.TotalMoney;
                    }
                    var newReport = new ReportProductDTO
                    {
                        ProductName = p.ProductName,
                        UnitInStock_Now = countProductNow,
                        UnitInStock_Old = countProductOld,
                        Image = p.Image,
                        TotalMoney_Now = sumOrderNow - sumReceiptNow,
                        TotalMoney_Old = sumOrderOld - sumReceiptOld,
                    };
                    reportProductDTOs.Add(newReport);
                }

                var pagedData = pagingHelper.GetPagedData(reportProductDTOs, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(reportProductDTOs, pageSize);
                return new ResponsePagingReportProduct { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        */
    }
}
