﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;

        public CategoryController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method get List of products
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
          Modify by: Duc VN
          Created on: 27/2/2024
        */
        [HttpGet("GetAll")]
        public async Task<ResponsePagingCategory?> GetAllCategory(int pageNumber, string? search)
        {
            try
            {
                var pagingHelper = new PagingHelper<CategoryDTO>();
                int pageSize = 10;
                if (search == null)
                {
                    var categories = await _context.Categories.Select(x => new CategoryDTO
                    {
                        CategoryId = x.CategoryId,
                        CategoryName = x.CategoryName,
                        Active = x.Active,
                    }).ToListAsync();

                    var pagedData = pagingHelper.GetPagedData(categories, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(categories, pageSize);
                    if (categories == null)
                    {
                        return new ResponsePagingCategory { };
                    }
                    return new ResponsePagingCategory { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var categories = await _context.Categories.Where(x => x.Active == true && x.CategoryName.ToLower().Contains(search.ToLower())).Select(x => new CategoryDTO
                    {
                        CategoryId = x.CategoryId,
                        CategoryName = x.CategoryName,
                        Active = x.Active,
                    }).ToListAsync();

                    var pagedData = pagingHelper.GetPagedData(categories, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(categories, pageSize);
                    if (categories == null)
                    {
                        return new ResponsePagingCategory { };
                    }
                    return new ResponsePagingCategory { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetAllCategory")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var categories = await _context.Categories.Where(x => x.Active == true).Select(x => new CategoryDTO
                {
                    CategoryId = x.CategoryId,
                    CategoryName = x.CategoryName,
                    Active = x.Active,
                }).ToListAsync();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method search Category by Name
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
          Modify by: Duc VN
          Created on: 27/2/2024(dd/mm/yyyy)
        */
        [HttpGet("SearchByName")]
        public async Task<IActionResult> SearchByName(string name)
        {
            try
            {
                var list = _context.Categories.Where(x => x.Active == true).ToList();
                var category = list.Where(c => c.CategoryName.ToLower().Contains(name.ToLower())).ToList();
                if (category == null)
                {
                    return NotFound("Category don't exist!!!");
                }
                return Ok(category);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 27/2/2024 (dd/mm/yyyy)
          Description: This method get Category by Id
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
        */
        [HttpGet("GetById")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var category = _context.Categories.FirstOrDefault(x => x.CategoryId == id);
                if (category == null)
                {
                    return NotFound("Category don't exist");
                }
                return Ok(category);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method add new Category
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
        */
        [HttpPost("Add")]
        public async Task<IActionResult> AddNewCategory(CategoryDTO categoryDTO)
        {
            try
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryName == categoryDTO.CategoryName);
                if (category == null)
                {
                    var newCategory = new Category
                    {
                        CategoryName = categoryDTO.CategoryName,
                        Active = true
                    };
                    _context.Categories.Add(newCategory);
                    _context.SaveChanges();
                    return Ok();
                }
                return BadRequest("Category exist!!!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method update Category 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
        */
        [HttpPut("Update")]
        public async Task<IActionResult> Update(CategoryDTO categoryDTO, int id)
        {
            try
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryId == id);
                if (category == null)
                {
                    return NotFound("Category don't exist!!!");
                }
                category.CategoryName = categoryDTO.CategoryName;
                _context.SaveChanges();
                return Ok();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 27/2/2024 (dd/mm/yyyy)
          Description: This method change active Category 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
        */
        [HttpPut("Block")]
        public async Task<IActionResult> Block(int id)
        {
            try
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.CategoryId == id);
                if (category != null)
                {
                    if (category.Active == true)
                    {
                        category.Active = false;
                        _context.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        category.Active = true;
                        _context.SaveChanges();
                        return Ok();
                    }
                }
                return NotFound("Category don't exist");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method search Category by Name
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
          Modify by: Duc VN
          Created on: 27/2/2024(dd/mm/yyyy)
        */
        [HttpGet("GetIdByName")]
        public async Task<IActionResult> GetIdByName(string name)
        {
            try
            {
                var category = _context.Categories.FirstOrDefault(x => x.Active == true && x.CategoryName.ToLower().Equals(name.ToLower()));
                if (category == null)
                {
                    return NotFound("Category don't exist!!!");
                }
                return Ok(category.CategoryId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
