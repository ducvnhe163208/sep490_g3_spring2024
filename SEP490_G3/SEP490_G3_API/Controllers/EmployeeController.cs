﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;

        public EmployeeController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method get list Employee 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
          Modify by: Duc VN
          Created on: 28/2/2024(dd/mm/yyyy)
          ---
          Modify by: Duc VN
          Created on: 13/3/2024 (dd/mm/yyyy)
        */
        [HttpGet("GetAll")]
        public async Task<ResponsePagingStaff?> GetAllEmployee(int pageNumber, string? search)
        {
            try
            {
                var pagingHelper = new PagingHelper<EmployeeDTO>();
                int pageSize = 5;
                if (search == null)
                {
                    var employees = await _context.staff.Include(x => x.Role).Select(x => new EmployeeDTO
                    {
                        Id = x.StaffId,
                        RoleId = x.RoleId,
                        RoleName = x.Role.RoleName,
                        FullName = x.FullName,
                        Phone = x.Phone,
                        Active = x.Active,
                        Address = x.Address,
                        Email = x.Email,
                        Username = x.Username,
                        Password = x.Password,
                        Avatar = x.Avatar,
                        CitizenIdentificationCard = x.CitizenIdentificationCard,
                        BankAccountNumber = x.BankAccountNumber,
                        TaxCode = x.TaxCode
                    }).ToListAsync();


                    var pagedData = pagingHelper.GetPagedData(employees, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(employees, pageSize);

                    return new ResponsePagingStaff { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var staff = await _context.staff.Include(x => x.Role).Where(x => x.FullName.ToLower().Contains(search.ToLower()) || x.Phone.Contains(search)).ToListAsync();
                    var staffs = staff.Select(x => new EmployeeDTO
                    {
                        Id = x.StaffId,
                        RoleId = x.RoleId,
                        RoleName = x.Role.RoleName,
                        FullName = x.FullName,
                        Phone = x.Phone,
                        Active = x.Active,
                        Address = x.Address,
                        Email = x.Email,
                        Username = x.Username,
                        Password = x.Password,
                        Avatar = x.Avatar,
                        CitizenIdentificationCard = x.CitizenIdentificationCard,
                        BankAccountNumber = x.BankAccountNumber,
                        TaxCode = x.TaxCode
                    }).ToList();
                    if (staff.Count == 0)
                    {
                        return new ResponsePagingStaff { };
                    }
                    var pagedData = pagingHelper.GetPagedData(staffs, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(staffs, pageSize);
                    return new ResponsePagingStaff { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetAllStaff")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var employees = await _context.staff.Include(x => x.Role).Select(x => new EmployeeDTO
                {
                    Id = x.StaffId,
                    RoleId = x.RoleId,
                    RoleName = x.Role.RoleName,
                    FullName = x.FullName,
                    Phone = x.Phone,
                    Active = x.Active,
                    Address = x.Address,
                    Email = x.Email,
                    Username = x.Username,
                    Password = x.Password,
                    Avatar = x.Avatar,
                    CitizenIdentificationCard = x.CitizenIdentificationCard,
                    BankAccountNumber = x.BankAccountNumber,
                    TaxCode = x.TaxCode
                }).ToListAsync();
                return Ok(employees);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method add new Employee 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpPost("Add")]
        public async Task<IActionResult> AddNewEmployee(AddEmployeeDTO employeeDTO)
        {
            try
            {
                var employee = await _context.staff.FirstOrDefaultAsync(x => x.Phone == employeeDTO.Phone);
                if (employee == null)
                {
                    var newEmployee = new staff
                    {
                        RoleId = employeeDTO.RoleId,
                        FullName = employeeDTO.FullName,
                        Username = employeeDTO.Username,
                        Password = SHA256Helper.SHA256Hash(employeeDTO.Password),
                        Email = employeeDTO.Email,
                        Phone = employeeDTO.Phone,
                        BankAccountNumber = employeeDTO.BankAccountNumber,
                        CitizenIdentificationCard = employeeDTO.CitizenIdentificationCard,
                        Avatar = employeeDTO.Avatar,
                        Address = employeeDTO.Address,
                        TaxCode = employeeDTO.TaxCode,
                        Active = true
                    };
                    _context.staff.Add(newEmployee);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                return BadRequest("Employee existed!!!");
            }catch (DbUpdateException ex)
            {
                // Xử lý ngoại lệ
                // Hiển thị ngoại lệ chính
                Console.WriteLine("Error occurred while saving changes: " + ex.Message);

                // Xem ngoại lệ bên trong để biết thông tin chi tiết
                if (ex.InnerException != null)
                {
                    Console.WriteLine("Inner exception: " + ex.InnerException.Message);
                }
                return BadRequest("An error occurred while saving changes.");
                // Nếu cần, có thể thêm các xử lý khác tại đây
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method search Employee by Name or Phone 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpGet("SearchByNameOrPhone")]
        public async Task<IActionResult> SearchByNameOrPhone(string search)
        {
            try
            {
                var staff = await _context.staff.Where(x => x.FullName.ToLower().Contains(search.ToLower()) || x.Phone.Contains(search)).ToListAsync();
                if (staff.Count > 0)
                {
                    return Ok(staff);
                }
                return NotFound("Employee don't exist!!!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 27/2/2024 (dd/mm/yyyy)
          Description: This method get Employee by Id 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpGet("GetById")]
        public IActionResult GetById(int id)
        {
            try
            {
                var employee = _context.staff.Where(x => x.Active == true).ToList();
                var employ = employee.FirstOrDefault(x => x.StaffId == id);
                if (employ == null)
                {
                    return NotFound("Employee don't exist");
                }
                return Ok(employ);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method update Employee 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpPut("Update")]
        public async Task<IActionResult> Update(int id, AddEmployeeDTO employeeDTO)
        {
            try
            {
                var staff = await _context.staff.FirstOrDefaultAsync(x => x.StaffId == id);
                if (staff != null)
                {
                    staff.FullName = employeeDTO.FullName;
                    staff.Phone = employeeDTO.Phone;
                    staff.Email = employeeDTO.Email;
                    staff.Username = employeeDTO.Username;
                    if (staff.Password != SHA256Helper.SHA256Hash(employeeDTO.Password))
                    {
                        staff.Password = SHA256Helper.SHA256Hash(employeeDTO.Password);
                    }
                    staff.RoleId = employeeDTO.RoleId;
                    staff.BankAccountNumber = employeeDTO.BankAccountNumber;
                    staff.Avatar = employeeDTO.Avatar;
                    staff.Address = employeeDTO.Address;
                    staff.CitizenIdentificationCard = employeeDTO.CitizenIdentificationCard;
                    staff.TaxCode = employeeDTO.TaxCode;
                    _context.SaveChanges();
                    return Ok();
                }
                return NotFound("Employee don't exist!!!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 28/2/2024 (dd/mm/yyyy)
          Description: This method block Employee 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpDelete("Block")]
        public async Task<IActionResult> Block(int id)
        {
            try
            {
                var employee = await _context.staff.FirstOrDefaultAsync(x => x.StaffId == id);
                if (employee != null)
                {
                    if (employee.Active == true)
                    {
                        employee.Active = false;
                    }
                    else
                    {
                        employee.Active = true;
                    }
                    _context.SaveChanges();
                    return Ok();
                }
                return BadRequest("Employee don't exist!!!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 1/4/2024 (dd/mm/yyyy)
          Description: This method Get All Role of Staff 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpGet("GetAllRole")]
        public async Task<IActionResult> GetAllRole()
        {
            try
            {
                var roles = await _context.Roles.ToListAsync();
                var role = roles.Select(x => new Role
                {
                    RoleId = x.RoleId,
                    RoleName = x.RoleName
                }).ToList();
                return Ok(role);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 1/4/2024 (dd/mm/yyyy)
          Description: This method add new role of staff
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\EmployeeController.cs
          ---
        */
        [HttpPost("AddRole")]
        public async Task<IActionResult> AddNewRole(RoleDTO roleDTO)
        {
            try
            {
                var role = await _context.Roles.FirstOrDefaultAsync(x => x.RoleName.ToLower().Equals(roleDTO.RoleName.ToLower()));
                if (role == null)
                {
                    var newRole = new Role
                    {
                        RoleName = roleDTO.RoleName
                    };
                    _context.Roles.Add(newRole);
                    _context.SaveChanges();
                    return Ok();
                }
                return BadRequest("Role existed!!!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetBillById")]
        public async Task<ResponsePagingBill?> GetBillById(int id, string start, string end, int pageNumber)
        {
            var pagingHelper = new PagingHelper<BillDTO>();
            int pageSize = 10;
            var from = DateTime.Parse(start);
            var to = DateTime.Parse(end);
            var bill = await _context.Orders.Where(x => x.StaffId == id && x.OderDate >= from && x.OderDate <= to).Select(order => new BillDTO
            {
                CustomerName = order.Customer.CustomerName,
                OderDate = order.OderDate,
                TotalMoney = order.TotalMoney
            }).ToListAsync();
            var pagedData = pagingHelper.GetPagedData(bill, pageNumber, pageSize);

            var totalPages = pagingHelper.CalculateTotalPages(bill, pageSize);
            if (bill == null)
            {
                return new ResponsePagingBill { };
            }
            return new ResponsePagingBill { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
        }
    }
}
