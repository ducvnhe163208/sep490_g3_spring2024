﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;
        public SalaryController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
        //Tạo ca làm việc
        [HttpPost("SetTimeSlot")]
        public async Task<IActionResult> SetTimeSlot(Timeslot timeslot)
        {
            try
            {
                var slot = await _context.Timeslots.FirstOrDefaultAsync(x => x.TimeslotId == timeslot.TimeslotId);
                if (slot == null)
                {
                    var newSlot = new Timeslot
                    {
                        TimeslotId = timeslot.TimeslotId,
                        Timeslot1 = timeslot.Timeslot1,
                    };
                    await _context.Timeslots.AddAsync(newSlot);
                    await _context.SaveChangesAsync();
                    return Ok(newSlot);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpPut("UpdateTimeSlot")]
        public async Task<IActionResult> UpdateTimeSlot(TimeslotDTO timeslot, int id)
        {
            try
            {
                var listTimeSlot = await _context.Timeslots.ToListAsync();
                var slot = await _context.Timeslots.FirstOrDefaultAsync(x => x.TimeslotId == id);
                if (slot != null)
                {
                    foreach (var item in listTimeSlot)
                    {
                        if (item.Timeslot1.ToLower().Equals(timeslot.Timeslot1.ToLower()))
                        {
                            return BadRequest();
                        }
                    }
                    slot.Timeslot1 = timeslot.Timeslot1;
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        */
        [HttpGet("GetAllTimeSlot")]
        public async Task<IActionResult> GetAllTimeSlot()
        {
            try
            {
                var timeslot = await _context.Timeslots.ToListAsync();
                return Ok(timeslot);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
        [HttpDelete("DeleteTimeslot")]
        public async Task<IActionResult> DeleteTimeSlot(int id)
        {
            try
            {
                var timeSlot = await _context.Timeslots.FirstOrDefaultAsync(x => x.TimeslotId == id);
                if (timeSlot != null)
                {
                    var session = await _context.Sessions.Where(x => x.TimeslotId == timeSlot.TimeslotId).ToListAsync();
                    if (session != null)
                    {
                        _context.Timeslots.Remove(timeSlot);
                        _context.Sessions.RemoveRange(session);
                        await _context.SaveChangesAsync();
                        return Ok();
                    }
                    _context.Timeslots.Remove(timeSlot);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Nhân viên xem lịch làm việc của mình
        [HttpGet("GetAllAttendanceByStaff")]
        public async Task<IActionResult> GetAllAttendanceByStaff(int staffId, DateTime start, DateTime end)
        {
            try
            {
                var timeTable = await _context.Attendances.Include(x => x.Session).ThenInclude(x => x.Timeslot).Include(x => x.Staff).Where(x => x.StaffId == staffId && x.Session.Date >= start && x.Session.Date <= end).Select(x => new SessionDTO
                {
                    Date = x.Session.Date,
                    TimeslotId = x.Session.TimeslotId,
                    Timeslot1 = x.Session.Timeslot.Timeslot1,
                    Status = x.Status,
                    FullName = x.Staff.FullName,
                    Note = x.Note
                }).ToListAsync();
                return Ok(timeTable);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        */
        [HttpGet("GetAllAttendanceForShopOwner")]
        public async Task<IActionResult> GetAllAttendanceForShopOwner(int month)
        {
            try
            {
                var timeTable = await _context.Attendances.Include(x => x.Session).ThenInclude(x => x.Timeslot).Include(x => x.Staff).Select(x => new SessionDTO
                {
                    Date = x.Session.Date,
                    TimeslotId = x.Session.TimeslotId,
                    Timeslot1 = x.Session.Timeslot.Timeslot1,
                    Status = x.Status,
                    FullName = x.Staff.FullName,
                    Note = x.Note
                }).ToListAsync();
                return Ok(timeTable);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Tạo biểu đồ làm việc sau khi staff điểm danh
        [HttpPost("CreateSessions")]
        public async Task<IActionResult> CreateSessions(int timeslotid, string? note, int staffid)
        {
            try
            {
                DateTime dateSession = DateTime.Now.Date;
                var existingSession = await _context.Sessions.FirstOrDefaultAsync(x => x.Date.Date == dateSession);
                var staffs = await _context.staff.CountAsync(x => x.Active == true);
                var listIdStaff = await _context.staff.ToListAsync();
                if (existingSession == null)
                {
                    var firstSession = new Session
                    {
                        Date = DateTime.Now,
                        TimeslotId = timeslotid,
                    };
                    _context.Sessions.Add(firstSession);
                    await _context.SaveChangesAsync();

                    var firstAttendance = new Attendance
                    {
                        Status = true,
                        StaffId = staffid,
                        Note = note + ("\nThời gian chấm công: " + DateTime.Now.Hour + "h : " + DateTime.Now.Minute + "p"),
                        SessionId = firstSession.SessionId,
                    };
                    _context.Attendances.Add(firstAttendance);
                    await _context.SaveChangesAsync();

                    for (var i = 0; i < staffs - 1; i++)
                    {
                        var allSession = new Session
                        {
                            Date = DateTime.Now,
                        };
                        _context.Sessions.Add(allSession);
                        await _context.SaveChangesAsync();

                        var allAttendance = new Attendance
                        {
                            Status = false,
                            SessionId = allSession.SessionId,
                        };
                        _context.Attendances.Add(allAttendance);
                        await _context.SaveChangesAsync();
                    }

                    return Ok();
                }
                else
                {
                    var existStaff = await _context.Attendances.FirstOrDefaultAsync(x => x.StaffId == staffid);
                    if(existStaff != null)
                    {
                        return BadRequest();
                    }
                    var attend = await _context.Attendances.Include(x => x.Session).FirstOrDefaultAsync(x => x.StaffId == null && x.Session.Date.Date == DateTime.Now.Date);
                    attend.StaffId = staffid;
                    attend.Status = true;
                    attend.Note = note + ("\nThời gian chấm công: " + DateTime.Now.Hour + "h : " + DateTime.Now.Minute + "p");

                    var session = await _context.Sessions.FirstOrDefaultAsync(x => x.SessionId == attend.SessionId);
                    session.TimeslotId = timeslotid;

                    await _context.SaveChangesAsync();

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("GetAttendInDay")]
        public async Task<IActionResult> GetAttentInDay()
        {
            try
            {
                var online = await _context.Attendances.Include(x => x.Session).CountAsync(x => x.Status == true && x.Session.Date.Date == DateTime.Now.Date);

                var offline = await _context.Attendances.Include(x => x.Session).CountAsync(x => x.Status == false && x.Session.Date.Date == DateTime.Now.Date);

                return Ok(new { online, offline });

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("GetAttendByDate")]
        public async Task<ResponsePagingSession> GetAttendByDate(string? start, string? end, int pageNumber)
        {
            try
            {
                var from = DateTime.Parse(start);
                var to = DateTime.Parse(end);
                var pagingHelper = new PagingHelper<SessionDTO>();
                int pageSize = 10;
                if (start != null || end != null)
                {
                    var staff = await _context.Attendances.Include(x => x.Session).ThenInclude(x => x.Timeslot)
                        .Where(x => x.Session.Date >= from && x.Session.Date <= to)
                        .Select(x => new SessionDTO
                        {
                            Date = x.Session.Date.Date,
                            TimeslotId = x.Session.TimeslotId,
                            Timeslot1 = x.Session.Timeslot.Timeslot1,
                            Note = x.Note,
                            Status = x.Status,
                            FullName = x.Staff.FullName
                        }).ToListAsync();
                    var pagedData = pagingHelper.GetPagedData(staff, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(staff, pageSize);
                    return new ResponsePagingSession { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var staff = await _context.Attendances.Include(x => x.Session).ThenInclude(x => x.Timeslot)
                        .Where(x => x.Session.Date.Month == DateTime.Now.Month && x.Session.Date.Year == DateTime.Now.Year)
                        .Select(x => new SessionDTO
                        {
                            Date = x.Session.Date.Date,
                            TimeslotId = x.Session.TimeslotId,
                            Timeslot1 = x.Session.Timeslot.Timeslot1,
                            Note = x.Note,
                            Status = x.Status,
                            FullName = x.Staff.FullName
                        }).ToListAsync();
                    var pagedData = pagingHelper.GetPagedData(staff, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(staff, pageSize);
                    return new ResponsePagingSession { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("GetAttendByMonth")]
        public async Task<ResponsePagingSalary> GetAttendByMonth(string time, int pageNumber, decimal commission, decimal hard_salary)
        {
            try
            {
                var pagingHelper = new PagingHelper<SalaryDTO>();
                int pageSize = 5;

                var datetime = DateTime.Parse(time);

                var liststaff = await _context.staff.Where(x => x.Active == true).ToListAsync();

                List<SalaryDTO> listsalary = new List<SalaryDTO>();

                foreach (var a in liststaff)
                {
                    var attendances = await _context.Attendances.Include(x => x.Session)
                        .ThenInclude(x => x.Timeslot)
                        .Where(x => x.Session.Date.Month == datetime.Month && x.Session.Date.Year == datetime.Year && x.StaffId == a.StaffId)
                        .ToListAsync();

                    int sumDayPresent = attendances.Count(x => x.Status == true);
                    int sumDayAbsent = attendances.Count(x => x.Status != true);

                    var orders = await _context.Orders.Where(x => x.StaffId == a.StaffId && x.OderDate.Month == datetime.Month && x.OderDate.Year == datetime.Year).ToListAsync();
                    decimal totalSalary = (decimal)orders.Sum(x => x.TotalMoney);
                    decimal bonus = (decimal)(commission * totalSalary) / 100;

                    decimal calculate = (sumDayPresent * (hard_salary * 4)) + bonus;

                    var newSalary = new SalaryDTO
                    {
                        StaffName = a.FullName,
                        Salary = calculate
                    };

                    listsalary.Add(newSalary);
                }

                var pagedData = pagingHelper.GetPagedData(listsalary, pageNumber, pageSize);
                var totalPages = pagingHelper.CalculateTotalPages(listsalary, pageSize);

                return new ResponsePagingSalary { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("ReportStaff")]
        public async Task<ResponsePagingStaffAttend> ReportStaff(int pageNumber)
        {
            try
            {
                var staffs = await _context.staff.Where(x => x.Active == true).ToListAsync();
                var attend = await _context.Attendances.Include(x => x.Session).Include(x => x.Staff).Where(x => x.Session.Date.Month == DateTime.Now.Month && x.Session.Date.Year == DateTime.Now.Year).ToListAsync();

                var pagingHelper = new PagingHelper<StaffDTO>();
                int pageSize = 5;

                List<StaffDTO> listStaff = new List<StaffDTO>();

                foreach (var a in staffs)
                {
                    int sumDayPresent = 0;
                    int sumDayAbsent = 0;

                    var attendances = await _context.Attendances.Where(x => x.StaffId == a.StaffId && x.Session.Date.Month == DateTime.Now.Month && x.Session.Date.Year == DateTime.Now.Year)
                        .ToListAsync();
                    foreach (var attendance in attendances)
                    {
                        if (attendance.Status == true)
                        {
                            sumDayPresent++;
                        }
                        else
                        {
                            sumDayAbsent++;
                        }
                    }
                    var newStaff = new StaffDTO
                    {
                        StaffName = a.FullName,
                        Present = sumDayPresent,
                        Absent = sumDayAbsent,
                    };
                    listStaff.Add(newStaff);
                }

                var pagedData = pagingHelper.GetPagedData(listStaff, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(listStaff, pageSize);

                return new ResponsePagingStaffAttend { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //Staff chấm công
        [HttpPut("Attendance")]
        public async Task<IActionResult> Attendance(int staffid)
        {
            try
            {
                var date = DateTime.Now.Day;
                var attendances = await _context.Attendances.Include(x => x.Session).ThenInclude(x => x.Timeslot).ToListAsync();
                var attendance = attendances.FirstOrDefault(x => x.StaffId == staffid && x.Session.Date.Day == date);

                if (attendance.Status == false)
                {
                    attendance.Status = true;
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
