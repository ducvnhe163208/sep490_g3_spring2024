﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;
using System.Numerics;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;

        public CustomerController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method get list of Customer 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CustomerController.cs
          ---
          Modify by: Duc VN
          Created on: 27/2/2024 (dd/mm/yyyy)
        */
        [HttpGet("GetAll")]
        public async Task<ResponsePagingCustomer?> GetAllCustomer(int pageNumber, string? search)
        {
            try
            {
                int pageSize = 10;
                var pagingHelper = new PagingHelper<CustomerDTO>();
                if (search == null)
                {
                    var customers = await _context.Customers.Select(x => new CustomerDTO
                    {
                        CustomerId = x.CustomerId,
                        CustomerName = x.CustomerName,
                        Phone = x.Phone,
                        Email = x.Email,
                        Address = x.Address,
                        Active = x.Active,
                    }).ToListAsync();

                    var pagedData = pagingHelper.GetPagedData(customers, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(customers, pageSize);
                    if (customers == null)
                    {
                        return new ResponsePagingCustomer { };
                    }
                    return new ResponsePagingCustomer { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var list = await _context.Customers.Where(x => x.Active == true).ToListAsync();
                    var customer = list.Where(c => c.Phone.ToLower().Contains(search.ToLower())).ToList();
                    var customers = await _context.Customers.Where(x => x.Active == true && x.Phone.ToLower().Contains(search.ToLower())).Select(x => new CustomerDTO
                    {
                        CustomerId = x.CustomerId,
                        CustomerName = x.CustomerName,
                        Phone = x.Phone,
                        Email = x.Email,
                        Address = x.Address,
                        Active = x.Active,
                    }).ToListAsync();
                    if (customer == null)
                    {
                        return new ResponsePagingCustomer { };
                    }
                    var pagedData = pagingHelper.GetPagedData(customers, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(customers, pageSize);
                    return new ResponsePagingCustomer { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetAllCustomer")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var customers = await _context.Customers.Select(x => new CustomerDTO
                {
                    CustomerId = x.CustomerId,
                    CustomerName = x.CustomerName,
                    Phone = x.Phone,
                    Email = x.Email,
                    Address = x.Address,
                    Active = x.Active,
                }).ToListAsync();
                return Ok(customers);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method search Customer by Phone 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CustomerController.cs
          ---
          Modify by: Duc VN
          Created on: 27/2/2024(dd/mm/yyyy)
        */
        [HttpGet("SearchByPhone")]
        public async Task<IActionResult> SearchByPhone(string phone)
        {
            try
            {
                var list = _context.Customers.Where(x => x.Active == true).ToList();
                var customer = list.Where(c => c.Phone.ToLower().Contains(phone.ToLower()));
                if (customer == null)
                {
                    return NotFound("Customer don't exist");
                }

                return Ok(customer);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 29/2/2024 (dd/mm/yyyy)
          Description: This method get Customer by Id 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CustomerController.cs
          ---
        */
        [HttpGet("GetById")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var customer = _context.Customers.Where(x => x.Active == true).ToList();
                var cus = customer.FirstOrDefault(x => x.CustomerId == id);
                if (cus == null)
                {
                    return NotFound("Employee don't exist");
                }
                return Ok(cus);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method add new Customer 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CustomerController.cs
          ---
          Modify by: NghiaLD
          Created on: 5/4/2024 (dd/mm/yyyy)
        */
        [HttpPost("Add")]
        public async Task<IActionResult> AddNewCustomer(CustomerDTO customerDTO)
        {
            try
            {
                var customer = await _context.Customers.FirstOrDefaultAsync(x => x.Phone == customerDTO.Phone);
                if (customer != null)
                {
                    //return BadRequest("Customer exist");
                    return Ok(customer.CustomerId);
                }
                var newCustomer = new Customer
                {
                    CustomerName = customerDTO.CustomerName,
                    Phone = customerDTO.Phone,
                    Email = customerDTO.Email,
                    Address = customerDTO.Address,
                    Active = true,
                };
                _context.Customers.Add(newCustomer);
                _context.SaveChanges();
                return Ok(newCustomer.CustomerId);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method update Customer 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CustomerController.cs
          ---
        */
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateCustomer(CustomerDTO customerDTO, int id)
        {
            try
            {
                //Tim customer theo id
                var customer = await _context.Customers.FirstOrDefaultAsync(c => c.CustomerId == id);
                if (customer != null)
                {
                    //Sua du lieu trong database
                    customer.CustomerName = customerDTO.CustomerName;
                    customer.Phone = customerDTO.Phone;
                    customer.Email = customerDTO.Email;
                    customer.Address = customerDTO.Address;
                    _context.SaveChanges();
                    return Ok();
                }
                return NotFound("Customer don't exist");
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 27/2/2024 (dd/mm/yyyy)
          Description: This method block Customer 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CustomerController.cs
          ---
        */
        [HttpPut("Block")]
        public async Task<IActionResult> Block(int id)
        {
            try
            {
                var customer = await _context.Customers.FirstOrDefaultAsync(c => c.CustomerId == id);
                if (customer != null)
                {
                    if (customer.Active == true)
                    {
                        customer.Active = false;
                        _context.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        customer.Active = true;
                        _context.SaveChanges();
                        return Ok();
                    }
                }
                return NotFound("Customer don't exist");
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
