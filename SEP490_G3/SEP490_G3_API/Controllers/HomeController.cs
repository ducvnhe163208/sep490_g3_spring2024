﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MimeKit;
using SEP490_G3_API.DTO;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.IServiceHelper;
using SEP490_G3_API.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection.Metadata.Ecma335;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        static HttpClient client = new HttpClient();
        private readonly IConfiguration _config;
        private readonly SEP490_G3_test4Context _context1;
        private readonly SEP490_G3_SiteContext _context2;
        private readonly IMailKitService _mailKitService;
        public HomeController(IMailKitService mailKitService, SEP490_G3_test4Context context1, SEP490_G3_SiteContext context2, IConfiguration config)
        {
            _mailKitService = mailKitService;
            _context1 = context1;
            _context2 = context2;
            _config = config;
        }
        /*
          Created on: 4/3/2024 (dd/mm/yyyy)
          Description: This method Register for User
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\HomeController.cs
          ---
          Modify by: Duc VN
          Created on: 5/3/2024 (dd/mm/yyyy)
          -- 
          Modify by: Duc VN
          Created on: 10/3/2024 (dd/mm/yyyy)
          -- 
          Modify by: Duc VN
          Created on: 19/3/2024 (dd/mm/yyyy)
          -- 
          Modify by: NghiaLD
          Created on: 22/3/2024 (dd/mm/yyyy)  
        */
        [HttpPost("Register")]
        public IActionResult Register(UserDTO user)
        {
            try
            {
                var checkSite = _context2.Sites.FirstOrDefault(x => x.SiteName == user.Phone);
                if (checkSite == null)
                {
                    var checkUser = _context2.Users.FirstOrDefault(x => x.UserName == user.UserName);
                    if (checkUser == null)
                    {
                        DateTime enddate = DateTime.Now.AddMonths(1);
                        var newUser = new User
                        {
                            UserName = user.UserName,
                            //ma hoa mat khau truoc khi luu vao database
                            Password = SHA256Helper.SHA256Hash(user.Password),
                            Logo = user.Logo,
                            Phone = user.Phone,
                            Email = user.Email,
                            TaxCode = user.TaxCode,
                            CitizenIdentificationCard = user.CitizenIdentificationCard,
                            BankAccountNumber = user.BankAccountNumber,
                            StartDate = DateTime.Now,
                            EndDate = enddate
                        };

                        _context2.Add(newUser);
                        _context2.SaveChanges();

                        var newSite = new Site
                        {
                            SiteName = user.Phone,
                            UserId = newUser.UserId
                        };

                        _context2.Add(newSite);
                        _context2.SaveChanges();

                        var optionsBuilder = new DbContextOptionsBuilder<SEP490_G3_test4Context>();
                        var newContext = new SEP490_G3_test4Context(optionsBuilder.Options, newSite);

                        newContext.Database.EnsureCreated();

                        var newAdmin = new Admin
                        {
                            FullName = user.FullName,
                            Phone = user.Phone,
                            Email = user.Email,
                            Avatar = user.Logo,
                            TaxCode = user.TaxCode,
                            CitizenIdentificationCard = user.CitizenIdentificationCard,
                            BankAccountNumber = user.BankAccountNumber,
                            BankBin = user.BankBin
                        };
                        newContext.Add(newAdmin);
                        newContext.SaveChanges();

                        var role = new Role
                        {
                            RoleName = "Staff"
                        };

                        newContext.Add(role);
                        newContext.SaveChanges();

                        var newEmployee = new staff
                        {
                            RoleId = null,
                            FullName = user.FullName,
                            Username = user.UserName,
                            Password = SHA256Helper.SHA256Hash(user.Password),
                            Email = user.Email,
                            Phone = user.Phone,
                            BankAccountNumber = user.BankAccountNumber,
                            CitizenIdentificationCard = user.CitizenIdentificationCard,
                            Avatar = user.Logo,
                            Address = null,
                            TaxCode = user.TaxCode,
                            Active = true
                        };
                        newContext.Add(newEmployee);
                        newContext.SaveChanges();

                        var timeslot = newContext.Timeslots.ToList();
                        if (timeslot.Count == 0)
                        {
                            var newTimeSlot1 = new Timeslot
                            {
                                TimeslotId = 1,
                                Timeslot1 = ("8h - 12h")
                            };
                            var newTimeSlot2 = new Timeslot
                            {
                                TimeslotId = 2,
                                Timeslot1 = ("12h - 16h")
                            };
                            var newTimeSlot3 = new Timeslot
                            {
                                TimeslotId = 3,
                                Timeslot1 = ("16h - 20h")
                            };

                            newContext.AddRange(newTimeSlot1, newTimeSlot2, newTimeSlot3);
                            newContext.SaveChanges();
                        }

                        return Ok();
                    }
                    return BadRequest("UserName Exist");
                }
                return BadRequest("Phone Exist");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpPost("ForgetPassword")]
        public async Task<IActionResult> ForgotPassword(string username)
        {
            try
            {
                var user = await _context2.Users.FirstOrDefaultAsync(x => x.UserName == username);
                if (user == null)
                {
                    return BadRequest();
                }
                string email = user.Email;
                string password = _mailKitService.GenerateRandomPassword();

                string message = "Mật khẩu của bạn là: " + password + ".\nCảm ơn đã sử dụng dịch vụ của chúng tôi";
                await _mailKitService.SendEmailAsync(email, "Forgot Password Service", message);

                user.Password = SHA256Helper.SHA256Hash(password);
                await _context2.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [HttpPut("ChangePassword")]
        public async Task<IActionResult> ChangePassword(int userid, string oldpass, string newpass)
        {
            try
            {
                var user = await _context2.Users.FirstOrDefaultAsync(x => x.UserId == userid);
                var passHash = SHA256Helper.SHA256Hash(oldpass);
                if (user.Password == passHash)
                {
                    user.Password = SHA256Helper.SHA256Hash(newpass);
                    await _context2.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created on: 10/3/2024 (dd/mm/yyyy)
          Description: This method Login for User
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\HomeController.cs
          ---
          Modify by: DucVN
          Created on: 14/3/2024 (dd/mm/yyyy)
        */
        [HttpGet("Login")]
        public IActionResult Login(string username, string password)
        {
            try
            {
                //ma hoa pass truoc khi dang nhap 
                var passHash = SHA256Helper.SHA256Hash(password);
                User login = new User();
                login.UserName = username;
                login.Password = passHash;
                IActionResult response = Unauthorized();

                var user = AuthenticationUser(login);

                var deadline = DateTime.Now;
                if (user != null)
                {
                    //Kiem tra xem tai khoan con han su dung khong, neu khong thi k cho dang nhap
                    if (login.EndDate <= deadline)
                    {
                        var tokenStr = GenerateJSONWebToken(user);
                        response = Ok(new { token = tokenStr, userid = user.UserId, phoneNumber = user.Phone });
                    }
                    else
                    {
                        response = BadRequest();
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created on: 14/3/2024 (dd/mm/yyyy)
          Description: This method authen User
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\HomeController.cs
          ---
        */
        private User AuthenticationUser(User login)
        {
            List<User> users = _context2.Users.ToList();
            User user = null;
            foreach (var u in users)
            {
                if (login.UserName == u.UserName && login.Password == u.Password)
                {
                    user = new User { UserId = u.UserId, UserName = u.UserName, Password = u.Password, Phone = u.Phone };
                }
            }
            return user;
        }
        /*
          Created on: 14/3/2024 (dd/mm/yyyy)
          Description: This method genarate token 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\HomeController.cs
          ---
        */
        private string GenerateJSONWebToken(User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(
                issuer: _config["Jwt: Issue"],
                audience: _config["Jwt: Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials
                );

            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }

        [HttpGet("StaffLogin")]
        public IActionResult StaffLogin(string phone, string username, string password)
        {
            try
            {
                var passHash = SHA256Helper.SHA256Hash(password);
                var site = _context2.Sites.FirstOrDefault(x => x.SiteName == phone);
                if (site == null)
                {
                    return BadRequest("Wrong site!");
                }
                else
                {
                    //ma hoa pass truoc khi dang nhap 
                    //var passHash = SHA256Helper.SHA256Hash(password);
                    staff login = new staff();
                    login.Username = username;
                    login.Password = passHash;
                    IActionResult response = Unauthorized();

                    var staff = AuthenticationStaff(login, site);

                    if (staff != null)
                    {
                        var tokenStr = GenerateJSONWebTokenForStaff(staff);
                        response = Ok(new { token = tokenStr, userid = staff.StaffId, phoneNumber = phone, role = staff.RoleId });
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private staff AuthenticationStaff(staff login, Site site)
        {
            //var dynamicConnectionString = $"server=localhost; database={phone};uid=sa;pwd=123;TrustServerCertificate=True;";
            var optionsBuilder = new DbContextOptionsBuilder<SEP490_G3_test4Context>();
            //optionsBuilder.UseSqlServer(dynamicConnectionString);
            //var site = _context2.Sites.FirstOrDefault(x => x.SiteName == site);
            var newContext = new SEP490_G3_test4Context(optionsBuilder.Options, site);
            List<staff> users = newContext.staff.ToList();
            staff staff = null;
            foreach (var u in users)
            {
                if (login.Username == u.Username)
                {
                    if (login.Password == u.Password)
                    {
                        staff = new staff { Username = login.Username, Password = login.Password, RoleId = u.RoleId, StaffId = u.StaffId };
                    }
                }
            }
            return staff;
        }
        /*
          Created on: 14/3/2024 (dd/mm/yyyy)
          Description: This method genarate token 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\HomeController.cs
          ---
        */
        private string GenerateJSONWebTokenForStaff(staff staff)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, staff.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(
                issuer: _config["Jwt: Issue"],
                audience: _config["Jwt: Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials
                );

            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }


    }
}
