﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReceiptController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;

        public ReceiptController(SEP490_G3_test4Context context)
        {
            _context = context;
        }

        [HttpGet("GetAllReceipt")]
        public async Task<ResponsePagingReceipt> GetAllReceipt(int pageNumber)
        {
            try
            {
                var pagingHelper = new PagingHelper<ReceiptDTO>();
                int pageSize = 10;

                var receipts = await _context.Receipts.Include(x => x.Product)
                    .Include(x => x.Supplier)
                    .Select(x => new ReceiptDTO
                    {
                        ProductId = x.ProductId,
                        ProductName = x.Product.ProductName,
                        ProductImage = x.Product.Image,
                        Unit = x.Product.Unit,
                        SupplierId = x.SupplierId,
                        SupplierName = x.Supplier.SupplierName,
                        Quantity = x.Quantity,
                        TotalMoney = x.TotalMoney,
                        Date = x.Date,
                        Note = x.Note
                    }).ToListAsync();

                var pagedData = pagingHelper.GetPagedData(receipts, pageNumber, pageSize);

                var totalPages = pagingHelper.CalculateTotalPages(receipts, pageSize);
                if (receipts == null)
                {
                    return new ResponsePagingReceipt { };
                }
                return new ResponsePagingReceipt { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
            }
            catch (Exception ex)
            {
                return new ResponsePagingReceipt { };
            }
        }
        [HttpPost("AddNewReceipt")]
        public async Task<IActionResult> AddNewReceipt(AddReceiptDTO receiptDTO)
        {
            try
            {
                var newReceipt = new Receipt
                {
                    ProductId = receiptDTO.ProductId,
                    SupplierId = receiptDTO.SupplierId,
                    Quantity = receiptDTO.Quantity,
                    TotalMoney = receiptDTO.TotalMoney,
                    Date = DateTime.Now,
                    Note = receiptDTO.Note
                };
                _context.Receipts.Add(newReceipt);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
