﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;

        public OrderController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
          Created by: NghiaLD
          Created on: 15/3/2024 (dd/mm/yyyy)
          Description: This method get list Order 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\OrderController.cs
          ---
          Modify by: NghiaLD
          Created on: 25/3/2024 (dd/mm/yyyy)  
          --
          Modify by: NghiaLD
          Created on: 10/4/2024 (dd/mm/yyyy)  
        */
        [HttpGet("GetAllOrders")]
        public async Task<ResponsePagingOrder> GetOrders(int pageNumber, DateTime? date)
        {
            try
            {
                var pagingHelper = new PagingHelper<OrderDTO>();
                int pageSize = 10;
                if (date == null)
                {
                    var orders = await _context.Orders.Select(x => new OrderDTO
                    {
                        CustomerName = x.Customer.CustomerName,
                        StaffName = x.Staff.FullName,
                        OderDate = x.OderDate,
                        TotalMoney = x.TotalMoney
                    }).ToListAsync();
                    var pagedData = pagingHelper.GetPagedData(orders, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(orders, pageSize);
                    return new ResponsePagingOrder { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {

                    var orderByDate = _context.Orders.Where(o => o.OderDate.Date == date).Select(x => new OrderDTO
                    {
                        CustomerName = x.Customer.CustomerName,
                        StaffName = x.Staff.FullName,
                        OderDate = x.OderDate,
                        TotalMoney = x.TotalMoney
                    }).ToList();
                    var pagedData = pagingHelper.GetPagedData(orderByDate, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(orderByDate, pageSize);
                    return new ResponsePagingOrder { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: NghiaLD
          Created on: 15/3/2024 (dd/mm/yyyy)
          Description: This method get list Order for day
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\OrderController.cs
          ---
          Modify by: NghiaLD
          Created on: 25/3/2024 (dd/mm/yyyy)
        */
        // GET: api/Order/5
        [HttpGet("GetOrderByDay")]
        public async Task<IActionResult> GetOrderByDay()
        {
            try
            {
                var order = _context.Orders.Where(o => o.OderDate.Date == DateTime.Today).ToList();
                var totalAmout = order.Sum(o => o.TotalMoney);
                if (order.Count == null)
                {
                    return BadRequest();
                }

                return Ok(new { Orders = order, TotalMount = totalAmout });
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: NghiaLD
          Created on: 15/3/2024 (dd/mm/yyyy)
          Description: This method get list Order for month
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\OrderController.cs
          ---
          Modify by: NghiaLD
          Created on: 25/3/2024 (dd/mm/yyyy)
        */
        // GET: api/Order/5
        [HttpGet("GetOrderByMonth")]
        public async Task<IActionResult> GetOrderMonth()
        {
            try
            {
                int month = DateTime.Today.Month;
                int year = DateTime.Today.Year;
                var order = _context.Orders.Where(o => o.OderDate.Month == month && o.OderDate.Year == year).ToList();
                var totalAmout = order.Sum(o => o.TotalMoney);
                if (order.Count == null)
                {
                    return BadRequest();
                }

                return Ok(new {Orders = order, TotalMount = totalAmout});
                //return Ok(order);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: NghiaLD
          Created on: 15/3/2024 (dd/mm/yyyy)
          Description: This method add an Order 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\OrderController.cs
          ---
          Modify by: NghiaLD
          Created on: 25/3/2024 (dd/mm/yyyy)
          --
          Modify by: NghiaLD
          Created on: 26/3/2024 (dd/mm/yyyy)  
          --
          Modify by: NghiaLD
          Created on: 5/4/2024 (dd/mm/yyyy) 
        */
        // POST: api/Order
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("NewOrder")]
        public async Task<IActionResult> CreateOrder(OrderCreateDTO orderDto)
        {
            try
            {
                var order = orderDto.Order;
                var listOrderDetail = orderDto.OrderDetails.ToList();

                decimal totalMoney = 0;
                foreach (var item in listOrderDetail)
                {
                    if (item.Quantity <= 0)
                    {
                        return BadRequest("Số lượng phải lớn hơn 0!");
                    }

                    var product = _context.Products.FirstOrDefault(o => o.ProductId == item.ProductId);
                    if (product == null || product.UnitInStock < item.Quantity)
                    {
                        return BadRequest("Sản phẩm này không đủ số lượng!");
                    }
                    else
                    {
                        totalMoney += item.Quantity.Value * product.UnitPrice.Value;
                    }
                }

                var newOrders = new Order
                {
                    CustomerId = order.CustomerId,
                    StaffId = order.StaffId,
                    OderDate = DateTime.Now,
                    TotalMoney = totalMoney
                };
                _context.Orders.Add(newOrders);
                _context.SaveChanges();

                foreach (var item in listOrderDetail)
                {
                    var product = _context.Products.FirstOrDefault(o => o.ProductId == item.ProductId);
                    var newOrderDetail = new OrderDetail
                    {
                        OrderId = newOrders.OrderId,
                        ProductId = item.ProductId,
                        UnitPrice = product.UnitPrice,
                        Quantity = item.Quantity
                    };
                    _context.OrderDetails.Add(newOrderDetail);

                    product.UnitInStock = product.UnitInStock - item.Quantity;
                    _context.SaveChanges();
                }
                return Ok(order);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
