﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.DTO.Entity;
using SEP490_G3_API.HelperServices;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context;

        public ProductController(SEP490_G3_test4Context context)
        {
            _context = context;
        }
        /*
          Created by: Duc VN
          Created on: 23/2/2024 (dd/mm/yyyy)
          Description: This method get list Product 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\CategoryController.cs
          ---
          Modify by: Duc VN
          Created on: 1/3/2024 (dd/mm/yyyy)
          ---
          Modify by: NghiaLD
          Created on: 31/3/2024 (dd/mm/yyyy)      
        */
        [HttpGet("GetAllProductPage")]
        public async Task<ResponsePagingProduct?> GetAllProduct(int pageNumber, string? search, int? categoryId)
        {
            try
            {
                var pagingHelper = new PagingHelper<ProductDTO>();
                int pageSize = 5;
                if (search == null && categoryId == null)
                {
                    //kết nối bảng Product với Category và Supplier 
                    var products = await _context.Products.Where(x => x.Active == true).Include(x => x.Category).Include(x => x.Supplier).Select(x => new ProductDTO
                    {
                        ProductId = x.ProductId,
                        ProductName = x.ProductName,
                        CateName = x.Category.CategoryName,
                        SupName = x.Supplier.SupplierName,
                        CateId = x.Category.CategoryId,
                        SupId = x.SupplierId,
                        Barcode = x.Barcode,
                        UnitPrice = x.UnitPrice,
                        UnitInStock = x.UnitInStock,
                        Description = x.Description,
                        Image = x.Image,
                        Unit = x.Unit,
                        Active = x.Active
                    }).ToListAsync();

                    var pagedData = pagingHelper.GetPagedData(products, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(products, pageSize);
                    return new ResponsePagingProduct { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
                else
                {
                    var list = await _context.Products.Include(x => x.Category).Include(x => x.Supplier).Where(x => x.ProductName.Contains(search.ToLower())).ToListAsync();
                    var products = list.Select(x => new ProductDTO
                    {
                        ProductId = x.ProductId,
                        ProductName = x.ProductName,
                        CateName = x.Category.CategoryName,
                        SupName = x.Supplier.SupplierName,
                        CateId = x.Category.CategoryId,
                        SupId = x.SupplierId,
                        Barcode = x.Barcode,
                        UnitPrice = x.UnitPrice,
                        UnitInStock = x.UnitInStock,
                        Description = x.Description,
                        Image = x.Image,
                        Unit = x.Unit,
                        Active = x.Active
                    }).ToList();
                    if (list.Count == 0)
                    {
                        return new ResponsePagingProduct { };
                    }
                    var pagedData = pagingHelper.GetPagedData(products, pageNumber, pageSize);

                    var totalPages = pagingHelper.CalculateTotalPages(products, pageSize);
                    return new ResponsePagingProduct { Data = pagedData, PageSize = pageSize, TotalPages = totalPages };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetAllProduct")]
        public async Task<IActionResult> GetAllProduct()
        {
            var list = await _context.Products.Where(x => x.Active == true).Include(x => x.Category).Include(x => x.Supplier).ToListAsync();
            var products = list.Select(x => new ProductDTO
            {
                ProductId = x.ProductId,
                ProductName = x.ProductName,
                CateName = x.Category.CategoryName,
                SupName = x.Supplier.SupplierName,
                Barcode = x.Barcode,
                UnitPrice = x.UnitPrice,
                UnitInStock = x.UnitInStock,
                Description = x.Description,
                Image = x.Image,
                Unit = x.Unit,
                Active = x.Active
            }).ToList();
            return Ok(products);
        }
        [HttpGet("SearchByName")]
        public IActionResult SearchByName(string name)
        {
            try
            {
                var list = _context.Products.Where(x => x.Active == true);
                var products = list.Where(x => x.ProductName.Contains(name.ToLower())).ToList();
                if (products.Count == 0)
                {
                    return BadRequest();
                }
                return Ok(products);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("SearchByBarCode")]
        public IActionResult SearchByBarCode(int id)
        {

            return Ok();
        }

        /*
          Created by: NghiaLD
          Created on: 01/4/2024 (dd/mm/yyyy)
          Description: This method create a Product 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
        */
        [HttpPost("CreateProduct")]
        public async Task<IActionResult> CreateProduct(ProductDTO productDTO, string phone)
        {
            try
            {
                var product = await _context.Products.FirstOrDefaultAsync(p => p.ProductName == productDTO.ProductName);
                if (product == null)
                {
                    var newProduct = new Product
                    {
                        CategoryId = productDTO.CateId,
                        SupplierId = productDTO.SupId,
                        ProductName = productDTO.ProductName,
                        
                        UnitPrice = productDTO.UnitPrice,
                        UnitInStock = productDTO.UnitInStock,
                        Description = productDTO.Description,
                        Image = productDTO.Image,
                        Unit = productDTO.Unit,
                        Active = true
                    };
                    _context.Products.Add(newProduct);
                    await _context.SaveChangesAsync();

                    var products = await _context.Products.FirstOrDefaultAsync(p => p.ProductId == newProduct.ProductId);
                    if(products != null)
                    {
                        products.Barcode = (phone + "-" + newProduct.ProductId);
                    }
                    await _context.SaveChangesAsync();
                    return Ok(newProduct.ProductId);
                }
                else
                {
                    return BadRequest("Product is exist!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: NghiaLD
          Created on: 01/4/2024 (dd/mm/yyyy)
          Description: This method update a Product 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
        */
        [HttpPut("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct(int id, ProductDTO productDTO)
        {
            try
            {
                var product = await _context.Products.FirstOrDefaultAsync(p => p.ProductId == id);
                if (product == null)
                {
                    return NotFound("Product doesn't exist!");
                }
                else
                {
                    product.ProductName = productDTO.ProductName;
                    product.CategoryId = productDTO.CateId;
                    product.SupplierId = productDTO.SupId;
                    product.Barcode = productDTO.Barcode;
                    product.UnitPrice = productDTO.UnitPrice;
                    product.UnitInStock = productDTO.UnitInStock;
                    product.Description = productDTO.Description;
                    product.Image = productDTO.Image;
                    product.Unit = productDTO.Unit;
                    product.Active = productDTO.Active;

                    _context.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /*
          Created by: NghiaLD
          Created on: 02/4/2024 (dd/mm/yyyy)
          Description: This method block a Product 
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
          Modify by: DucVN
          Created on: 14/4/2024 (dd/mm/yyyy)
        */
        [HttpPut("Block")]
        public async Task<IActionResult> Block(int id)
        {
            try
            {
                var product = await _context.Products.FirstOrDefaultAsync(x => x.ProductId == id);
                if (product != null)
                {
                    if (product.Active == true)
                    {
                        product.Active = false;
                        await _context.SaveChangesAsync();
                        return Ok();
                    }
                    else
                    {
                        product.Active = true;
                        await _context.SaveChangesAsync();
                        return Ok();
                    }
                }
                return NotFound("Product don't exist");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: NghiaLD
          Created on: 10/4/2024 (dd/mm/yyyy)
          Description: This method get top 10 product with highest sale quantity
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
        */
        [HttpGet("TopSaleQuantity")]
        public async Task<IActionResult> TopProductQuantity()
        {
            try
            {
                var topProducts = await _context.OrderDetails
                .GroupBy(od => od.ProductId)
                .Select(g => new
                {
                    ProductId = g.Key,
                    TotalQuantity = g.Sum(od => od.Quantity)
                })
                .OrderByDescending(p => p.TotalQuantity)
                .Take(10)
                .ToListAsync();

                var topProductIds = topProducts.Select(p => p.ProductId).ToList();

                var products = await _context.Products.Where(p => topProductIds.Contains(p.ProductId)).ToListAsync();

                var result = products
                    .Select(p => new
                    {
                        p.ProductId,
                        p.ProductName,
                        TotalQuantity = GetTotalQuantity(topProducts, p.ProductId)
                    })
                    .OrderByDescending(p => p.TotalQuantity)
                    .ToList();

                return Ok(result);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /*
          Created by: NghiaLD
          Created on: 10/4/2024 (dd/mm/yyyy)
          Description: This method get total sale quantity of product
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
        */
        private static int GetTotalQuantity(IEnumerable<dynamic> topProducts, int productId)
        {
            var topProduct = topProducts.FirstOrDefault(tp => tp.ProductId == productId);
            return topProduct != null ? topProduct.TotalQuantity : 0;
        }

        /*
          Created by: NghiaLD
          Created on: 10/4/2024 (dd/mm/yyyy)
          Description: This method get top 10 product with highest sale revenue
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
        */
        [HttpGet("GetTopRevenueProducts")]
        public async Task<IActionResult> GetTopRevenueProducts()
        {
            var topProducts = await _context.OrderDetails
                .GroupBy(od => od.ProductId)
                .Select(g => new
                {
                    ProductId = g.Key,
                    TotalRevenue = g.Sum(od => od.Quantity * od.Product.UnitPrice)
                })
                .OrderByDescending(p => p.TotalRevenue)
                .Take(10)
                .ToListAsync();

            var topProductIds = topProducts.Select(p => p.ProductId).ToList();

            var products = await _context.Products.Where(p => topProductIds.Contains(p.ProductId)).ToListAsync();

            var result = products.Select(p => new
                {
                    p.ProductId,
                    p.ProductName,
                    TotalRevenue = GetTotalRevenue(topProducts, p.ProductId)
                })
                .OrderByDescending(p => p.TotalRevenue)
                .ToList();

            return Ok(result);
        }

        /*
          Created by: NghiaLD
          Created on: 10/4/2024 (dd/mm/yyyy)
          Description: This method get sale revenue of product
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\ProductController.cs
          ---      
        */
        private static decimal GetTotalRevenue(IEnumerable<dynamic> topProducts, int productId)
        {
            var topProduct = topProducts.FirstOrDefault(tp => tp.ProductId == productId);
            return topProduct != null ? topProduct.TotalRevenue : 0;
        }
    }
}
