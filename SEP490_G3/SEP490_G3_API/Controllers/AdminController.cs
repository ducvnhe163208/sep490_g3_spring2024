﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEP490_G3_API.DTO;
using SEP490_G3_API.Models;

namespace SEP490_G3_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly SEP490_G3_test4Context _context1;
        private readonly SEP490_G3_SiteContext _context2;

        public AdminController(SEP490_G3_test4Context context1, SEP490_G3_SiteContext context2)
        {
            _context1 = context1;
            _context2 = context2;
        }
        /*
          Created by: Duc VN
          Created on: 5/3/2024 (dd/mm/yyyy)
          Description: This method get profile user
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\AdminController.cs
          ---
        */
        [HttpGet("GetAdmin")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var admin = await _context1.Admins.Select(a => new
                {
                    a.AdminId,
                    a.FullName,
                    a.Phone,
                    a.Email,
                    a.Avatar,
                    a.TaxCode,
                    a.BankAccountNumber,
                    a.CitizenIdentificationCard,
                    a.BankBin
                }).FirstOrDefaultAsync(x => x.AdminId == 1);
                return Ok(admin);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /*
          Created by: Duc VN
          Created on: 5/3/2024 (dd/mm/yyyy)
          Description: This method update profile
          File: $\sep490_g3_spring2024\SEP490_G3\SEP490_G3_API\Controllers\AdminController.cs
          ---
        */
        [HttpPut("UpdateProfile")]
        public async Task<IActionResult> UpdateProfile(AdminDTO adminDTO, int id)
        {
            try
            {
                var admin = await _context1.Admins.FirstOrDefaultAsync(x => x.AdminId == id);
                if (admin != null)
                {
                    admin.FullName = adminDTO.FullName;
                    admin.Phone = adminDTO.Phone;
                    admin.Email = adminDTO.Email;
                    admin.Avatar = adminDTO.Avatar;
                    admin.TaxCode = adminDTO.TaxCode;
                    admin.BankAccountNumber = adminDTO.BankAccountNumber;
                    admin.BankBin = adminDTO.BankBin;
                    await _context1.SaveChangesAsync();
                    return Ok();
                }
                return NotFound("Admin don't exist");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet("GetAdminById")]
        public async Task<IActionResult> GetAdminById(int id)
        {
            try
            {
                var admin = await _context1.Admins.FirstOrDefaultAsync(x => x.AdminId == id);
                if(admin == null)
                {
                    return BadRequest();
                }
                return Ok(admin);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
