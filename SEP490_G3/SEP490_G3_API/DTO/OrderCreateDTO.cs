﻿using SEP490_G3_API.Models;

namespace SEP490_G3_API.DTO
{
    public class OrderCreateDTO
    {
        public OrderDTO Order { get; set; }
        public List<OrderDetailDTO> OrderDetails { get; set; }
    }
}
