﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class OrderDetailDTO
    {
        //public int? OrderId { get; set; }
        [Required(ErrorMessage = "Choose product!")]
        public int? ProductId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Quantity must be greater than 0")]
        public int? Quantity { get; set; }
    }
}
