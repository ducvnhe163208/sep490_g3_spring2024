﻿namespace SEP490_G3_API.DTO
{
    public class SessionDTO
    {
        public DateTime? Date { get; set; }
        public int? TimeslotId { get; set; }
        public string? Timeslot1 { get; set; }
        public bool? Status { get; set; }
        public string? Note { get; set; }
        public string? FullName { get; set; }
    }
    public class SessionDTO2
    {
        public int StaffId { get; set; }
        public DateTime? Date { get; set; }
        public int? TimeslotId { get; set; }
        public string? Timeslot1 { get; set; }
        public bool? Status { get; set; }
        public string? Note { get; set; }
        public string? FullName { get; set; }
    }
}
