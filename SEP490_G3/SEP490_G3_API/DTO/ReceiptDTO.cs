﻿namespace SEP490_G3_API.DTO
{
    public class ReceiptDTO
    {
        public int? ProductId { get; set; }
        public string? ProductName { get; set; }
        public string? ProductImage { get; set; }
        public string? Unit { get; set; }
        public int? SupplierId { get; set; }
        public string? SupplierName { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalMoney { get; set; }
        public string? Note { get; set; }
        public DateTime Date { get; set; }
    }
    public class AddReceiptDTO
    {
        public int ProductId { get; set; }
        public int SupplierId { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalMoney { get; set; }
        public DateTime Date { get; set; }
        public string? Note { get; set; }
    }
}
