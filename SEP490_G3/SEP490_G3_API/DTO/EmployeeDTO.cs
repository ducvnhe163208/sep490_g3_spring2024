﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public int? RoleId { get; set; }
        public string? RoleName { get; set; }

        [StringLength(100, ErrorMessage = "Full name must be at most 100 characters")]
        public string? FullName { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string? Password { get; set; }

        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string? Phone { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string? Email { get; set; }

        public string? Avatar { get; set; }

        [StringLength(200, ErrorMessage = "Address must be at most 200 characters")]
        public string? Address { get; set; }

        [StringLength(20, ErrorMessage = "Tax code must be at most 20 characters")]
        public string? TaxCode { get; set; }

        [StringLength(12, MinimumLength = 12, ErrorMessage = "Citizen identification card must be 12 digits")]
        public string? CitizenIdentificationCard { get; set; }

        [RegularExpression(@"^\d{10,20}$", ErrorMessage = "Invalid bank account number")]
        public string? BankAccountNumber { get; set; }

        public bool? Active { get; set; }
    }
    public class AddEmployeeDTO
    {
        public int? RoleId { get; set; }

        [StringLength(100, ErrorMessage = "Full name must be at most 100 characters")]
        public string? FullName { get; set; }

        [Required(ErrorMessage = "Username is required")]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string? Password { get; set; }

        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string? Phone { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string? Email { get; set; }

        public string? Avatar { get; set; }

        [StringLength(200, ErrorMessage = "Address must be at most 200 characters")]
        public string? Address { get; set; }

        [StringLength(20, ErrorMessage = "Tax code must be at most 20 characters")]
        public string? TaxCode { get; set; }

        [StringLength(12, MinimumLength = 12, ErrorMessage = "Citizen identification card must be 12 digits")]
        public string? CitizenIdentificationCard { get; set; }

        [RegularExpression(@"^\d{10,20}$", ErrorMessage = "Invalid bank account number")]
        public string? BankAccountNumber { get; set; }

        public bool? Active { get; set; }
    }
    public class SalaryDTO
    {
        public string StaffName { get; set; }
        public decimal Salary { get; set; }
    }
    public class StaffDTO
    {
        public string StaffName { get; set; }
        public int Present { get; set; }
        public int Absent { get; set; }
    }
    public class ReportDTO
    {
        public string StaffName { get; set; }
        public decimal? totalMoney { get; set; }
    }
    public class ReportBenefitStaff
    {
        public string StaffName { get; set; }
        public decimal? totalMoney { get; set; }
    }
}
