﻿namespace SEP490_G3_API.DTO.Entity
{
    public class ResponseData<T>
    {
        public List<T>? Data { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
    }
    public class ResponseDataBenefit<T>
    {
        public List<T>? Data { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public decimal? TotalMoney { get; set; }
    }
}
