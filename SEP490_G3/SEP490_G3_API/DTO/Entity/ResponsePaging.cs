﻿namespace SEP490_G3_API.DTO.Entity
{
    public class ResponsePagingSupplier: ResponseData<DTO.SupplierDTO>
    {
    }
    public class ResponsePagingCustomer: ResponseData<DTO.CustomerDTO>
    {
    }
    public class ResponsePagingStaff : ResponseData<DTO.EmployeeDTO>
    {
    }
    public class ResponsePagingCategory : ResponseData<DTO.CategoryDTO>
    {
    }
    public class ResponsePagingProduct : ResponseData<DTO.ProductDTO>
    {
    }
    public class ResponsePagingUser : ResponseData<DTO.UserDTO>
    {
    }
    public class ResponsePagingOrder : ResponseData<DTO.OrderDTO>
    {
    }
    public class ResponsePagingBill : ResponseData<DTO.BillDTO>
    {
    }
    public class ResponsePagingSite : ResponseData<DTO.SiteDTO>
    {
    }
    public class ResponsePagingTimeslot : ResponseData<Models.Timeslot>
    {
    }
    public class ResponsePagingSession : ResponseData<DTO.SessionDTO>
    {
    }
    public class ResponsePagingOrderModel : ResponseData<Models.Order>
    {
    }
    public class ResponsePagingSalary : ResponseData<DTO.SalaryDTO>
    {
    }
    public class ResponsePagingStaffAttend : ResponseData<DTO.StaffDTO>
    {
    }
    public class ResponsePagingReceipt : ResponseData<DTO.ReceiptDTO>
    {
    }
    public class ResponsePagingReportCustomer : ResponseData<DTO.ReportCustomerDTO>
    {
    }
    public class ResponsePagingReportProduct : ResponseData<DTO.ReportProductDTO>
    {
    }
    public class ResponsePagingReportOrder : ResponseDataBenefit<DTO.ReportOrderByDateDTO>
    {
    }
    public class ResponsePagingReportOrderByStaff : ResponseData<DTO.ReportOrderDTO>
    {
    }
}
