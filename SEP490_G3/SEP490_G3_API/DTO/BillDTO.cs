﻿namespace SEP490_G3_API.DTO
{
    public class BillDTO
    {
        public string? CustomerName { get; set; }
        public DateTime? OderDate { get; set; }
        public decimal? TotalMoney { get; set; }
    }
}
