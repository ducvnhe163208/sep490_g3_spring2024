﻿namespace SEP490_G3_API.DTO
{
    public class CategoryDTO
    {
        public int CategoryId { get; set; }
        public string? CategoryName { get; set; } = null!;
        public bool? Active { get; set; }
    }
}
