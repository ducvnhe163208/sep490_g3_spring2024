﻿namespace SEP490_G3_API.DTO
{
    public class OrderDTO
    {
        public int? CustomerId { get; set; }
        public int? StaffId { get; set; }
        public string? CustomerName { get; set; }
        public string? StaffName { get; set; }
        public decimal? TotalMoney { get; set; }
        public DateTime? OderDate { get; set; }
    }
}
