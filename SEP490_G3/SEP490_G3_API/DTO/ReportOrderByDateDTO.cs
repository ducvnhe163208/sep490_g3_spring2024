﻿namespace SEP490_G3_API.DTO
{
    public class ReportOrderByDateDTO
    {
        public string? ProductName { get; set; }
        public int? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? UnitInStock { get; set; }
        public decimal? TotalMoney { get; set; }
    }
    public class ReportOrderDTO
    {
        public string ProductName { get; set; }
        public int? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalMoney { get; set; }
    }
}
