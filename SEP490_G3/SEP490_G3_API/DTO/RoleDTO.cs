﻿namespace SEP490_G3_API.DTO
{
    public class RoleDTO
    {
        public string? RoleName { get; set; }
    }
}
