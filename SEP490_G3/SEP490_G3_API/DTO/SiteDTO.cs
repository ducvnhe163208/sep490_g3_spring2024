﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class SiteDTO
    {
        public string? SiteName { get; set; }
        public int? UserId { get; set; }

        [StringLength(100, ErrorMessage = "Full name must be at most 100 characters")]
        public string? Logo { get; set; }

        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string? Phone { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Tax code is required")]
        [StringLength(20, ErrorMessage = "Tax code must be at most 20 characters")]
        public string TaxCode { get; set; } = null!;

        [Required(ErrorMessage = "Citizen identification card is required")]
        [StringLength(12, MinimumLength = 12, ErrorMessage = "Citizen identification card must be 12 digits")]
        public string CitizenIdentificationCard { get; set; } = null!;

        [Required(ErrorMessage = "Bank account number is required")]
        [StringLength(20, ErrorMessage = "Bank account number must be at most 20 characters")]
        public string BankAccountNumber { get; set; } = null!;

        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
