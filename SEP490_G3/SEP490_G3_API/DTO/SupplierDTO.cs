﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class SupplierDTO
    {
        public int SupplierId { get; set; }
        public string? SupplierName { get; set; }
        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string? Phone { get; set; }

        [StringLength(200, ErrorMessage = "Address must be at most 200 characters")]
        public string? Address { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string? Email { get; set; }

        public string? Logo { get; set; }

        [StringLength(20, ErrorMessage = "Tax code must be at most 20 characters")]
        public string? TaxCode { get; set; }

        public bool? Active { get; set; }
    }
}
