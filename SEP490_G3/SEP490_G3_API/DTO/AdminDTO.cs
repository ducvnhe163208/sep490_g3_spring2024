﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class AdminDTO
    {
        [Required(ErrorMessage = "Full name is required")]
        public string FullName { get; set; } = null!;

        public string? Avatar { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string Phone { get; set; } = null!;

        [Required(ErrorMessage = "Email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; } = null!;

        [Required(ErrorMessage = "Tax code is required")]
        public string TaxCode { get; set; } = null!;

        [Required(ErrorMessage = "Citizen identification card is required")]
        [StringLength(12, MinimumLength = 12, ErrorMessage = "Citizen identification card must be 12 digits")]
        public string CitizenIdentificationCard { get; set; } = null!;

        [Required(ErrorMessage = "Bank account number is required")]
        [RegularExpression(@"^\d{10,20}$", ErrorMessage = "Invalid bank account number")]
        public string BankAccountNumber { get; set; } = null!;
        public string BankBin { get; set; } = null!;
    }
}
