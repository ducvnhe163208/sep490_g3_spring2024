﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class CustomerDTO
    {
        public int CustomerId { get; set; }
        [Required]
        public string? CustomerName { get; set; }
        [Required]
        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string? Phone { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string? Email { get; set; }

        [StringLength(200, ErrorMessage = "Address must be at most 200 characters")]
        public string? Address { get; set; }

        public bool? Active { get; set; }
    }
    public class ReportCustomerDTO
    {
        [Required]
        public string? CustomerName { get; set; }
        [Required]
        [RegularExpression(@"^\d{10,11}$", ErrorMessage = "Invalid phone number")]
        public string? Phone { get; set; }
        [StringLength(200, ErrorMessage = "Address must be at most 200 characters")]
        public string? Address { get; set; }
        public decimal? TotalMoney { get; set; }
    }
}
