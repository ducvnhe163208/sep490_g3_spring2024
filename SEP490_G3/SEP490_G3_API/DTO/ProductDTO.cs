﻿using System.ComponentModel.DataAnnotations;

namespace SEP490_G3_API.DTO
{
    public class ProductDTO
    {
        [Required(ErrorMessage = "ProductId is required")]
        public int ProductId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "CateId must be a positive integer")]
        public int? CateId { get; set; }
        public string? CateName { get; set; }
        public string? SupName { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "SupId must be a positive integer")]
        public int? SupId { get; set; }

        [StringLength(50, ErrorMessage = "Barcode must be at most 50 characters")]
        public string? Barcode { get; set; }

        [Required(ErrorMessage = "Product name is required")]
        [StringLength(100, ErrorMessage = "Product name must be at most 100 characters")]
        public string? ProductName { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "Unit price must be a non-negative number")]
        public decimal? UnitPrice { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Unit in stock must be a non-negative integer")]
        public int? UnitInStock { get; set; }

        [StringLength(200, ErrorMessage = "Description must be at most 200 characters")]
        public string? Description { get; set; }

        public string? Image { get; set; }

        [StringLength(50, ErrorMessage = "Unit must be at most 50 characters")]
        public string? Unit { get; set; }
        public decimal? UnitCost { get; set; }
        public bool? Active { get; set; }
    }

    public class ReportProductDTO
    {
        public string? ProductName { get; set; }
        public decimal? TotalMoney_Old { get; set; }
        public int? UnitInStock_Old { get; set; }
        public int? UnitInStock_Now { get; set; }
        public string? Image { get; set; }
        public decimal? TotalMoney_Now { get; set; }
    }
}
